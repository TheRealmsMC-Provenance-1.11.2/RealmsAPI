package PlayerCommands;

import PlayerManager.PlayerManager.nPlayer;
import chatapi.Utilities;
import java.util.UUID;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import Realms.RealmsAPI;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author lanca
 */
public class NationCommands implements CommandExecutor {
    
    private RealmsAPI plugin;
    
    public NationCommands(RealmsAPI plugin){
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args){
        if(!(sender instanceof Player)){
            return false;
        } else {
            Player player = (Player) sender;
            UUID pid = player.getUniqueId();
            
            //WHENEVER SOMEONE TYPES /nations
            if(args.length == 0) {
                Utilities.sendNormal(player, "------[Realms Nations]----");
                this.plugin.getNationManager().printNationList(player);
                return true;
            }
            
           
            if (args.length == 1) {
                //WHENEVER SOMEONE TYPES "/nations leave"
                if (args[0].equalsIgnoreCase("leave")){
                    
                    nPlayer n = this.plugin.getPlayerManager().playerList.get(pid.toString());
                    if(n != null){
                        if (!n.getNation().equals("none")) {
                            this.plugin.getPlayerManager().leaveNation(pid);
                            return true;

                        } else {
                            Utilities.sendBad(player, "You are currently not part of a Nation");
                            return true;
                        }                    
                    }
                }
                
                //WHENEVER SOMEONE TYPES "/nations deposit"
                if(args[0].equalsIgnoreCase("deposit")){
                    ItemStack checking = player.getInventory().getItemInMainHand();
                    player.getInventory().setItemInMainHand(new ItemStack(Material.AIR,0));
                    player.updateInventory();
                    
                    Boolean deposited = this.plugin.getPlayerManager().depositItem(player, checking);
                    if (!deposited)
                    {
                        Utilities.sendBad(player, "Item in hand could not be deposited into Nation Bank");
                        player.getLocation().getWorld().dropItemNaturally(player.getLocation(), checking);
                    } 
                }
                
                //WHENEVER SOMEONE TYPES "/nations bank"
                if(args[0].equalsIgnoreCase("bank")){
                    nPlayer check = this.plugin.getPlayerManager().playerList.get(player.getUniqueId().toString());
                    if(check != null){
                        this.plugin.getNationManager().printNationResurces(player, check.getNation());  
                    }
                }
                
            }
            if(args.length == 2){
                
                
                
                //SOMEONE TYPES IN "/nations new X"
                if(args[0].equalsIgnoreCase("new")){
                    String nation = args[1];
                    if(nation.length() <= 20){
                        this.plugin.getPlayerManager().leaveNation(pid);
                        this.plugin.getNationManager().newNation(player, nation);
                        
                        nPlayer check = this.plugin.getPlayerManager().playerList.get(player.getUniqueId().toString());
                        if(check != null){
                            check.setNation(nation);
                            check.setRole(3);
                            Utilities.sendGood(player, "You have joined: " + nation);                         
                        }
                        
                        return true;
                    } else {
                        Utilities.sendBad(player, "Nation name must be shorter than 20 Characters");
                        return true;
                    }
                }
                
                //SOMEONE TYPES IN "/nations info X"
                if ((args[0].equalsIgnoreCase("info")) || (args[0].equalsIgnoreCase("i"))) {
                    String infoNation = args[1].toLowerCase();
                    this.plugin.getNationManager().infoNation(player, infoNation);
                    return true;
                }
                
                //SOMEONE TYPES IN "/nations buildings X"
                if ((args[0].equalsIgnoreCase("buildings")) || (args[0].equalsIgnoreCase("b"))) {
                    String infoNation = args[1].toLowerCase();
                    this.plugin.getNationManager().printNationBuildings(player, infoNation);
                    return true;
                }
                
                //SOMEONE TYPES IN "/nations list X"
                if ((args[0].equalsIgnoreCase("list"))){
                    String infoNation = args[1].toLowerCase();
                    this.plugin.getNationManager().printNationPlayers(player, infoNation);
                }
                
                
                //SOMEONE TYPES IN "/nations invite X"
                if (args[0].equalsIgnoreCase("invite")) {
                    Player invited = this.plugin.getServer().getPlayer(args[1].toLowerCase());
                    if (invited != null) {
                        
                        nPlayer inviter = this.plugin.getPlayerManager().playerList.get(pid.toString());
                        if(inviter != null){
                            if (!inviter.getNation().equalsIgnoreCase("none")) {
                                this.plugin.addInviteRequest(player.getDisplayName(), invited.getDisplayName(), inviter.getNation());
                                Utilities.sendGood(player, "Your invite has been sent to " + invited.getDisplayName());
                                return true;
                            } else {
                                Utilities.sendBad(player, "You are currently not part of a Nation");
                                return true;
                            }                         
                        }
                        
                    } else {
                        Utilities.sendBad(player, args[1].toLowerCase() + " is either not a valid Player or Offline");
                        return true;
                    }
                }
                
                //SOMEONE TYPES IN "/nations promote X"
                if (args[0].equalsIgnoreCase("promote")) {
                    Player promoted = this.plugin.getServer().getPlayer(args[1].toLowerCase());
                    if (promoted != null) {
                        nPlayer promote = this.plugin.getPlayerManager().playerList.get(promoted.getUniqueId().toString());
                        nPlayer ruler = this.plugin.getPlayerManager().playerList.get(pid.toString());
                        if(ruler != null){
                            if (!ruler.getNation().equalsIgnoreCase("none")) {
                                if(promote != null)
                                {
                                    if(promote.getNation().equalsIgnoreCase(ruler.getNation()))
                                    {
                                        int level = promote.getRole() + 1;
                                        if(level > 3)
                                        {
                                            level = 3;
                                        }
                                        if(level < ruler.getRole())
                                        {
                                            promote.setRole(level);
                                            Utilities.sendGood(promoted, "You have had your role increased within the nation it is now Role Level " + level);
                                            Utilities.sendGood(player, "You have promoted " + promoted.getDisplayName() + " to Role Level " + level);
                                            return true;
                                        }
                                    } else {
                                        Utilities.sendBad(player, promoted.getDisplayName() + " is not within your Nation");
                                        return false;
                                    }
                                }
                                return false;
                            } else {
                                Utilities.sendBad(player, "You are currently not part of a Nation");
                                return true;
                            }                         
                        }  
                    } else {
                        Utilities.sendBad(player, args[1].toLowerCase() + " is either not a valid Player or Offline");
                        return true;
                    }
                }

                //Whenever someone types in "/nations demote X"
                if (args[0].equalsIgnoreCase("demote")) {
                    Player demoted = this.plugin.getServer().getPlayer(args[1].toLowerCase());
                    if (demoted != null) {
                        nPlayer demote = this.plugin.getPlayerManager().playerList.get(demoted.getUniqueId().toString());
                        nPlayer ruler = this.plugin.getPlayerManager().playerList.get(pid.toString());
                        if (ruler != null) {
                            if (!ruler.getNation().equalsIgnoreCase("none")) {
                                if (demote != null) {
                                    if (demote.getNation().equalsIgnoreCase(ruler.getNation())) {
                                        int level = demote.getRole() - 1;
                                        if(level < 1)
                                        {
                                            level = 1;
                                        }
                                        if (level+1 <= ruler.getRole()) {
                                            demote.setRole(level);
                                            Utilities.sendGood(demoted, "You have had your role decreased within the nation it is now Role Level " + level);
                                            Utilities.sendGood(player, "You have demoted " + demoted.getDisplayName() + " to Role Level " + level);
                                            return true;
                                        }
                                    } else {
                                        Utilities.sendBad(player, demoted.getDisplayName() + " is not within your Nation");
                                        return false;
                                    }
                                }
                                return false;
                            } else {
                                Utilities.sendBad(player, "You are currently not part of a Nation");
                                return true;
                            }
                        }
                    } else {
                        Utilities.sendBad(player, args[1].toLowerCase() + " is either not a valid Player or Offline");
                        return true;
                    }
                }
                
                if (args[0].equalsIgnoreCase("kick")) {
                    nPlayer commandPlayer = this.plugin.getPlayerManager().playerList.get(pid.toString());
                    if(commandPlayer != null){
                        if(!commandPlayer.getNation().equalsIgnoreCase("none")){
                            if(commandPlayer.getRole() >= 2){
                                String nation = commandPlayer.getNation();
                                this.plugin.getPlayerManager().kickPlayer(args[1], player, nation);
                                return true;
                            }
                            Utilities.sendBad(player, "You do not have a high enough rank to perform that command");
                        }
                        Utilities.sendBad(player, "You are currently not part of a nation");
                        return false;
                    }
                }
            }
        }
        return false;
    }
}
