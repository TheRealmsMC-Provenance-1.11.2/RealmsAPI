package Realms;

import org.bukkit.inventory.ItemStack;

/**
 *
 * @author lanca
 */
public class Util {
    
    public static int getHunger(ItemStack item) {
        switch (item.getType()) {
            case APPLE:
                return 4;
            case BAKED_POTATO:
                return 5;
            case BEETROOT:
                return 1;
            case BEETROOT_SOUP:
                return 6;
            case BREAD:
                return 5;
            case CAKE:
                return 14;
            case CARROT:
                return 3;
            case CHORUS_FRUIT:
                return 4;
            case COOKED_BEEF:
                return 8;
            case COOKED_CHICKEN:
                return 6;
            case COOKED_FISH:
                if (item.getDurability() == 0) { // fish
                    return 5;
                } else if (item.getDurability() == 1) { //salmon
                    return 6;
                } else if (item.getData().getData() == 2) { //clownfish
                    return 1;
                } else if (item.getData().getData() == 3) { //puffer
                    return 1;
                } else {
                    return 0;
                }
            case COOKED_MUTTON:
                return 6;
            case GRILLED_PORK:
                return 8;
            case COOKED_RABBIT:
                return 5;
            case COOKIE:
                return 2;
            case GOLDEN_APPLE:
                return 4;
            case GOLDEN_CARROT:
                return 6;
            case MELON:
                return 2;
            case MUSHROOM_SOUP:
                return 6;
            case POISONOUS_POTATO:
                return 2;
            case POTATO:
                return 1;
            case PUMPKIN_PIE:
                return 8;
            case RABBIT_STEW:
                return 10;
            case RAW_BEEF:
                return 3;
            case RAW_CHICKEN:
                return 2;
            case RAW_FISH: //4 TO LISTreturn ;
                if (item.getDurability() == 0) { // fish
                    return 2;
                } else if (item.getDurability() == 1) { //salmon
                    return 2;
                } else if (item.getData().getData() == 2) { //clownfish
                    return 1;
                } else if (item.getData().getData() == 3) { //puffer
                    return 1;
                } else {
                    return 0;
                }
            case MUTTON:
                return 2;
            case PORK:
                return 3;
            case RABBIT:
                return 3;
            case ROTTEN_FLESH:
                return 4;
            case SPIDER_EYE:
                return 2;
//            case SKULL_ITEM:
//                if (item.getItemMeta().hasDisplayName())
//                    if (api.hasHead(item.getItemMeta().getDisplayName()))
//                    {
//                        return api.getHeadHunger(item.getItemMeta().getDisplayName());
//                    }
            default:
                return 0;
        }
    }
    
}
