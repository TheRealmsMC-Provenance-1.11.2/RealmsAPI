
package Realms;

import BuildingManager.BuildingDesign;
import BuildingManager.BuildingManager;
import BuildingManager.BuildingSQL;
import BuildingManager.DesignStorage;
import Buildings.BuildingBreak;
import Buildings.BuildingCreate;
import Buildings.Kiln;
import InviteManager.InviteManager;
import InviteManager.InviteManager.Request;
import NationManager.NationManager;
import PlayerCommands.NationCommands;
import PlayerManager.PlayerManager;
import RealmsGuards.GuardCommands;
import RealmsGuards.GuardManager;
import RealmsGuards.SentryListener;
import RealmsGuards.SentryTrait;
import com.zaxxer.hikari.HikariDataSource;
import java.text.ParseException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.trait.TraitInfo;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import playerListeners.PlayerListeners;


public class RealmsAPI extends JavaPlugin {
    
    //Instances
    private static RealmsAPI instance;
    private SQLManager sqlManager;
    private PlayerManager playerManager;
    private NationManager nationManager;
    private GuardManager guardManager;
    
    //Logger
    private static final Logger logger = Logger.getLogger("Minecraft");
    
    //DATABASE INFORMATION
    public String ip;
    public String port;
    public String database;
    public String user;
    public String password;
    
    
    private DesignStorage designStorage;
    private BuildingManager buildingManager;
    private BuildingSQL buildingSQL;
    private PlayerListeners listeners;
    private InviteManager inviteManager;
    private SentryListener sentryListener;
    private Kiln kiln;
    private BuildingCreate buildingcreate;
    private BuildingBreak buildingbreak;
    public HashMap<String, Integer> maxBuilds;
    
    public boolean debug = false;
    
    @Override
    public void onEnable()
    {
        if (getServer().getPluginManager().getPlugin("Citizens") == null || getServer().getPluginManager().getPlugin("Citizens").isEnabled() == false) {
            getLogger().log(Level.SEVERE, "Citizens 2.0 not found or not enabled");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
        instance = this;
        this.maxBuilds = new HashMap<>();
        
        CitizensAPI.getTraitFactory().registerTrait(TraitInfo.create(SentryTrait.class).withName("sentry"));
        this.getServer().getPluginManager().registerEvents(new SentryListener(this), this);
        getCommand("guards").setExecutor(new GuardCommands(this));
        getCommand("nations").setExecutor(new NationCommands(this));
        
        loadConfig();
        
        this.sqlManager = new SQLManager(this, this.logger);
        this.playerManager = new PlayerManager(this);
        this.nationManager = new NationManager(this);
        this.designStorage = new DesignStorage(this);
        designStorage.loadBuildingDesigns();
        
        this.buildingManager = new BuildingManager(this);
        this.buildingSQL = new BuildingSQL(this);
        this.buildingManager.loadBuilds();
        this.inviteManager = new InviteManager(this);
        this.guardManager = new GuardManager(this);
        this.getGuardManager().reloadMyConfig();
        
        this.listeners = new PlayerListeners(this);
        this.sentryListener = new SentryListener(this);
        this.buildingcreate = new BuildingCreate(this);
        this.buildingbreak = new BuildingBreak(this);
        this.kiln = new Kiln(this);
        try {
            this.buildingManager.upkeepBuildings();
        } catch (ParseException ex) {
            Logger.getLogger(RealmsAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.getServer().getPluginManager().registerEvents(this.kiln, this);
        this.getServer().getPluginManager().registerEvents(this.listeners, this);
        this.getServer().getPluginManager().registerEvents(this.sentryListener, this);
        
        BuildingRecipes.registerRecipes();
        
    }
    
    @Override
    public void onDisable()
    {
        for(Player player : getServer().getOnlinePlayers())
        {
            this.getPlayerManager().savePlayer(player.getUniqueId().toString());
            //player.kickPlayer("Server is restarting"); 
        }
        this.guardManager.saveWaypoints();
        this.buildingManager.saveBuilds();
        this.nationManager.saveNation();
        this.nationManager.saveResources();
        this.nationManager.deleteNation();
//        this.playerManager.saveAll();
        
        
        Bukkit.getServer().getScheduler().cancelTasks(this);
        
        if (this.sqlManager.hikari != null)
        {
            this.sqlManager.hikari.close();
        }
    }
    
    
    public void loadConfig(){
        this.saveResource("config.yml", false); 
        ip = this.getConfig().getString("database.ip");
        port = this.getConfig().getString("database.port");
        database = this.getConfig().getString("database.database");
        user = this.getConfig().getString("database.user");
        password = this.getConfig().getString("database.password");
        this.maxBuilds.put("Barracks", this.getConfig().getInt("maxbuild.Barracks"));
        this.maxBuilds.put("CityHall", this.getConfig().getInt("maxbuild.CityHall", 0));
        this.maxBuilds.put("Crusher", this.getConfig().getInt("maxbuild.Crusher", 0));
        this.maxBuilds.put("Kiln", this.getConfig().getInt("maxbuild.Kiln", 0));
        this.maxBuilds.put("Center", this.getConfig().getInt("maxbuild.Center", 0));
        this.maxBuilds.put("CenterUpg", this.getConfig().getInt("maxbuild.CenterUpg", 0));
        this.maxBuilds.put("Smeltery", this.getConfig().getInt("maxbuild.Smeltery", 0));
        this.maxBuilds.put("Forge", this.getConfig().getInt("maxbuild.Forge", 0));
        this.maxBuilds.put("Pier", this.getConfig().getInt("maxbuild.Pier", 0));
        this.maxBuilds.put("Granary", this.getConfig().getInt("maxbuild.Granary", 0));
        this.maxBuilds.put("PrintingPress", this.getConfig().getInt("maxbuild.PrintingPress", 0));
        this.maxBuilds.put("Library", this.getConfig().getInt("maxbuild.Library", 0));
        
    }
    
    
    boolean checkPlugin(String name) {
        if (getServer().getPluginManager().getPlugin(name) != null) {
            if (getServer().getPluginManager().getPlugin(name).isEnabled() == true) {
                return true;
            }
        }
        return false;
    }

    public void debug(String s) {
        if (debug) {
            this.getServer().getLogger().info(s);
        }
    }    
    
    
    public static RealmsAPI getInstance()
    {
        return instance; 
    }
    public HikariDataSource getHikari(){
        return this.sqlManager.hikari;
    }
    
    public PlayerManager getPlayerManager() {
        return this.playerManager;
    }
    
    public NationManager getNationManager() {
        return this.nationManager;
    }
    
    public DesignStorage getDesignStorage(){
        return this.designStorage;
    }
    
    public BuildingManager getBuildingManager(){
        return this.buildingManager;
    }
    
    public BuildingSQL getBuildingSQL(){
        return this.buildingSQL;
    }
    
    public BuildingDesign getBuildingDesign(String designId){
        return getDesignStorage().getDesign(designId);
    }
    
    public GuardManager getGuardManager(){
        return this.guardManager;
    }
    
    public PlayerListeners getListeners(){
        return this.listeners;
    }
    
    public BuildingCreate getBuildingCreate(){
        return this.buildingcreate;
    }
    
    public BuildingBreak getBuildingBreak(){
        return this.buildingbreak;
    }
    
    public void logInfo(String msg) {
        //msg = ChatColor.translateAlternateColorCodes('&', msg);
        this.logger.info("[Nations]" + ChatColor.stripColor(msg));
    }
    
    public void addInviteRequest(String sender, String invited, String nation){
        this.inviteManager.addInviteRequest(sender, invited, nation);
    }
    
    public void acceptInvite(Request req)
    {
        this.inviteManager.processAcceptInvite(req);
    }
    
    public void denyInvite(Request req){
        this.inviteManager.processDenyInvite(req);
    }
    
    public HashMap<String, Request> invites()
    {
        return this.inviteManager.invites;
    }
    
    
}
