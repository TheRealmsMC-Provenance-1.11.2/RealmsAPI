package Realms;

import com.zaxxer.hikari.HikariDataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Logger;

public class SQLManager {

    private RealmsAPI plugin;
    public HikariDataSource hikari;
    private Logger logger;

    public SQLManager(RealmsAPI plugin, Logger logger) {
        this.plugin = plugin;
        this.logger = logger;

        hikari = new HikariDataSource();
        hikari.setMaximumPoolSize(10);
        hikari.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
        hikari.addDataSourceProperty("serverName", plugin.ip);
        hikari.addDataSourceProperty("port", plugin.port);
        hikari.addDataSourceProperty("databaseName", plugin.database);
        hikari.addDataSourceProperty("user", plugin.user);
        hikari.addDataSourceProperty("password", plugin.password);

        setupPlayerTable();
        setupBuildingsTable();
        setupNationBuildings();
        setupNationTable();
    }

    public final void setupPlayerTable() {

        Connection connection = null;
        String setupPlayer = "CREATE TABLE IF NOT EXISTS players (uuid VARCHAR(40), name VARCHAR(40), start VARCHAR(40), nation VARCHAR(40), role INT, discord VARCHAR(40), token VARCHAR(40), PRIMARY KEY (uuid))";
        PreparedStatement createPlayers = null;
        try {
            connection = hikari.getConnection();
            createPlayers = connection.prepareStatement(setupPlayer);
            createPlayers.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (createPlayers != null) {
                try {
                    createPlayers.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public final void setupBuildingsTable() {

        Connection connection = null;
        String setupBuilding = "CREATE TABLE IF NOT EXISTS buildings (uuid VARCHAR(40), nation VARCHAR(40), world VARCHAR(40), LocX INT, LocY INT, LocZ INT, designID VARCHAR(40), buildingDirection VARCHAR(40), date VARCHAR(40), PRIMARY KEY (uuid))";
        PreparedStatement createBuildings = null;
        try {
            connection = hikari.getConnection();
            createBuildings = connection.prepareStatement(setupBuilding);
            createBuildings.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (createBuildings != null) {
                try {
                    createBuildings.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public final void setupNationTable() {
        
        Connection connection = null;
        String setupNations = "CREATE TABLE IF NOT EXISTS nations (name VARCHAR(40), ruler VARCHAR(40), members INT, barracks INT, cityHall INT, crusher INT, kiln INT, center INT,"
                + " centerUpg INT, smithy INT, forge INT, pier INT, granary INT, printing INT, library INT, PRIMARY KEY (name))";
        PreparedStatement createNations = null;
        try {
            connection = hikari.getConnection();
            createNations = connection.prepareStatement(setupNations);
            createNations.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (createNations != null) {
                try {
                    createNations.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }
    
    public final void setupNationBuildings(){
        
        Connection connection = null;
        String setupNations = "CREATE TABLE IF NOT EXISTS nationbuildings (name VARCHAR(40), iron INT, gold INT, emerald INT, diamond INT, redstone INT, coal INT, "
                + "stone INT, logs INT, ink INT, books INT, food INT, PRIMARY KEY (name))";
        PreparedStatement createNations = null;
        try {
            connection = hikari.getConnection();
            createNations = connection.prepareStatement(setupNations);
            createNations.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (createNations != null) {
                try {
                    createNations.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        
    }

}
