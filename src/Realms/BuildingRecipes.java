
package Realms;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author lanca
 */
public class BuildingRecipes {
    
    static NamespacedKey namespace = new NamespacedKey(RealmsAPI.getInstance(), "RealmsAPI");
    
    public static void registerRecipes(){
        
        barracksPermit();
        kilnPermit();
        centerPermit();
        smelteryPermit();
        centerUpgPermit();
        crusherPermit();
        pierPermit();
        granaryPermit();
        cityHallPermit();
        libraryPermit();
        printingPermit();
        swordsman();
        bowman();
        bookPress();
        smelteryRecipes();
    }
    
    public static void barracksPermit(){
        
        ItemStack barracksPermit = new ItemStack(Material.PAPER);
        ItemMeta meta = barracksPermit.getItemMeta();
        List<String> lore = new ArrayList<>();
        String lore1 = ChatColor.GOLD + "Nation Building Permit";
        String lore2 = ChatColor.BLUE + "Barracks";
        String lore3 = "Right Click on a Barracks Structure";
        lore.add(lore1);
        lore.add(lore2);
        lore.add(lore3);
        meta.setLore(lore);
        meta.setDisplayName("Nation Building Permit");
        barracksPermit.setItemMeta(meta);
        
        ShapelessRecipe check = new ShapelessRecipe(barracksPermit);
        check.addIngredient(Material.DIAMOND_SWORD);
        check.addIngredient(Material.PAPER);
        RealmsAPI.getInstance().getServer().addRecipe(check);
    }
    
    public static void kilnPermit(){
        ItemStack kilnPermit = new ItemStack(Material.PAPER);
        ItemMeta meta = kilnPermit.getItemMeta();
        List<String> lore = new ArrayList<>();
        String lore1 = ChatColor.GOLD + "Nation Building Permit";
        String lore2 = ChatColor.BLUE + "Kiln";
        String lore3 = "Right Click on a Kiln Structure";
        lore.add(lore1);
        lore.add(lore2);
        lore.add(lore3);
        meta.setLore(lore);
        meta.setDisplayName("Nation Building Permit");
        kilnPermit.setItemMeta(meta);
        
        ShapelessRecipe check1 = new ShapelessRecipe(kilnPermit);
        check1.addIngredient(Material.BRICK);
        check1.addIngredient(Material.PAPER);
        RealmsAPI.getInstance().getServer().addRecipe(check1);
    }
    
    public static void smelteryPermit(){
        ItemStack smelteryPermit = new ItemStack(Material.PAPER);
        ItemMeta meta = smelteryPermit.getItemMeta();
        List<String> lore = new ArrayList<>();
        String lore1 = ChatColor.GOLD + "Nation Building Permit";
        String lore2 = ChatColor.BLUE + "Smeltery";
        String lore3 = "Right Click on a Smeltery Structure";
        lore.add(lore1);
        lore.add(lore2);
        lore.add(lore3);
        meta.setLore(lore);
        meta.setDisplayName("Nation Building Permit");
        smelteryPermit.setItemMeta(meta);
        
        ShapelessRecipe check2 = new ShapelessRecipe(smelteryPermit);
        check2.addIngredient(Material.IRON_BLOCK);
        check2.addIngredient(Material.PAPER);
        RealmsAPI.getInstance().getServer().addRecipe(check2);        
    }
    
    public static void centerPermit(){
        ItemStack kilnPermit = new ItemStack(Material.PAPER);
        ItemMeta meta = kilnPermit.getItemMeta();
        List<String> lore = new ArrayList<>();
        String lore1 = ChatColor.GOLD + "Nation Building Permit";
        String lore2 = ChatColor.BLUE + "Center";
        String lore3 = "Right Click on a Town Center Structure";
        lore.add(lore1);
        lore.add(lore2);
        lore.add(lore3);
        meta.setLore(lore);
        meta.setDisplayName("Nation Building Permit");
        kilnPermit.setItemMeta(meta);
        
        ShapelessRecipe check3 = new ShapelessRecipe(kilnPermit);
        check3.addIngredient(Material.GLOWSTONE);
        check3.addIngredient(Material.PAPER);
        RealmsAPI.getInstance().getServer().addRecipe(check3);        
    }
    
    public static void centerUpgPermit(){
        ItemStack centerUpgPermit = new ItemStack(Material.PAPER);
        ItemMeta meta = centerUpgPermit.getItemMeta();
        List<String> lore = new ArrayList<>();
        String lore1 = ChatColor.GOLD + "Nation Building Permit";
        String lore2 = ChatColor.BLUE + "CenterUpg";
        String lore3 = "Right Click on a Town Center Upgrade Structure";
        lore.add(lore1);
        lore.add(lore2);
        lore.add(lore3);
        meta.setLore(lore);
        meta.setDisplayName("Nation Building Permit");
        centerUpgPermit.setItemMeta(meta);
        
        ShapelessRecipe check4 = new ShapelessRecipe(centerUpgPermit);
        check4.addIngredient(Material.BLAZE_ROD);
        check4.addIngredient(Material.PAPER);
        RealmsAPI.getInstance().getServer().addRecipe(check4);        
    }

    public static void crusherPermit(){
        ItemStack crusherPermit = new ItemStack(Material.PAPER);
        ItemMeta meta = crusherPermit.getItemMeta();
        List<String> lore = new ArrayList<>();
        String lore1 = ChatColor.GOLD + "Nation Building Permit";
        String lore2 = ChatColor.BLUE + "Crusher";
        String lore3 = "Right Click on a Crusher Structure";
        lore.add(lore1);
        lore.add(lore2);
        lore.add(lore3);
        meta.setLore(lore);
        meta.setDisplayName("Nation Building Permit");
        crusherPermit.setItemMeta(meta);
        
        ShapelessRecipe check5 = new ShapelessRecipe(crusherPermit);
        check5.addIngredient(Material.PISTON_BASE);
        check5.addIngredient(Material.PAPER);
        RealmsAPI.getInstance().getServer().addRecipe(check5);        
    }
 
    public static void pierPermit(){
        ItemStack pierPermit = new ItemStack(Material.PAPER);
        ItemMeta meta = pierPermit.getItemMeta();
        List<String> lore = new ArrayList<>();
        String lore1 = ChatColor.GOLD + "Nation Building Permit";
        String lore2 = ChatColor.BLUE + "Pier";
        String lore3 = "Right Click on a Pier Structure";
        lore.add(lore1);
        lore.add(lore2);
        lore.add(lore3);
        meta.setLore(lore);
        meta.setDisplayName("Nation Building Permit");
        pierPermit.setItemMeta(meta);
        
        ShapelessRecipe check6 = new ShapelessRecipe(pierPermit);
        check6.addIngredient(Material.WATER_BUCKET);
        check6.addIngredient(Material.PAPER);
        RealmsAPI.getInstance().getServer().addRecipe(check6);        
    }

    public static void granaryPermit(){
        ItemStack centerUpgPermit = new ItemStack(Material.PAPER);
        ItemMeta meta = centerUpgPermit.getItemMeta();
        List<String> lore = new ArrayList<>();
        String lore1 = ChatColor.GOLD + "Nation Building Permit";
        String lore2 = ChatColor.BLUE + "Granary";
        String lore3 = "Right Click on a Granary Structure";
        lore.add(lore1);
        lore.add(lore2);
        lore.add(lore3);
        meta.setLore(lore);
        meta.setDisplayName("Nation Building Permit");
        centerUpgPermit.setItemMeta(meta);
        
        ShapelessRecipe check7 = new ShapelessRecipe(centerUpgPermit);
        check7.addIngredient(Material.HAY_BLOCK);
        check7.addIngredient(Material.PAPER);
        RealmsAPI.getInstance().getServer().addRecipe(check7);        
    }

    public static void cityHallPermit(){
        ItemStack cityHallPermit = new ItemStack(Material.PAPER);
        ItemMeta meta = cityHallPermit.getItemMeta();
        List<String> lore = new ArrayList<>();
        String lore1 = ChatColor.GOLD + "Nation Building Permit";
        String lore2 = ChatColor.BLUE + "CityHall";
        String lore3 = "Right Click on a City Hall Structure";
        lore.add(lore1);
        lore.add(lore2);
        lore.add(lore3);
        meta.setLore(lore);
        meta.setDisplayName("Nation Building Permit");
        cityHallPermit.setItemMeta(meta);
        
        ShapelessRecipe check8 = new ShapelessRecipe(cityHallPermit);
        check8.addIngredient(Material.EMERALD_BLOCK);
        check8.addIngredient(Material.PAPER);
        RealmsAPI.getInstance().getServer().addRecipe(check8);        
    }

    public static void libraryPermit(){
        ItemStack libraryPermit = new ItemStack(Material.PAPER);
        ItemMeta meta = libraryPermit.getItemMeta();
        List<String> lore = new ArrayList<>();
        String lore1 = ChatColor.GOLD + "Nation Building Permit";
        String lore2 = ChatColor.BLUE + "Library";
        String lore3 = "Right Click on a Library Structure";
        lore.add(lore1);
        lore.add(lore2);
        lore.add(lore3);
        meta.setLore(lore);
        meta.setDisplayName("Nation Building Permit");
        libraryPermit.setItemMeta(meta);
        
        ShapelessRecipe check9 = new ShapelessRecipe(libraryPermit);
        check9.addIngredient(Material.BOOKSHELF);
        check9.addIngredient(Material.PAPER);
        RealmsAPI.getInstance().getServer().addRecipe(check9);        
    }

    public static void printingPermit(){
        ItemStack printingPermit = new ItemStack(Material.PAPER);
        ItemMeta meta = printingPermit.getItemMeta();
        List<String> lore = new ArrayList<>();
        String lore1 = ChatColor.GOLD + "Nation Building Permit";
        String lore2 = ChatColor.BLUE + "PrintingPress";
        String lore3 = "Right Click on a Printing Press Structure";
        lore.add(lore1);
        lore.add(lore2);
        lore.add(lore3);
        meta.setLore(lore);
        meta.setDisplayName("Nation Building Permit");
        printingPermit.setItemMeta(meta);
        
        ShapelessRecipe check0 = new ShapelessRecipe(printingPermit);
        check0.addIngredient(Material.BOOK);
        check0.addIngredient(Material.PAPER);
        RealmsAPI.getInstance().getServer().addRecipe(check0);        
    }
    
    public static void swordsman(){
        ItemStack Swordsman = new ItemStack(Material.PAPER);
        ItemMeta meta = Swordsman.getItemMeta();
        List<String> lore = new ArrayList<>();
        String lore1 = ChatColor.GOLD + "Guard Role";
        String lore2 = ChatColor.BLUE + "Swordsman";
        lore.add(lore1);
        lore.add(lore2);
        meta.setLore(lore);
        meta.setDisplayName("Guard Role Item");
        Swordsman.setItemMeta(meta);
        
        ShapelessRecipe check01 = new ShapelessRecipe(Swordsman);
        check01.addIngredient(Material.WOOD_SWORD);
        check01.addIngredient(Material.PAPER);
        RealmsAPI.getInstance().getServer().addRecipe(check01);  
    }

    public static void bowman(){
        ItemStack bowman = new ItemStack(Material.PAPER);
        ItemMeta meta = bowman.getItemMeta();
        List<String> lore = new ArrayList<>();
        String lore1 = ChatColor.GOLD + "Guard Role";
        String lore2 = ChatColor.BLUE + "Bowman";
        lore.add(lore1);
        lore.add(lore2);
        meta.setLore(lore);
        meta.setDisplayName("Guard Role Item");
        bowman.setItemMeta(meta);
        
        ShapelessRecipe check02 = new ShapelessRecipe(bowman);
        check02.addIngredient(Material.BOW);
        check02.addIngredient(Material.PAPER);
        RealmsAPI.getInstance().getServer().addRecipe(check02);  
    }
    
    public static void nameTag(){
        ItemStack nameTag = new ItemStack(Material.PAPER);
        ItemMeta meta = nameTag.getItemMeta();
        List<String> lore = new ArrayList<>();
        String lore1 = ChatColor.GOLD + "Guard Role";
        String lore2 = ChatColor.BLUE + "Name Tag";
        lore.add(lore1);
        lore.add(lore2);
        meta.setLore(lore);
        meta.setDisplayName("Guard Role Item");
        nameTag.setItemMeta(meta);
        
        ShapelessRecipe check03 = new ShapelessRecipe(nameTag);
        check03.addIngredient(Material.COAL_ORE);
        check03.addIngredient(Material.PAPER);
        RealmsAPI.getInstance().getServer().addRecipe(check03);  
    }
    
    public static void bookPress(){
        ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
        book.setAmount(9);
        ShapedRecipe check04 = new ShapedRecipe(book);
        check04.shape("CBB","BBB","BBB");
        check04.setIngredient('B', Material.BOOK);
        check04.setIngredient('C', Material.WRITTEN_BOOK);
        
        RealmsAPI.getInstance().getServer().addRecipe(check04);
    }
    
    public static void smelteryRecipes(){
        
        ItemStack dHorse = new ItemStack(Material.DIAMOND_BARDING);
        ShapedRecipe dHorseRecipe = new ShapedRecipe(dHorse);
        dHorseRecipe.shape("AAD","DDL","DLA");
        dHorseRecipe.setIngredient('A', Material.AIR);
        dHorseRecipe.setIngredient('D', Material.DIAMOND_BLOCK);
        dHorseRecipe.setIngredient('L', Material.LEATHER);
        RealmsAPI.getInstance().getServer().addRecipe(dHorseRecipe);
        
        ItemStack iHorse = new ItemStack(Material.IRON_BARDING);
        ShapedRecipe iHorseRecipe = new ShapedRecipe(iHorse);
        iHorseRecipe.shape("AAD","DDL","DLA");
        iHorseRecipe.setIngredient('A', Material.AIR);
        iHorseRecipe.setIngredient('D', Material.IRON_BLOCK);
        iHorseRecipe.setIngredient('L', Material.LEATHER);
        RealmsAPI.getInstance().getServer().addRecipe(iHorseRecipe);
        
        ItemStack gHorse = new ItemStack(Material.GOLD_BARDING);
        ShapedRecipe gHorseRecipe = new ShapedRecipe(gHorse);
        gHorseRecipe.shape("AAD","DDL","DLA");
        gHorseRecipe.setIngredient('A', Material.AIR);
        gHorseRecipe.setIngredient('D', Material.GOLD_BLOCK);
        gHorseRecipe.setIngredient('L', Material.LEATHER);
        RealmsAPI.getInstance().getServer().addRecipe(gHorseRecipe);
    }
    
}
