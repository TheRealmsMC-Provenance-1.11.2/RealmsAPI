package PlayerManager;


import NationManager.nNation;
import NationManager.nResources;
import chatapi.Utilities;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import org.bukkit.entity.Player;
import Realms.RealmsAPI;
import Realms.Util;
import java.util.HashMap;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author lanca
 */
public class PlayerManager {
    
    private RealmsAPI plugin;
    public HashMap<String, nPlayer> playerList;
    
    public PlayerManager(RealmsAPI plugin)
    {
        this.plugin = plugin;
        this.playerList = new HashMap<String, nPlayer>();
    }
    
 
    public void addPlayer(Player player) {
        
        String uuid = player.getUniqueId().toString();
        String name = player.getDisplayName();
        String discord = "none";
        String token = generateToken();
        String start = Utilities.getDate();
        String nation = "none";
        int role = 0;
        
        Connection connection = null;
        String save = "INSERT IGNORE INTO players(uuid,name,start,nation,role,discord,token) VALUES(?,?,?,?,?,?,?)";
        PreparedStatement savePlayer = null;
        
        try{
            connection = this.plugin.getHikari().getConnection();
            
            savePlayer = connection.prepareStatement(save);
            savePlayer.setString(1, uuid);
            savePlayer.setString(2, name);
            savePlayer.setString(3, start);
            savePlayer.setString(4, nation);
            savePlayer.setInt(5, role);
            savePlayer.setString(6, discord);
            savePlayer.setString(7, token);
            savePlayer.execute();
            
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null){
                try{
                    connection.close();
                } catch(SQLException e) {
                    e.printStackTrace();
                }
            }
            
            if (savePlayer != null) {
                try {
                    savePlayer.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } 
    }
    
    public String generateToken() {
        char[] symbols;
        char[] buf = new char[10];

        StringBuilder tmp = new StringBuilder();
        for (char ch = '0'; ch <= '9'; ++ch) {
            tmp.append(ch);
        }
        for (char ch = 'a'; ch <= 'z'; ++ch) {
            tmp.append(ch);
        }
        symbols = tmp.toString().toCharArray();

        Random random = new Random();

        for (int idx = 0; idx < buf.length; ++idx) {
            buf[idx] = symbols[random.nextInt(symbols.length)];
        }

        return new String(buf);
    }
    
    public void loadPlayer(String uuidLoad) {

        String uuid;
        String start;
        String nation;
        int role;

        Connection connection = null;
        String load = "SELECT * FROM players WHERE uuid=?";
        PreparedStatement loadPlayer = null;

        try {
            connection = this.plugin.getHikari().getConnection();

            loadPlayer = connection.prepareStatement(load);
            loadPlayer.setString(1, uuidLoad);
            ResultSet r = loadPlayer.executeQuery();

            if (!r.next()) {
                r.close();
            } else {
                    uuid = r.getString("uuid");
                    start = r.getString("start");
                    nation = r.getString("nation");
                    role = r.getInt("role");
                    
                    nPlayer player = new nPlayer(uuid, start, nation, role);
                    this.playerList.put(uuid, player);
                    
                    while (r.next()) {
                        uuid = r.getString("uuid");
                        start = r.getString("start");
                        nation = r.getString("nation");
                        role = r.getInt("role");
                    
                        player = new nPlayer(uuid, start, nation, role);
                        this.playerList.put(uuid, player);
                    
                    }
                r.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    
    public void kickPlayer(String name, Player player, String nation) {
        Connection connection = null;
        String uuid = null;
        OfflinePlayer op = Bukkit.getOfflinePlayer(name);
        if(op.hasPlayedBefore())
        {
            uuid = op.getUniqueId().toString();
            
            String kick = "INSERT INTO players(uuid,nation,role) VALUES (?,?,?) ON DUPLICATE KEY UPDATE nation=?, role=?";
            PreparedStatement kickPlayer = null;
            
            try{
                connection = this.plugin.getHikari().getConnection();
                kickPlayer = connection.prepareStatement(kick);
                kickPlayer.setString(1, uuid);
                kickPlayer.setString(2, "none");
                kickPlayer.setInt(3, 0);
                kickPlayer.setString(4, "none");
                kickPlayer.setInt(5, 0);
                kickPlayer.executeQuery();
                
                Utilities.sendGood(player, name + " has been kicked from your nation");
                
                for (nNation kickNation : plugin.getNationManager().nationList) {
                    if (kickNation.getName().equals(nation)) {
                        kickNation.setMembers(kickNation.getMembers() - 1);
                    }
                }
                
                
            } catch(SQLException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

                if (kickPlayer != null) {
                    try {
                        kickPlayer.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
            
            
            
        } else {
            Utilities.sendBad(player, name + " is either invalid or never played before");
        }
        
        
        
    }
    
    public void savePlayer(String uuid) {

        String nation = "none";
        int role = 0;
        nPlayer check = null;
        

        check = this.playerList.get(uuid);

        if(check != null)
        {
            nation = check.getNation();
            role = check.getRole();
            this.playerList.remove(uuid);

            Connection connection = null;
            String save = "INSERT INTO players(uuid,nation,role) VALUES (?,?,?) ON DUPLICATE KEY UPDATE nation=?, role=?";
            PreparedStatement savePlayer = null;

            try {
                connection = this.plugin.getHikari().getConnection();

                savePlayer = connection.prepareStatement(save);
                savePlayer.setString(1, uuid);
                savePlayer.setString(2, nation);
                savePlayer.setInt(3, role);
                savePlayer.setString(4, nation);
                savePlayer.setInt(5, role);
                savePlayer.execute();

            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

                if (savePlayer != null) {
                    try {
                        savePlayer.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
              
    }
    
    public int checkRole(UUID uuid){
        int role = 0;
        
        nPlayer check = this.playerList.get(uuid.toString());
        if(check != null){
            role = check.getRole();
        }
        
        return role;
    }
    
    public String checkNation(UUID uuid){
        String nation = "none";
        
        nPlayer check = this.playerList.get(uuid.toString());
        if(check != null){
            nation = check.getNation();
        }
        return nation;
    }
    
    public void leaveNation(UUID uuid){
        String currentNation = "none";
        
        nPlayer check = this.playerList.get(uuid.toString());
        if(check != null){
                currentNation = check.getNation();
                check.setNation("none");            
        }
        
        Utilities.sendWarn(RealmsAPI.getInstance().getServer().getPlayer(uuid), 
                "You have left " + currentNation);
        
        nNation oldNation = null;
        
        for (nNation nation : plugin.getNationManager().nationList)
        {
            if(nation.getName().equals(currentNation))
            {
               nation.setMembers(nation.getMembers() - 1);
               oldNation = nation;
            }
        }
        
        if(oldNation != null)
        {
            if(oldNation.getMembers() <= 0)
            {
                plugin.getNationManager().nationList.remove(oldNation);
                plugin.getNationManager().nationDeletions.add(oldNation);
                Utilities.sendBad(RealmsAPI.getInstance().getServer().getPlayer(uuid), 
                        "Your old Nation has been removed as you were the last member");
            }
        } 
    }
    
    public void addNation(nPlayer player, String nationChange) {
        
        if(!nationChange.equalsIgnoreCase("none"))
        {
            for(nNation nation : plugin.getNationManager().nationList)
            {
                if(nation.getName().equalsIgnoreCase(nationChange))
                {
                    nation.setMembers(nation.getMembers() + 1);
                    
                    player.setNation(nationChange);
                }
            }
        } 
    }
    
    public Boolean depositItem(Player player, ItemStack item)
    {

        String nation = "none";
        
        nPlayer check = this.playerList.get(player.getUniqueId().toString());
        if(check != null){
            nation = check.getNation();            
        }       
        if(nation.equalsIgnoreCase("none"))
        {
            return false;
        }
        nResources resourceNation = null;
        for(nResources checking : this.plugin.getNationManager().nationResources)
        {
            if(checking.getName().equalsIgnoreCase(nation))
            {
                resourceNation = checking;
            }
        }
        if(resourceNation == null)
        {
            return false;
        }
        if(item.getType().equals(Material.IRON_INGOT)){
            resourceNation.setIron(resourceNation.getIron() + item.getAmount());
            return true;
        }        
        if(item.getType().equals(Material.GOLD_INGOT)){
            resourceNation.setGold(resourceNation.getGold() + item.getAmount());
            return true;
        }
        if(item.getType().equals(Material.EMERALD)) {
            resourceNation.setEmerald(resourceNation.getEmerald() + item.getAmount());
            return true;
        }
        if(item.getType().equals(Material.DIAMOND)){
            resourceNation.setDiamond(resourceNation.getDiamond() + item.getAmount());
            return true;
        }
        if(item.getType().equals(Material.REDSTONE)){
            resourceNation.setRedstone(resourceNation.getRedstone() + item.getAmount());
            return true;
        }
        if(item.getType().equals(Material.COAL)){
            resourceNation.setCoal(resourceNation.getCoal() + item.getAmount());
            return true;
        }
        if(item.getType().equals(Material.STONE)){
            resourceNation.setStone(resourceNation.getStone() + item.getAmount());
            return true;
        }
        if(item.getType().equals(Material.LOG) || item.getType().equals(Material.LOG_2)){
            resourceNation.setLogs(resourceNation.getLogs() + item.getAmount());
            return true;
        }
        if(item.getType().equals(Material.INK_SACK)){
            resourceNation.setInk(resourceNation.getInk() + item.getAmount());
            return true;
        }
        if(item.getType().equals(Material.BOOK)){
            resourceNation.setBooks(resourceNation.getBooks() + item.getAmount());
            return true;
        }
        int food = 0;
        if(Util.getHunger(item) > 0)
        {
            food = Util.getHunger(item);
            food = food * item.getAmount();
            resourceNation.setFood(resourceNation.getFood() + food);
            return true;
        }
        
        return false;
    }
    
    public class nPlayer {
        public String uuid;
        public String start;
        public String nation;
        public int role;
        
        nPlayer(String uuid, String start, String nation, int role){
            this.uuid = uuid;
            this.start = start;
            this.nation = nation;
            this.role = role;
        }
        
        public void setUUID(String uuid) {
            this.uuid = uuid;
        }
        
        public String getUUID(){
            return this.uuid;
        }
        
        public void setStart(String start) {
            this.start = start;
        }
        
        public String getStart(){
            return this.start;
        }
        
        public void setNation(String nation) {
            this.nation = nation;
        }
        
        public String getNation() {
            return this.nation;
        }
        
        public void setRole(int role) {
            this.role = role;
        }
        
        public int getRole(){
            return this.role;
        }
    }
    
}
