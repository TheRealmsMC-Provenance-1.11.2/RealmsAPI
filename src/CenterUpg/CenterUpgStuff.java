package CenterUpg;


import Realms.RealmsAPI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;


public class CenterUpgStuff implements Comparable<CenterUpgStuff> {

    static public ConcurrentHashMap<TownsXZ, CenterUpgStuff> xz2Ant = new ConcurrentHashMap<TownsXZ, CenterUpgStuff>();

    final TownsXZ xz;
    final int baseY;
    int tipY;
    int BroadcastRadius = 50;
    String nation;
    

    // Normal antenna creation method
    public CenterUpgStuff(Location loc, String owner) {
        xz = new TownsXZ(loc);
        baseY = (int)loc.getY();
        tipY = baseY;
        nation = owner;
        xz2Ant.put(xz, this);
    }

    public static CenterUpgStuff getAntenna(Location loc) {
        return getAntenna(new TownsXZ(loc));
    }
    public static CenterUpgStuff getAntenna(TownsXZ loc) {
        CenterUpgStuff a = xz2Ant.get(loc);
        return a;
    }

    // Get an antenna by base directly adjacent to given location
    public static CenterUpgStuff getAntennaByAdjacent(Location loc) {
        for (int x = -1; x <= 1; x += 1) {
            for (int z = -1; z <= 1; z += 1) {
                CenterUpgStuff ant = getAntenna(loc.clone().add(x+0.5, 0, z+0.5));
                if (ant != null) {
                    return ant;
                }
            }
        }
        return null;
    }

    
    public Location getBaseLocation() {
        return xz.getLocation(baseY);
    }


    public boolean withinReceiveRange(Location receptionLoc, int receptionRadius) {
        if (!xz.world.equals(receptionLoc.getWorld())) {
            // No cross-world communication... yet! TODO: how?
            return false;
        }
       
        // Sphere intersection of broadcast range from source
        return getBaseLocation().distanceSquared(receptionLoc) < square(BroadcastRadius + receptionRadius);
    }

    // Square a number, returning a double as to not overflow if x>sqrt(2**31)
    private static double square(int x) {
        return (double)x * (double)x;
    }

    // Get 3D distance from tip
    public int getDistance(Location receptionLoc) {
        return (int)Math.sqrt(getBaseLocation().distanceSquared(receptionLoc));
    }

    // Get 2D distance from antenna xz
    public double get2dDistance(Location otherLoc) {
        Location otherLoc2d = otherLoc.clone();
        Location baseLoc = getBaseLocation();

        otherLoc2d.setY(baseLoc.getY());

        return baseLoc.distance(otherLoc2d);
    }

    // Return whether antenna is in same world as other location
    public boolean inSameWorld(Location otherLoc) {
        return xz.world.equals(otherLoc.getWorld());
    }
    
    public String getNation(){
        return nation;
    }

  
    static public Location moveSignals(Player player, Location receptionLoc, int receptionRadius) {
        
        int count = 0;
        Location Nearest = null;
        List<CenterUpgStuff> nearbyAnts = new ArrayList<CenterUpgStuff>();

        for (Map.Entry<TownsXZ,CenterUpgStuff> pair : CenterUpgStuff.xz2Ant.entrySet()) {
            CenterUpgStuff otherAnt = pair.getValue();

            if (otherAnt.withinReceiveRange(receptionLoc, receptionRadius)) {
                int distance = otherAnt.getDistance(receptionLoc);
                if (distance == 0) {
                    continue;
                }
                nearbyAnts.add(otherAnt);
            }
        }

        Collections.sort(nearbyAnts, new TownsDistanceComparator(receptionLoc));
        count = nearbyAnts.size();
        if (count == 0) {
        } else  {
                Integer targetInteger = RealmsAPI.getInstance().getListeners().playerTargets.get(player.getUniqueId());
                int targetInt;
                if (targetInteger == null) {
                    targetInt = 0;
                } else {
                    targetInt = Math.abs(targetInteger.intValue()) % count;
                }
                CenterUpgStuff antLoc = nearbyAnts.get(targetInt);
                Nearest = antLoc.getBaseLocation();
                
        }
        return Nearest;
    }
    
    static public Location moveSignals(LivingEntity player, Location receptionLoc, int receptionRadius) {

        int count = 0;
        Location Nearest = null;
        List<CenterUpgStuff> nearbyAnts = new ArrayList<CenterUpgStuff>();

        for (Map.Entry<TownsXZ, CenterUpgStuff> pair : CenterUpgStuff.xz2Ant.entrySet()) {
            CenterUpgStuff otherAnt = pair.getValue();

            if (otherAnt.withinReceiveRange(receptionLoc, receptionRadius)) {
                int distance = otherAnt.getDistance(receptionLoc);
                if (distance == 0) {
                    continue;
                }
                nearbyAnts.add(otherAnt);
            }
        }

        Collections.sort(nearbyAnts, new TownsDistanceComparator(receptionLoc));
        count = nearbyAnts.size();
        if (count == 0) {
        } else {
            Integer targetInteger = RealmsAPI.getInstance().getListeners().playerTargets.get(player.getUniqueId());
            int targetInt;
            if (targetInteger == null) {
                targetInt = 0;
            } else {
                targetInt = Math.abs(targetInteger.intValue()) % count;
            }
            CenterUpgStuff antLoc = nearbyAnts.get(targetInt);
            Nearest = antLoc.getBaseLocation();

        }
        return Nearest;
    }
       
    // Delegate comparison to location
    public int compareTo(CenterUpgStuff otherAnt) {
        return xz.compareTo(otherAnt.xz);
    }
}

class TownsXZ implements Comparable<TownsXZ> 
{
    World world;
    int x, z;

    public TownsXZ(World w, int x0,  int z0) {
        world = w;
        x = x0;
        z = z0;
    }

    public TownsXZ(Location loc) {
        world = loc.getWorld();
        x = loc.getBlockX();
        z = loc.getBlockZ();
    }

    public Location getLocation(double y) {
        return new Location(world, x + 0.5, y, z + 0.5);
    }

    public String toString() {
        return x + "," + z;
    }

    @Override
    public int compareTo(TownsXZ rhs) {
        if (!world.equals(rhs.world)) {
            return world.getName().compareTo(rhs.world.getName());
        }

        if (x - rhs.x != 0) {
            return x - rhs.x;
        } else if (z - rhs.z != 0) {
            return z - rhs.z;
        }

        return 0;
    }

    public boolean equals(Object obj) {
        return compareTo((TownsXZ) obj) == 0;      // why do I have to do this myself?
    }

    public int hashCode() {
        // lame hashing TODO: improve?
        return x * z;
    }
}



class TownsDistanceComparator implements Comparator<CenterUpgStuff> {
    Location otherLoc;

    public TownsDistanceComparator(Location otherLoc) {
        this.otherLoc = otherLoc;
    }

    public int compare(CenterUpgStuff a, CenterUpgStuff b) {
        return a.getDistance(otherLoc) - b.getDistance(otherLoc);
    }
}