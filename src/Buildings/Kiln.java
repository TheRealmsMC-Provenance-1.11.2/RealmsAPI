package Buildings;

import BuildingManager.Building;
import PlayerManager.PlayerManager.nPlayer;
import java.util.Random;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.FurnaceSmeltEvent;
import org.bukkit.inventory.ItemStack;
import Realms.RealmsAPI;
import chatapi.Utilities;
import java.util.HashSet;
import java.util.logging.Level;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.PrepareItemCraftEvent;

/**
 *
 * @author lanca
 */
public class Kiln implements Listener {

    private RealmsAPI plugin;

    public Kiln(RealmsAPI plugin) {
        this.plugin = plugin;
    }

    //KILN SMELTING
    @EventHandler
    public void onKilnSmelt(FurnaceSmeltEvent e) {
        Location loc = e.getBlock().getLocation();
        this.plugin.debug("Burn Event");

        Building building = plugin.getBuildingManager().getBuildingFromStorage(loc);
        if (isSalvage(e.getSource())) {
            e.setCancelled(true);
        }
        if (building != null) {

            if (building.getDesignID().equalsIgnoreCase("kiln")) {
                ItemStack result = e.getResult();
                if (result.getType().equals(Material.GLASS) || result.getType().equals(Material.CLAY_BRICK)
                        || result.getType().equals(Material.NETHER_BRICK_ITEM)
                        || result.getType().equals(Material.STONE)
                        || result.getType().equals(Material.HARD_CLAY)) {

                    Random rand = new Random();
                    int n = rand.nextInt(100) + 1;
                    if (n >= 55) {
                        int amount = result.getAmount();
                        result.setAmount(amount + (rand.nextInt(3) + 1));
                        e.setResult(result);

                    }
                }
            }

            if (building.getDesignID().equalsIgnoreCase("smeltery")) {

                if (isSalvage(e.getSource())) {
                    e.setCancelled(false);


                    ItemStack result = e.getResult();
                    if(result.getType().equals(Material.IRON_NUGGET))
                    {
                        result.setType(Material.IRON_INGOT);
                    }
                    if(result.getType().equals(Material.GOLD_NUGGET))
                    {
                        result.setType(Material.GOLD_INGOT);
                    }
                    
                    int max = result.getAmount();
                    if(e.getSource().getType().equals(Material.IRON_HELMET) || e.getSource().getType().equals(Material.GOLD_HELMET) || e.getSource().getType().equals(Material.CHAINMAIL_HELMET))
                    {
                        max = 2;
                    }
                    if(e.getSource().getType().equals(Material.IRON_BOOTS) || e.getSource().getType().equals(Material.GOLD_BOOTS) || e.getSource().getType().equals(Material.CHAINMAIL_BOOTS))
                    {
                        max = 3;
                    }
                    if(e.getSource().getType().equals(Material.IRON_LEGGINGS) || e.getSource().getType().equals(Material.GOLD_LEGGINGS) || e.getSource().getType().equals(Material.CHAINMAIL_LEGGINGS))
                    {
                        max = 4;
                    }
                    if(e.getSource().getType().equals(Material.IRON_CHESTPLATE) || e.getSource().getType().equals(Material.GOLD_CHESTPLATE) || e.getSource().getType().equals(Material.CHAINMAIL_CHESTPLATE))
                    {
                        max = 5;
                    }


                    double percentage = (e.getSource().getType().getMaxDurability() - e.getSource().getDurability());
                    percentage = percentage/e.getSource().getType().getMaxDurability();
                    //int test = (int) (max * percentage);
                    //this.plugin.getLogger().log(Level.SEVERE, "Max is: {0} Current: {1} Percentage: {2} Test: {3}", new Object[]{e.getSource().getType().getMaxDurability(), e.getSource().getDurability(), percentage, test});
                    if(Double.isNaN(percentage)){
                        percentage = 1.0D;
                    } else if (percentage < 0)
                    {
                        percentage = 0.0D;
                    }
                    
                    int amount = (int)(max * percentage);
                    if(amount == 0)
                    {
                        amount = 1;
                    }
                    
                    result.setAmount(amount);
                    e.setResult(result);
                }
                if (isOre(e.getSource())) {
                    ItemStack result = e.getResult();
                    Random rand = new Random();
                    int n = rand.nextInt(100) + 1;
                    if (n >= 65) {
                        int amount = result.getAmount();
                        result.setAmount(amount + (rand.nextInt(2) + 1));
                        e.setResult(result);

                    }
                }
            }
        }
    }

    @EventHandler
    public void onInventoryOpen(InventoryOpenEvent e) {
        Player player = this.plugin.getServer().getPlayer(e.getPlayer().getName());
        if(e.getInventory().getType().equals(InventoryType.ENDER_CHEST))
        {
            return;
        }
        Location loc = e.getInventory().getLocation();
        if(loc != null)
        {
            Building kiln = this.plugin.getBuildingManager().getBuildingFromStorage(loc);
            if (kiln != null) {
                String kilnNation = kiln.getNation();
                nPlayer test = this.plugin.getPlayerManager().playerList.get(e.getPlayer().getUniqueId().toString());
                if (test != null) {
                    if (!test.getNation().equalsIgnoreCase(kilnNation)) {
                        Utilities.sendBad(player, "Unable to Access. You are not part of this Nation");
                        e.setCancelled(true);
                    }
                }
            }          
        }

    }

    @EventHandler
    public void craftingEvent(CraftItemEvent e) {
        
        
        Player player = (Player) e.getInventory().getHolder();
        Location loc = player.getTargetBlock(null, 10).getLocation();
        Building building = plugin.getBuildingManager().getBuildingFromStorage(loc);
        if (building != null) {
            
            nPlayer check = this.plugin.getPlayerManager().playerList.get(player.getUniqueId().toString());
            if(check != null){
                if(check.getNation().equalsIgnoreCase(building.getNation()))
                {
                    if (building.getDesignID().equalsIgnoreCase("smeltery")) {
                        
                        ItemStack result = e.getRecipe().getResult();
                        if (isSalvage(result) || result.getType().equals(Material.IRON_SWORD) || result.getType().equals(Material.GOLD_SWORD)
                                || result.getType().equals(Material.DIAMOND_SWORD)) {
                            
                            Random rand = new Random();
                            int n = rand.nextInt(100) + 1;
                            if (n >= 50) {
                                
                                int amount = result.getAmount();
                                result.setAmount(amount + (rand.nextInt(2) + 1));
                                e.getInventory().setResult(result);
                            }
                        }
                        if(result.getType().equals(Material.RAILS))
                        {
                                result.setAmount(64);
                                e.getInventory().setResult(result);                            
                        }   
                    }                   
                }
            }
        } else {
            this.plugin.debug("Smeltery Precraft building null :S");
        }
    }
    
//    @EventHandler
//    public void preCraftingStuff(PrepareItemCraftEvent e)
//    {
//        if(e.getRecipe().getResult() != null)
//        {
//            ItemStack result = e.getRecipe().getResult();
//
//            if (result.getType().equals(Material.DIAMOND_BARDING) || result.getType().equals(Material.IRON_BARDING) || result.getType().equals(Material.GOLD_BARDING)) {
//                Player player = (Player) e.getInventory().getHolder();
//                Location loc = player.getTargetBlock(null, 10).getLocation();
//                Building building = plugin.getBuildingManager().getBuildingFromStorage(loc);
//                if (building == null) {
//                    e.getInventory().setResult(null);
//                }
//                if (building != null) {
//                    if (!building.getDesignID().equalsIgnoreCase("smeltery")) {
//                        e.getInventory().setResult(null);
//                    }
//                }
//            }
//            
//        }
//    }

    public Boolean isOre(ItemStack item) {
        if (item.getType().equals(Material.IRON_ORE) || item.getType().equals(Material.GOLD_ORE) || item.getType().equals(Material.DIAMOND_ORE)
                || item.getType().equals(Material.EMERALD_ORE)) {
            return true;
        } else {
            return false;
        }
    }

    public Boolean isSalvage(ItemStack item) {

        if (item.getType().equals(Material.DIAMOND_BOOTS) || item.getType().equals(Material.DIAMOND_CHESTPLATE) || item.getType().equals(Material.DIAMOND_LEGGINGS)
                || item.getType().equals(Material.DIAMOND_HELMET)) {
            return true;

        } else if (item.getType().equals(Material.IRON_BOOTS) || item.getType().equals(Material.IRON_CHESTPLATE) || item.getType().equals(Material.IRON_LEGGINGS)
                || item.getType().equals(Material.IRON_HELMET)) {
            return true;

        } else if (item.getType().equals(Material.GOLD_BOOTS) || item.getType().equals(Material.GOLD_CHESTPLATE) || item.getType().equals(Material.GOLD_LEGGINGS)
                || item.getType().equals(Material.GOLD_HELMET)) {
            return true;
        } else if (item.getType().equals(Material.CHAINMAIL_BOOTS) || item.getType().equals(Material.CHAINMAIL_CHESTPLATE) || item.getType().equals(Material.CHAINMAIL_LEGGINGS)
                || item.getType().equals(Material.CHAINMAIL_HELMET)) {
            return true;
        }

        return false;
    }
}
