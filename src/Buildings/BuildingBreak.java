package Buildings;

import BuildingManager.Building;
import Realms.RealmsAPI;
import RealmsGuards.SentryInstance;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;

/**
 *
 * @author lanca
 */
public class BuildingBreak {
    
    private RealmsAPI plugin;

    public BuildingBreak(RealmsAPI plugin) {
        this.plugin = plugin;
    }
  
    public void breakBarracks(Building building) {
        int[] offset = {-6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6};
        

        World world = building.getWorldBukkit();
        Location loc = new Location(building.getWorldBukkit(), building.getOffset().getBlockX(), building.getOffset().getBlockY(), building.getOffset().getBlockZ());
        int baseX = loc.getChunk().getX();
        int baseZ = loc.getChunk().getZ();

        for (int x : offset) {
            for (int z : offset) {
                Chunk chunk = world.getChunkAt(baseX + x, baseZ + z);
                
                Boolean load = false;
                if(!chunk.isLoaded())
                {
                    load = true;
                    chunk.load();                    
                }
                
                for(Entity entity : chunk.getEntities())
                {
                    if(entity.hasMetadata("NPC"))
                    {
                        SentryInstance inst = this.plugin.getGuardManager().getSentry(entity);
                        if(inst != null)
                        {
                            NPC newGuard = CitizensAPI.getNPCRegistry().getNPC(entity);
                            newGuard.destroy();
                        }
                    }
                }
                if(load)
                {
                    chunk.unload();
                }
            }
        }
    }
}
