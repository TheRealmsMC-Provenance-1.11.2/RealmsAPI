package Buildings;

import Center.CenterStuff;
import BuildingManager.Building;
import CenterUpg.CenterUpgStuff;
import Granary.GranaryStuff;
import Pier.PierStuff;
import org.bukkit.event.Listener;
import Realms.RealmsAPI;
import RealmsGuards.SentryInstance;
import RealmsGuards.SentryTrait;
import chatapi.Utilities;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.trait.trait.Equipment;
import net.citizensnpcs.api.trait.trait.Inventory;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;


/**
 *
 * @author lanca
 */
public class BuildingCreate{

    private RealmsAPI plugin;

    public BuildingCreate(RealmsAPI plugin) {
        this.plugin = plugin;
    }

    public void createBarracks(Player player, Building building){
        
        Utilities.sendGood(player, "Barracks Built");
        this.plugin.getBuildingManager().createBuilding(building);
        spawnGuard(player.getLocation().add(1,0,0), building.getNation());
        spawnGuard(player.getLocation().add(0,0,1), building.getNation());
        spawnGuard(player.getLocation().add(2,0,0), building.getNation());
        spawnGuard(player.getLocation(), building.getNation());     
    }
    
    public void createKiln(Player player, Building building){
        Utilities.sendGood(player, "Kiln Built");
        this.plugin.getBuildingManager().createBuilding(building);
    }
    
    public void createSmeltery(Player player, Building building){
        Utilities.sendGood(player, "Smelter Built");
        this.plugin.getBuildingManager().createBuilding(building);
    }
    
    public void createCenter(Player player, Building building){
        Utilities.sendGood(player, "Town Center Built");
        this.plugin.getBuildingManager().createBuilding(building);
        Location loc = new Location(building.getWorldBukkit(),building.getOffset().getBlockX(),building.getOffset().getBlockY(),building.getOffset().getBlockZ());
        CenterStuff antLoc = new CenterStuff(loc, building.getNation());
    }
    
    public void createCenterUpg(Player player, Building building){
        Utilities.sendGood(player, "Upgraded Town Center Built");
        this.plugin.getBuildingManager().createBuilding(building);
        Location loc = new Location(building.getWorldBukkit(),building.getOffset().getBlockX(),building.getOffset().getBlockY(),building.getOffset().getBlockZ());
        CenterUpgStuff antLoc = new CenterUpgStuff(loc, building.getNation());
    }
    
    public void createCrusher(Player player, Building building){
        Utilities.sendGood(player, "Crusher Built");
        this.plugin.getBuildingManager().createBuilding(building);        
    }
    
    public void createPier(Player player, Building building){
        Utilities.sendGood(player, "Pier Built");
        this.plugin.getBuildingManager().createBuilding(building);
        Location loc = new Location(building.getWorldBukkit(),building.getOffset().getBlockX(),building.getOffset().getBlockY(),building.getOffset().getBlockZ());
        PierStuff antLoc = new PierStuff(loc, building.getNation());
    }
    
    public void createGranary(Player player, Building building){
        Utilities.sendGood(player, "Granary Built");
        this.plugin.getBuildingManager().createBuilding(building);
        Location loc = new Location(building.getWorldBukkit(),building.getOffset().getBlockX(),building.getOffset().getBlockY(),building.getOffset().getBlockZ());
        GranaryStuff antLoc = new GranaryStuff(loc, building.getNation());
    }
    
    public void createCityHall(Player player, Building building) {
        Utilities.sendGood(player, "City Hall Built");
        this.plugin.getBuildingManager().createBuilding(building);
    }

    public void createLibrary(Player player, Building building) {
        Utilities.sendGood(player, "Library Built");
        this.plugin.getBuildingManager().createBuilding(building);
    }

    public void createPrintingPress(Player player, Building building) {
        Utilities.sendGood(player, "Printing Press Built");
        this.plugin.getBuildingManager().createBuilding(building);
    }
    public void spawnGuard(Location loc, String nation) {

        String guardName = "New Guard";
        NPC newGuard = CitizensAPI.getNPCRegistry().createNPC(EntityType.PLAYER, guardName);
        newGuard.addTrait(SentryTrait.class);
        newGuard.addTrait(Equipment.class);
        newGuard.addTrait(Inventory.class);
        newGuard.data().setPersistent(NPC.PLAYER_SKIN_UUID_METADATA, "scarecr0w");
        newGuard.data().setPersistent(NPC.PLAYER_SKIN_USE_LATEST, false);
        newGuard.spawn(loc);
        SentryInstance inst = newGuard.getTrait(SentryTrait.class).getInstance();
        inst.setGroup(nation);

        String ignore = "NATION:" + nation.toUpperCase();
        if (!inst.containsIgnore(ignore.toUpperCase())) {
            inst.ignoreTargets.add(ignore.toUpperCase());
        }
        inst.processTargets();
        inst.setTarget(null, false);
        inst.sentryStatus = SentryInstance.Status.isLOOKING;
        inst.sentryRange = 40;
        inst.RespawnDelaySeconds = 300;
        

        ItemStack helm = new ItemStack(Material.DIAMOND_HELMET, 1);
        ItemStack chest = new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
        ItemStack leggings = new ItemStack(Material.DIAMOND_LEGGINGS, 1);
        ItemStack boots = new ItemStack(Material.DIAMOND_BOOTS, 1);
        ItemStack sword = new ItemStack(Material.DIAMOND_SWORD, 1);

        this.plugin.getGuardManager().equip(newGuard, helm);
        this.plugin.getGuardManager().equip(newGuard, chest);
        this.plugin.getGuardManager().equip(newGuard, leggings);
        this.plugin.getGuardManager().equip(newGuard, boots);
        this.plugin.getGuardManager().equip(newGuard, sword);

    }


}
