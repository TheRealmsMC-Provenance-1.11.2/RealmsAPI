package NationManager;

import BuildingManager.BuildingDesign;
import NationManager.TableGenerator.Alignment;
import NationManager.TableGenerator.Receiver;
import PlayerManager.PlayerManager;
import chatapi.Utilities;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import Realms.RealmsAPI;

/**
 *
 * @author lanca
 */
public class NationManager {

    private RealmsAPI plugin;
    public List<nNation> nationList;
    public List<nResources> nationResources;
    public List<nNation> nationDeletions;
//    public HashMap<String, Integer> maxBuilds;

    public NationManager(RealmsAPI plugin) {
        this.plugin = plugin;
        this.nationList = new ArrayList<>();
        this.nationResources = new ArrayList<>();
        this.nationDeletions = new ArrayList<>();
        //this.maxBuilds = new HashMap<>();

        loadNations();
        sortNationList();
        loadResources();
        //loadMaxBuild();

    }

    public void sortNationList() {

        Collections.sort(this.nationList, new TopNationsComparator());
    }

    public void printNationList(Player player) {
        Utilities.sendNormal(player, "List of the Current Nations");

        TableGenerator tg = new TableGenerator(Alignment.LEFT, Alignment.RIGHT, Alignment.RIGHT);

        tg.addRow(ChatColor.GOLD + " NATION ", ChatColor.GOLD + " MEMBERS ", ChatColor.GOLD + " RULER ");

        for (nNation nation : this.nationList) {
            tg.addRow(nation.getName(), String.valueOf(nation.getMembers()), nation.getRuler());
        }
        for (String line : tg.generate(Receiver.CLIENT, true, true)) {
            player.sendMessage(line);
        }
    }

    public void printNationBuildings(Player player, String nation) {
        Utilities.sendNormal(player, "List of " + nation + " buildings");

        TableGenerator tg = new TableGenerator(Alignment.LEFT, Alignment.RIGHT);
        tg.addRow(ChatColor.GOLD + "BUILDING", ChatColor.GOLD + "AMOUNT");
        for (nNation buildNation : this.nationList) {
            if (buildNation.getName().equalsIgnoreCase(nation)) {
                tg.addRow("Barracks", String.valueOf(buildNation.getBarracks()));
                tg.addRow("City Hall", String.valueOf(buildNation.getCityHall()));
                tg.addRow("Crusher", String.valueOf(buildNation.getCrusher()));
                tg.addRow("Kiln", String.valueOf(buildNation.getKiln()));
                tg.addRow("Town Center", String.valueOf(buildNation.getCenter()));
                tg.addRow("UpgTown Center", String.valueOf(buildNation.getCenterUpg()));
                tg.addRow("Smeltery", String.valueOf(buildNation.getSmithy()));
                tg.addRow("Pier", String.valueOf(buildNation.getPier()));
                tg.addRow("Granary", String.valueOf(buildNation.getGranary()));
                tg.addRow("Printing Press", String.valueOf(buildNation.getPrinting()));
                tg.addRow("Library", String.valueOf(buildNation.getLibrary()));
            }
        }
        for (String line : tg.generate(Receiver.CLIENT, true, true)) {
            player.sendMessage(line);
        }
    }

    public void printNationPlayers(Player player, String nation) {

        Connection connection = null;
        String load = "SELECT * FROM players WHERE nation=?";
        PreparedStatement loadPlayer = null;
        TableGenerator tg = new TableGenerator(Alignment.LEFT);
        tg.addRow(ChatColor.GOLD + nation + " Players:");

        try {
            connection = this.plugin.getHikari().getConnection();
            loadPlayer = connection.prepareStatement(load);
            loadPlayer.setString(1, nation);
            ResultSet r = loadPlayer.executeQuery();

            if (!r.next()) {
                r.close();
            } else {
                
                String playerName = r.getString("name");
                tg.addRow(playerName);

                while (r.next()) {
                    playerName = r.getString("name");
                    tg.addRow(playerName);
                }
                r.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        
        for (String line : tg.generate(Receiver.CLIENT, true, true)) {
            player.sendMessage(line);
        }
    }

    public void printNationResurces(Player player, String nation) {
        int iron = 0;
        int gold = 0;
        int emerald = 0;
        int diamond = 0;
        int redstone = 0;
        int coal = 0;
        int stone = 0;
        int logs = 0;
        int ink = 0;
        int books = 0;
        int food = 0;

        for (nResources resources : this.nationResources) {
            if (resources.getName().equalsIgnoreCase(nation)) {
                iron = resources.getIron();
                gold = resources.getGold();
                emerald = resources.getEmerald();
                diamond = resources.getDiamond();
                redstone = resources.getRedstone();
                coal = resources.getCoal();
                stone = resources.getStone();
                logs = resources.getLogs();
                ink = resources.getInk();
                books = resources.getBooks();
                food = resources.getFood();
            }
        }
        TableGenerator tg = new TableGenerator(Alignment.LEFT, Alignment.RIGHT);
        tg.addRow("Iron", String.valueOf(iron));
        tg.addRow("Gold", String.valueOf(gold));
        tg.addRow("Emerald", String.valueOf(emerald));
        tg.addRow("Diamond", String.valueOf(diamond));
        tg.addRow("Redstone", String.valueOf(redstone));
        tg.addRow("coal", String.valueOf(coal));
        tg.addRow("Stone", String.valueOf(stone));
        tg.addRow("Logs", String.valueOf(logs));
        tg.addRow("Ink", String.valueOf(ink));
        tg.addRow("Books", String.valueOf(books));
        tg.addRow("Food", String.valueOf(food));

        player.sendMessage(ChatColor.GOLD + "---[" + nation + " Bank]---");
        for (String line : tg.generate(Receiver.CLIENT, true, true)) {
            player.sendMessage(line);
        }

    }

    public void newNation(Player player, String nationName) {

        boolean unique = true;

        for (nNation nation : this.nationList) {
            if (nationName.equalsIgnoreCase(nation.getName())) {
                unique = false;
            }
        }

        if (unique = false) {
            Utilities.sendBad(player, "Nation name is not unique");
        } else {
            nNation nation = new nNation(nationName, player.getDisplayName(), 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            nResources resource = new nResources(nationName, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            this.nationResources.add(resource);
            this.nationList.add(nation);
            Utilities.sendGood(player, "Successfully created " + nationName);
        }

    }

    public void infoNation(Player player, String nation) {
        Boolean works = false;
        for (nNation checking : this.nationList) {
            if (checking.getName().equalsIgnoreCase(nation)) {
                works = true;
                int members = checking.getMembers();
                String founder = checking.getRuler();
                player.sendMessage(ChatColor.GOLD + "---[" + checking.getName() + "]---");
                player.sendMessage("This nation was founded by " + ChatColor.GOLD + founder);
                player.sendMessage("It currently has " + ChatColor.GOLD + members + " members");

            }
        }
        if (!works) {
            Utilities.sendBad(player, "This nation could not be found!");
        }
    }

    public Boolean checkCost(Player player, BuildingDesign design, nResources nation) {
        if (design.getIron() <= nation.getIron()) {
            if (design.getGold() <= nation.getGold()) {
                if (design.getEmerald() <= nation.getEmerald()) {
                    if (design.getDiamond() <= nation.getDiamond()) {
                        if (design.getRedstone() <= nation.getRedstone()) {
                            if (design.getCoal() <= nation.getCoal()) {
                                if (design.getStone() <= nation.getStone()) {
                                    if (design.getLogs() <= nation.getLogs()) {
                                        if (design.getInk() <= nation.getInk()) {
                                            if (design.getBooks() <= nation.getBooks()) {
                                                if (design.getFood() <= nation.getFood()) {

                                                    return true;
                                                }
                                                Utilities.sendBad(player, "Nation does not have enough Food");
                                                return false;
                                            }
                                            Utilities.sendBad(player, "Nation does not have enough Books");
                                            return false;
                                        }
                                        Utilities.sendBad(player, "Nation does not have enough Ink");
                                        return false;
                                    }
                                    Utilities.sendBad(player, "Nation does not have enough Logs");
                                    return false;
                                }
                                Utilities.sendBad(player, "Nation does not have enough Stone");
                                return false;
                            }
                            Utilities.sendBad(player, "Nation does not have enough Coal");
                            return false;
                        }
                        Utilities.sendBad(player, "Nation does not have enough Redstone");
                        return false;
                    }
                    Utilities.sendBad(player, "Nation does not have enough Diamond");
                    return false;
                }
                Utilities.sendBad(player, "Nation does not have enough Emerald");
                return false;
            }
            Utilities.sendBad(player, "Nation does not have enough Gold");
            return false;
        }
        Utilities.sendBad(player, "Nation does not have enough Iron");
        return false;
    }

    public Boolean checkUpkeepCost(BuildingDesign design, nResources nation) {
        if (design.getRecurringIron() <= nation.getIron()) {
            if (design.getRecurringGold() <= nation.getGold()) {
                if (design.getRecurringEmerald() <= nation.getEmerald()) {
                    if (design.getRecurringDiamond() <= nation.getDiamond()) {
                        if (design.getRecurringRedstone() <= nation.getRedstone()) {
                            if (design.getRecurringCoal() <= nation.getCoal()) {
                                if (design.getRecurringStone() <= nation.getStone()) {
                                    if (design.getRecurringLogs() <= nation.getLogs()) {
                                        if (design.getRecurringInk() <= nation.getInk()) {
                                            if (design.getRecurringBooks() <= nation.getBooks()) {
                                                if (design.getRecurringFood() <= nation.getFood()) {
                                                    return true;
                                                }
                                                return false;
                                            }
                                            return false;
                                        }
                                        return false;
                                    }
                                    return false;
                                }
                                return false;
                            }
                            return false;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }
        return false;
    }

    public Boolean checkBuildable(Player player, String building, nNation nation) {
        int built = 0;

        if (building.equalsIgnoreCase("barracks")) {

            built = nation.getBarracks();

            if (built >= this.plugin.maxBuilds.get("Barracks")) {
                return false;
            } else {
                return true;
            }

        }
        if (building.equalsIgnoreCase("cityhall")) {
            built = nation.getCityHall();
            if (built >= this.plugin.maxBuilds.get("CityHall")) {
                return false;
            } else {
                return true;
            }

        }
        if (building.equalsIgnoreCase("crusher")) {
            built = nation.getCrusher();
            if (built >= this.plugin.maxBuilds.get("Crusher")) {
                return false;
            } else {
                return true;
            }

        }
        if (building.equalsIgnoreCase("kiln")) {
            built = nation.getKiln();
            if (built >= this.plugin.maxBuilds.get("Kiln")) {
                return false;
            } else {
                return true;
            }

        }
        if (building.equalsIgnoreCase("center")) {

            built = nation.getCenter();
            if (built >= this.plugin.maxBuilds.get("Center")) {
                return false;
            } else {
                return true;
            }

        }
        if (building.equalsIgnoreCase("centerupg")) {
            built = nation.getCenterUpg();
            if (built >= this.plugin.maxBuilds.get("CenterUpg")) {
                return false;
            } else {
                return true;
            }

        }
        if (building.equalsIgnoreCase("smeltery")) {
            built = nation.getSmithy();
            if (built >= this.plugin.maxBuilds.get("Smeltery")) {
                return false;
            } else {
                return true;
            }

        }
        if (building.equalsIgnoreCase("forge")) {
            built = nation.getForge();
            if (built >= this.plugin.maxBuilds.get("Forge")) {
                return false;
            } else {
                return true;
            }

        }
        if (building.equalsIgnoreCase("pier")) {
            built = nation.getPier();
            if (built >= this.plugin.maxBuilds.get("Pier")) {
                return false;
            } else {
                return true;
            }

        }
        if (building.equalsIgnoreCase("granary")) {
            built = nation.getGranary();
            if (built >= this.plugin.maxBuilds.get("Granary")) {
                return false;
            } else {
                return true;
            }

        }
        if (building.equalsIgnoreCase("printingpress")) {
            built = nation.getPrinting();
            if (built >= this.plugin.maxBuilds.get("PrintingPress")) {
                return false;
            } else {
                return true;
            }

        }
        if (building.equalsIgnoreCase("library")) {
            built = nation.getLibrary();
            if (built >= this.plugin.maxBuilds.get("Library")) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    public void loadNations() {

        String name;
        String ruler;
        int members;
        int barracks;
        int cityHall;
        int crusher;
        int kiln;
        int center;
        int centerUpg;
        int smithy;
        int forge;
        int pier;
        int granary;
        int printing;
        int library;

        Connection connection = null;
        String load = "SELECT * FROM nations";
        PreparedStatement loadNations = null;

        try {
            connection = this.plugin.getHikari().getConnection();

            loadNations = connection.prepareStatement(load);
            ResultSet r = loadNations.executeQuery();

            if (!r.next()) {
                r.close();
            } else {
                name = r.getString("name");
                ruler = r.getString("ruler");
                members = r.getInt("members");
                barracks = r.getInt("barracks");
                cityHall = r.getInt("cityHall");
                crusher = r.getInt("crusher");
                kiln = r.getInt("kiln");
                center = r.getInt("center");
                centerUpg = r.getInt("centerUpg");
                smithy = r.getInt("smithy");
                forge = r.getInt("forge");
                pier = r.getInt("pier");
                granary = r.getInt("granary");
                printing = r.getInt("printing");
                library = r.getInt("library");

                nNation nation = new nNation(name, ruler, members, barracks, cityHall, crusher, kiln, center, centerUpg,
                        smithy, forge, pier, granary, printing, library);

                this.nationList.add(nation);

                while (r.next()) {
                    name = r.getString("name");
                    ruler = r.getString("ruler");
                    members = r.getInt("members");
                    barracks = r.getInt("barracks");
                    cityHall = r.getInt("cityHall");
                    crusher = r.getInt("crusher");
                    kiln = r.getInt("kiln");
                    center = r.getInt("center");
                    centerUpg = r.getInt("centerUpg");
                    smithy = r.getInt("smithy");
                    forge = r.getInt("forge");
                    pier = r.getInt("pier");
                    granary = r.getInt("granary");
                    printing = r.getInt("printing");
                    library = r.getInt("library");

                    nation = new nNation(name, ruler, members, barracks, cityHall, crusher, kiln, center, centerUpg,
                            smithy, forge, pier, granary, printing, library);

                    this.nationList.add(nation);

                }

                r.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (loadNations != null) {
                try {
                    loadNations.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public void loadResources() {

        String name;
        int iron;
        int gold;
        int emerald;
        int diamond;
        int redstone;
        int coal;
        int stone;
        int logs;
        int ink;
        int books;
        int food;

        Connection connection = null;
        String load = "SELECT * FROM nationbuildings";
        PreparedStatement loadNations = null;

        try {
            connection = this.plugin.getHikari().getConnection();

            loadNations = connection.prepareStatement(load);
            ResultSet r = loadNations.executeQuery();

            if (!r.next()) {
                r.close();
            } else {

                name = r.getString("name");
                iron = r.getInt("iron");
                gold = r.getInt("gold");
                emerald = r.getInt("emerald");
                diamond = r.getInt("diamond");
                redstone = r.getInt("redstone");
                coal = r.getInt("coal");
                stone = r.getInt("stone");
                logs = r.getInt("logs");
                ink = r.getInt("ink");
                books = r.getInt("books");
                food = r.getInt("food");

                nResources resources = new nResources(name, iron, gold, emerald, diamond, redstone, coal, stone, logs, ink, books, food);

                this.nationResources.add(resources);

                while (r.next()) {

                    name = r.getString("name");
                    iron = r.getInt("iron");
                    gold = r.getInt("gold");
                    emerald = r.getInt("emerald");
                    diamond = r.getInt("diamond");
                    redstone = r.getInt("redstone");
                    coal = r.getInt("coal");
                    stone = r.getInt("stone");
                    logs = r.getInt("logs");
                    ink = r.getInt("ink");
                    books = r.getInt("books");
                    food = r.getInt("food");

                    resources = new nResources(name, iron, gold, emerald, diamond, redstone, coal, stone, logs, ink, books, food);

                    this.nationResources.add(resources);
                }
                r.close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (loadNations != null) {
                try {
                    loadNations.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public void saveNation() {

        for (nNation nation : this.nationList) {
            Connection connection = null;
            String save = "INSERT INTO nations(name,ruler,members,barracks,cityHall,crusher,kiln,center,"
                    + "centerUpg,smithy,forge,pier,granary, printing,library) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE ruler=?, members=?, "
                    + "barracks=?, cityHall=?, crusher=?, kiln=?, center=?, centerUpg=?, smithy=?, forge=?, pier=?, granary=?, printing=?, library=?";
            PreparedStatement saveNation = null;

            try {
                connection = this.plugin.getHikari().getConnection();

                saveNation = connection.prepareStatement(save);
                saveNation.setString(1, nation.getName());
                saveNation.setString(2, nation.getRuler());
                saveNation.setInt(3, nation.getMembers());
                saveNation.setInt(4, nation.getBarracks());
                saveNation.setInt(5, nation.getCityHall());
                saveNation.setInt(6, nation.getCrusher());
                saveNation.setInt(7, nation.getKiln());
                saveNation.setInt(8, nation.getCenter());
                saveNation.setInt(9, nation.getCenterUpg());
                saveNation.setInt(10, nation.getSmithy());
                saveNation.setInt(11, nation.getForge());
                saveNation.setInt(12, nation.getPier());
                saveNation.setInt(13, nation.getGranary());
                saveNation.setInt(14, nation.getPrinting());
                saveNation.setInt(15, nation.getLibrary());

                saveNation.setString(16, nation.getRuler());
                saveNation.setInt(17, nation.getMembers());
                saveNation.setInt(18, nation.getBarracks());
                saveNation.setInt(19, nation.getCityHall());
                saveNation.setInt(20, nation.getCrusher());
                saveNation.setInt(21, nation.getKiln());
                saveNation.setInt(22, nation.getCenter());
                saveNation.setInt(23, nation.getCenterUpg());
                saveNation.setInt(24, nation.getSmithy());
                saveNation.setInt(25, nation.getForge());
                saveNation.setInt(26, nation.getPier());
                saveNation.setInt(27, nation.getGranary());
                saveNation.setInt(28, nation.getPrinting());
                saveNation.setInt(29, nation.getLibrary());

                saveNation.execute();

            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

                if (saveNation != null) {
                    try {
                        saveNation.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void saveResources() {

        for (nResources resources : this.nationResources) {
            Connection connection = null;
            String save = "INSERT INTO nationbuildings(name,iron,gold,emerald,diamond,redstone,coal,stone,"
                    + "logs,ink,books,food) VALUES(?,?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE iron=?, gold=?, emerald=?, diamond=?, redstone=?, coal=?,"
                    + " stone=?, logs=?, ink=?, books=?, food=?";
            PreparedStatement saveResources = null;

            try {
                connection = this.plugin.getHikari().getConnection();

                saveResources = connection.prepareStatement(save);
                saveResources.setString(1, resources.getName());
                saveResources.setInt(2, resources.getIron());
                saveResources.setInt(3, resources.getGold());
                saveResources.setInt(4, resources.getEmerald());
                saveResources.setInt(5, resources.getDiamond());
                saveResources.setInt(6, resources.getRedstone());
                saveResources.setInt(7, resources.getCoal());
                saveResources.setInt(8, resources.getStone());
                saveResources.setInt(9, resources.getLogs());
                saveResources.setInt(10, resources.getInk());
                saveResources.setInt(11, resources.getBooks());
                saveResources.setInt(12, resources.getFood());

                saveResources.setInt(13, resources.getIron());
                saveResources.setInt(14, resources.getGold());
                saveResources.setInt(15, resources.getEmerald());
                saveResources.setInt(16, resources.getDiamond());
                saveResources.setInt(17, resources.getRedstone());
                saveResources.setInt(18, resources.getCoal());
                saveResources.setInt(19, resources.getStone());
                saveResources.setInt(20, resources.getLogs());
                saveResources.setInt(21, resources.getInk());
                saveResources.setInt(22, resources.getBooks());
                saveResources.setInt(23, resources.getFood());

                saveResources.execute();

            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

                if (saveResources != null) {
                    try {
                        saveResources.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void deleteNation() {
        for (nNation nation : this.nationDeletions) {
            Connection connection = null;
            Connection connection2 = null;
            String delete = "DELETE FROM nations WHERE name=?";
            String delete2 = "DELETE FROM nationbuildings WHERE name=?";
            PreparedStatement deleteNation = null;
            PreparedStatement deleteNation2 = null;

            try {
                connection = this.plugin.getHikari().getConnection();
                deleteNation = connection.prepareStatement(delete);
                deleteNation.setString(1, nation.getName());
                deleteNation.execute();

                connection2 = this.plugin.getHikari().getConnection();
                deleteNation2 = connection.prepareStatement(delete2);
                deleteNation2.setString(1, nation.getName());
                deleteNation2.execute();

            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

                if (deleteNation != null) {
                    try {
                        deleteNation.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
                if (connection2 != null) {
                    try {
                        connection2.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

                if (deleteNation2 != null) {
                    try {
                        deleteNation2.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public class TopNationsComparator implements Comparator {

        public TopNationsComparator() {
        }

        public int compare(Object object1, Object object2) {

            nNation n1 = (nNation) object1;
            nNation n2 = (nNation) object2;

            return n2.members - n1.members;

        }
    }
}
