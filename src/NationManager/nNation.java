package NationManager;

/**
 *
 * @author lanca
 */
public class nNation {

    public String name;
    public String ruler;
    public int members;
    public int barracks;
    public int cityHall;
    public int crusher;
    public int kiln;
    public int center;
    public int centerUpg;
    public int smithy;
    public int forge;
    public int pier;
    public int granary;
    public int printing;
    public int library;

    nNation(String name, String ruler, int members, int barracks, int cityHall, int crusher, int kiln, int center, int centerUpg, int smithy,
            int forge, int pier, int granary, int printing, int library) {

        this.name = name;
        this.ruler = ruler;
        this.members = members;
        this.barracks = barracks;
        this.cityHall = cityHall;
        this.crusher = crusher;
        this.kiln = kiln;
        this.center = center;
        this.centerUpg = centerUpg;
        this.smithy = smithy;
        this.forge = forge;
        this.pier = pier;
        this.granary = granary;
        this.printing = printing;
        this.library = library;

    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setRuler(String ruler) {
        this.ruler = ruler;
    }

    public String getRuler() {
        return this.ruler;
    }

    public int getMembers() {
        return this.members;
    }

    public int getBarracks() {
        return this.barracks;
    }

    public int getCityHall() {
        return this.cityHall;
    }

    public int getCrusher() {
        return this.crusher;
    }

    public int getKiln() {
        return this.kiln;
    }

    public int getCenter() {
        return this.center;
    }

    public int getCenterUpg() {
        return this.centerUpg;
    }

    public int getSmithy() {
        return this.smithy;
    }

    public int getForge() {
        return this.forge;
    }

    public int getPier() {
        return this.pier;
    }

    public int getGranary() {
        return this.granary;
    }

    public int getPrinting() {
        return this.printing;
    }

    public int getLibrary() {
        return this.library;
    }

    public void setMembers(int setting) {
        this.members = setting;
    }

    public void setBarracks(int setting) {
        this.barracks = setting;
    }

    public void setCityHall(int setting) {
        this.cityHall = setting;
    }

    public void setCrusher(int setting) {
        this.crusher = setting;
    }

    public void setKiln(int setting) {
        this.kiln = setting;
    }

    public void setCenter(int setting) {
        this.center = setting;
    }

    public void setCenterUpg(int setting) {
        this.centerUpg = setting;
    }

    public void setSmithy(int setting) {
        this.smithy = setting;
    }

    public void setForge(int setting) {
        this.forge = setting;
    }

    public void setPier(int setting) {
        this.pier = setting;
    }

    public void setGranary(int setting) {
        this.granary = setting;
    }

    public void setPrinting(int setting) {
        this.printing = setting;
    }

    public void setLibrary(int setting) {
        this.library = setting;
    }

}
