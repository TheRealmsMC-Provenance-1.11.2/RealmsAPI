package NationManager;

/**
 *
 * @author lanca
 */
public class nResources {

    public String name;
    public int iron;
    public int gold;
    public int emerald;
    public int diamond;
    public int redstone;
    public int coal;
    public int stone;
    public int logs;
    public int ink;
    public int books;
    public int food;

    nResources(String name, int iron, int gold, int emerald, int diamond, int redstone, int coal, int stone, int logs, int ink, int books, int food) {

        this.name = name;
        this.iron = iron;
        this.gold = gold;
        this.emerald = emerald;
        this.diamond = diamond;
        this.redstone = redstone;
        this.coal = coal;
        this.stone = stone;
        this.logs = logs;
        this.ink = ink;
        this.books = books;
        this.food = food;

    }

    public String getName() {
        return this.name;
    }

    public int getIron() {
        return this.iron;
    }

    public int getGold() {
        return this.gold;
    }

    public int getEmerald() {
        return this.emerald;
    }

    public int getDiamond() {
        return this.diamond;
    }

    public int getRedstone() {
        return this.redstone;
    }

    public int getCoal() {
        return this.coal;
    }

    public int getStone() {
        return this.stone;
    }

    public int getLogs() {
        return this.logs;
    }

    public int getInk() {
        return this.ink;
    }

    public int getBooks() {
        return this.books;
    }

    public int getFood() {
        return this.food;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIron(int i) {
        this.iron = i;
    }

    public void setGold(int i) {
        this.gold = i;
    }

    public void setEmerald(int i) {
        this.emerald = i;
    }

    public void setDiamond(int i) {
        this.diamond = i;
    }

    public void setRedstone(int i) {
        this.redstone = i;
    }

    public void setCoal(int i) {
        this.coal = i;
    }

    public void setStone(int i) {
        this.stone = i;
    }

    public void setLogs(int i) {
        this.logs = i;
    }

    public void setInk(int i) {
        this.ink = i;
    }

    public void setBooks(int i) {
        this.books = i;
    }

    public void setFood(int i) {
        this.food = i;
    }
}
