package InviteManager;

import PlayerManager.PlayerManager.nPlayer;
import chatapi.Utilities;
import java.util.ArrayList;
import java.util.HashMap;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import Realms.RealmsAPI;

/**
 *
 * @author lanca
 */
public class InviteManager {
    
    private RealmsAPI plugin;
    public HashMap<String, Request> invites;
    
    public InviteManager(RealmsAPI plugin)
    {
        this.plugin = plugin;
        this.invites = new HashMap<>();    
        askerTask();
    }
    
    
    // BELOW IS EVERYTHING TO DO WITH INVITES   
    
    
    public void addInviteRequest(String sender, String invited, String nation)
    {
        String message = "You have been invited to join " + nation;
        Request req = new Request(nation, message, invited, sender);
        this.invites.put(invited.toLowerCase(), req);
        ask(req); 
    }

    public void processAcceptInvite(Request req)
    {
        Player invited = RealmsAPI.getInstance().getServer().getPlayer(req.player);
        nPlayer invite = null;
        ArrayList<nPlayer> nationPlayers = new ArrayList<>();
        
        if(invited != null)
        {

            nPlayer player = this.plugin.getPlayerManager().playerList.get(invited.getUniqueId().toString());
            if(player != null){
                invite = player;
                if(player.getNation().equalsIgnoreCase(req.nation)){
                    nationPlayers.add(player);
                }
            }
            
            if (invite != null) {

                for (nPlayer nationPlayer : nationPlayers) {
                    Player member = RealmsAPI.getInstance().getServer().getPlayer(nationPlayer.getUUID());
                    if (member != null) {
                        Utilities.sendGood(member, req.player + " has joined your nation!");
                    }
                }

                plugin.getPlayerManager().leaveNation(invited.getUniqueId());
                plugin.getPlayerManager().addNation(invite, req.nation);

                
                Utilities.sendGood(invited, "You have joined the nation " + req.nation);
                this.invites.remove(req.player.toLowerCase());
                
            }
        }
    }
    
    public void processDenyInvite(Request req)
    {
        Player invited = RealmsAPI.getInstance().getServer().getPlayer(req.player);
        Player inviter = RealmsAPI.getInstance().getServer().getPlayer(req.inviter);
        
        if (inviter != null)
        {
            Utilities.sendBad(inviter, req.player + " declined your invitation to join " + req.nation);
        }
        if (invited != null)
        {
            Utilities.sendBad(invited, "You have declined the invitation to join " + req.nation);
            this.invites.remove(req.player.toLowerCase());
        }

    }
    
    public void askerTask()
    {
        this.plugin.getServer().getScheduler().scheduleSyncRepeatingTask(this.plugin, new Runnable()
                {
                    public void run()
                    {
                        for (Request req : InviteManager.this.invites.values())
                        {
                            InviteManager.this.ask(req);
                        }
                    }
                }, 0L, 2400L);
    }
    
    public void ask(Request req)
    {
        String message = req.message;
        String option = ChatColor.WHITE + "Use " + ChatColor.DARK_GREEN + "/join " + ChatColor.WHITE + "or " + ChatColor.DARK_RED + "/ignore";
        Player player = RealmsAPI.getInstance().getServer().getPlayer(req.player);
        if (player != null)
        {
            player.sendMessage(" ");
            Utilities.sendNormal(player, message);
            Utilities.sendNormal(player, option);
            player.sendMessage(" ");
        }
    }
    

    public class Request
    {
        private String nation;
        private String message;
        private String player;
        private String inviter;
        
        public Request(String nation, String message, String player, String inviter)
        {
            this.nation = nation;
            this.message = message;
            this.player = player;
            this.inviter = inviter;
        }
    }
}
