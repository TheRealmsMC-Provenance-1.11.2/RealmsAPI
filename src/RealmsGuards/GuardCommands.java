/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RealmsGuards;

import PlayerManager.PlayerManager.nPlayer;
import chatapi.Utilities;
import java.util.HashMap;
import java.util.List;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import Realms.RealmsAPI;

/**
 *
 * @author James
 */
public class GuardCommands implements CommandExecutor {

    private RealmsAPI plugin;

    public GuardCommands(RealmsAPI plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (!(sender instanceof Player)) {
            return false;
        } else {
            Player player = (Player) sender;

            //using /guards list <group>
            //List the guards in the Area you Control
            if (args.length == 2 && args[0].equalsIgnoreCase("list")) {
                String group = args[1];

                nPlayer check = this.plugin.getPlayerManager().playerList.get(player.getUniqueId().toString());
                if (check != null) {
                    String nation = check.getNation();
                    if (check.getRole() >= 2) {
                        List<Entity> inRange = player.getNearbyEntities(20, 20, 20);
                        int i = 0;
                        for (Entity entity : inRange) {
                            if (entity.hasMetadata("NPC")) {
                                SentryInstance inst = this.plugin.getGuardManager().getSentry(entity);
                                if (inst != null) {
                                    String entgroup = inst.getGroup();
                                    if (entgroup.equalsIgnoreCase(group)) {
                                        i++;
                                    }
                                }
                            }
                        }
                        if (i == 0) {
                            Utilities.sendBad(player, "You own no guards in this area");
                            return true;
                        } else {
                            Utilities.sendGood(player, "You control " + i + " guards in this area");
                            return true;
                        }
                    } else {
                        Utilities.sendBad(player, "You do not have the required Nation Permissions to run this command");
                        return true;
                    }
                }

            }

            // using /guards area
            if (args.length == 1 && args[0].equalsIgnoreCase("area")) {
                String group;
                HashMap<String, Integer> groups = new HashMap<>();
                List<Entity> entities = player.getNearbyEntities(20, 20, 20);
                for (Entity entity : entities) {
                    if (entity.hasMetadata("NPC")) {
                        SentryInstance inst = this.plugin.getGuardManager().getSentry(entity);
                        if (inst != null) {
                            group = inst.getGroup();
                            if (!groups.containsKey(group)) {
                                groups.put(group, 1);
                            } else {
                                int i = groups.get(group);
                                i += 1;
                                groups.put(group, i);
                            }
                        }
                    }
                }
                return true;
            }

            //Using "/guards attack player X"
            if (args.length == 3 && args[0].equalsIgnoreCase("attack")) {
                int role = 0;
                String nation = "none";
                nPlayer check = this.plugin.getPlayerManager().playerList.get(player.getUniqueId().toString());
                if (check != null) {
                    role = check.getRole();
                    nation = check.getNation();
                }
                if (!nation.equalsIgnoreCase("none")) {
                    if (role >= 2) {
                        String target = null;
                        if (args[1].equalsIgnoreCase("player")) {
                            target = "PLAYER:";
                        } else if (args[1].equalsIgnoreCase("nation")) {
                            target = "NATION:";
                        } else if (args[1].equalsIgnoreCase("entity")) {
                            target = "ENTITY:";
                        }

                        if (target != null) {
                            target = target + args[2].toUpperCase();

                            List<Entity> inRange = player.getNearbyEntities(20, 20, 20);
                            int i = 0;
                            for (Entity entity : inRange) {
                                if (entity.hasMetadata("NPC")) {
                                    SentryInstance inst = this.plugin.getGuardManager().getSentry(entity);
                                    if (inst != null) {
                                        String entgroup = inst.getGroup();

                                        if (entgroup.equalsIgnoreCase(nation)) {

                                            if (!inst.containsTarget(target.toUpperCase())) {
                                                inst.validTargets.add(target.toUpperCase());
                                                inst.processTargets();
                                                i++;
                                            }
                                        }
                                    }
                                }
                            }
                            if (i > 0) {
                                Utilities.sendGood(player, i + " Guards have been set to target: " + target);
                                return true;
                            } else {
                                Utilities.sendGood(player, "No Guards were found within range");
                                return true;
                            }
                        }
                        Utilities.sendBad(player, "The target must be set as: player, nation, or entity");
                        return false;
                    }
                    Utilities.sendBad(player, "You do not have the correct Nation Permissions");
                    return false;
                }
                Utilities.sendBad(player, "You are not currently in a Nation");
                return false;
            }

            //Using "/guards noattack player X"
            if (args.length == 3 && args[0].equalsIgnoreCase("noattack")) {
                int role = 0;
                String nation = "none";
                nPlayer check = this.plugin.getPlayerManager().playerList.get(player.getUniqueId().toString());
                if (check != null) {
                    role = check.getRole();
                    nation = check.getNation();
                }
                if (!nation.equalsIgnoreCase("none")) {
                    if (role >= 2) {
                        String target = null;
                        if (args[1].equalsIgnoreCase("player")) {
                            target = "PLAYER:";
                        } else if (args[1].equalsIgnoreCase("nation")) {
                            target = "NATION:";
                        } else if (args[1].equalsIgnoreCase("entity")) {
                            target = "ENTITY:";
                        }

                        if (target != null) {
                            target = target + args[2].toUpperCase();

                            List<Entity> inRange = player.getNearbyEntities(20, 20, 20);
                            int i = 0;
                            for (Entity entity : inRange) {
                                if (entity.hasMetadata("NPC")) {
                                    SentryInstance inst = this.plugin.getGuardManager().getSentry(entity);
                                    if (inst != null) {
                                        String entgroup = inst.getGroup();

                                        if (entgroup.equalsIgnoreCase(nation)) {

                                            if (inst.containsTarget(target.toUpperCase())) {
                                                inst.validTargets.remove(target.toUpperCase());
                                                inst.processTargets();
                                                i++;
                                            }
                                        }
                                    }
                                }
                            }
                            if (i > 0) {
                                Utilities.sendGood(player, i + " Guards have been set to target: " + target);
                                return true;
                            } else {
                                Utilities.sendGood(player, "No Guards were found within range");
                                return true;
                            }
                        }
                        Utilities.sendBad(player, "The target must be set as: player, nation, or entity");
                        return false;
                    }
                    Utilities.sendBad(player, "You do not have the correct Nation Permissions");
                    return false;
                }
                Utilities.sendBad(player, "You are not currently in a Nation");
                return false;
            }

            //Using "/guards ignore player X"
            if (args.length == 3 && args[0].equalsIgnoreCase("ignore")) {
                int role = 0;
                String nation = "none";
                nPlayer check = this.plugin.getPlayerManager().playerList.get(player.getUniqueId().toString());
                if (check != null) {
                    role = check.getRole();
                    nation = check.getNation();
                }
                if (!nation.equalsIgnoreCase("none")) {
                    if (role >= 2) {
                        String ignore = null;
                        if (args[1].equalsIgnoreCase("player")) {
                            ignore = "PLAYER:";
                        } else if (args[1].equalsIgnoreCase("nation")) {
                            ignore = "NATION:";
                        } else if (args[1].equalsIgnoreCase("entity")) {
                            ignore = "ENTITY:";
                        }

                        if (ignore != null) {
                            ignore = ignore + args[2].toUpperCase();

                            List<Entity> inRange = player.getNearbyEntities(20, 20, 20);
                            int i = 0;
                            for (Entity entity : inRange) {
                                if (entity.hasMetadata("NPC")) {
                                    SentryInstance inst = this.plugin.getGuardManager().getSentry(entity);
                                    if (inst != null) {
                                        String entgroup = inst.getGroup();

                                        if (entgroup.equalsIgnoreCase(nation)) {

                                            if (!inst.containsIgnore(ignore.toUpperCase())) {
                                                inst.ignoreTargets.add(ignore.toUpperCase());
                                                inst.processTargets();
                                                i++;
                                            }
                                        }
                                    }
                                }
                            }
                            if (i > 0) {
                                Utilities.sendGood(player, i + " Guards have been set to ignore: " + ignore);
                                return true;
                            } else {
                                Utilities.sendGood(player, "No Guards were found within range");
                                return true;
                            }
                        }
                        Utilities.sendBad(player, "The target must be set as: player, nation, or entity");
                        return false;
                    }
                    Utilities.sendBad(player, "You do not have the correct Nation Permissions");
                    return false;
                }
                Utilities.sendBad(player, "You are not currently in a Nation");
                return false;
            }

            //Using "/guards noignore player X"
            if (args.length == 3 && args[0].equalsIgnoreCase("noignore")) {
                int role = 0;
                String nation = "none";
                nPlayer check = this.plugin.getPlayerManager().playerList.get(player.getUniqueId().toString());
                if (check != null) {
                    role = check.getRole();
                    nation = check.getNation();
                }
                if (!nation.equalsIgnoreCase("none")) {
                    if (role >= 2) {
                        String ignore = null;
                        if (args[1].equalsIgnoreCase("player")) {
                            ignore = "PLAYER:";
                        } else if (args[1].equalsIgnoreCase("nation")) {
                            ignore = "NATION:";
                        } else if (args[1].equalsIgnoreCase("entity")) {
                            ignore = "ENTITY:";
                        }

                        if (ignore != null) {
                            ignore = ignore + args[2].toUpperCase();

                            List<Entity> inRange = player.getNearbyEntities(20, 20, 20);
                            int i = 0;
                            for (Entity entity : inRange) {
                                if (entity.hasMetadata("NPC")) {
                                    SentryInstance inst = this.plugin.getGuardManager().getSentry(entity);
                                    if (inst != null) {
                                        String entgroup = inst.getGroup();

                                        if (entgroup.equalsIgnoreCase(nation)) {

                                            if (inst.containsIgnore(ignore.toUpperCase())) {
                                                inst.ignoreTargets.remove(ignore.toUpperCase());
                                                inst.processTargets();
                                                i++;
                                            }
                                        }
                                    }
                                }
                            }
                            if (i > 0) {
                                Utilities.sendGood(player, i + " Guards have been set to ignore: " + ignore);
                                return true;
                            } else {
                                Utilities.sendGood(player, "No Guards were found within range");
                                return true;
                            }
                        }
                        Utilities.sendBad(player, "The target must be set as: player, nation, or entity");
                        return false;
                    }
                    Utilities.sendBad(player, "You do not have the correct Nation Permissions");
                    return false;
                }
                Utilities.sendBad(player, "You are not currently in a Nation");
                return false;
            }

            //using /guards warn <group> warning would go here for those who break it and stuff
            //sets the warning message of the Guards in the area
            if (args.length >= 3 && args[0].equalsIgnoreCase("warn")) {
                String group = args[1];

                String arg = "";
                for (int i = 2; i < args.length; i++) {
                    arg += " " + args[i];
                }
                arg = arg.trim();

                String warningMsg = arg.replaceAll("\"$", "").replaceAll("^\"", "").replaceAll("'$", "").replaceAll("^'", "");

                nPlayer check = this.plugin.getPlayerManager().playerList.get(player.getUniqueId().toString());
                if (check != null) {
                    String nation = check.getNation();
                    if (check.getRole() >= 2) {
                        List<Entity> inRange = player.getNearbyEntities(20, 20, 20);
                        int i = 0;
                        for (Entity entity : inRange) {
                            if (entity.hasMetadata("NPC")) {
                                SentryInstance inst = this.plugin.getGuardManager().getSentry(entity);
                                if (inst != null) {
                                    String entgroup = inst.getGroup();
                                    if (entgroup.equalsIgnoreCase(group)) {
                                        inst.WarningMessage = "&e<GROUP> &b<NPC> says: " + warningMsg;
                                        i++;
                                    }
                                }
                            }
                        }

                        Utilities.sendGood(player, i + " Guards warning message set to: '" + ChatColor.translateAlternateColorCodes('&', warningMsg) + "'");
                        return true;
                    } else {
                        Utilities.sendBad(player, "You do not have the required Nation Permissions to run this command");
                        return true;
                    }
                }
            }

            //using /guards greet <group> greeting message would go here for those who enter
            //Sets the greeting message of the guards in the area
            if (args.length >= 3 && args[0].equalsIgnoreCase("greet")) {
                String group = args[1];

                String arg = "";
                for (int i = 2; i < args.length; i++) {
                    arg += " " + args[i];
                }
                arg = arg.trim();

                String greetingMsg = arg.replaceAll("\"$", "").replaceAll("^\"", "").replaceAll("'$", "").replaceAll("^'", "");

                nPlayer check = this.plugin.getPlayerManager().playerList.get(player.getUniqueId().toString());
                if (check != null) {
                    String nation = check.getNation();
                    if (check.getRole() >= 2) {
                        List<Entity> inRange = player.getNearbyEntities(20, 20, 20);
                        int i = 0;
                        for (Entity entity : inRange) {
                            if (entity.hasMetadata("NPC")) {
                                SentryInstance inst = this.plugin.getGuardManager().getSentry(entity);
                                if (inst != null) {
                                    String entgroup = inst.getGroup();
                                    if (entgroup.equalsIgnoreCase(group)) {
                                        inst.GreetingMessage = "&e<GROUP> &b<NPC> says: " + greetingMsg;
                                        i++;
                                    }
                                }
                            }
                        }

                        Utilities.sendGood(player, i + " Guards greeting message set to: '" + ChatColor.translateAlternateColorCodes('&', greetingMsg) + "'");
                        return true;
                    } else {
                        Utilities.sendBad(player, "You do not have the required Nation Permissions to run this command");
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
