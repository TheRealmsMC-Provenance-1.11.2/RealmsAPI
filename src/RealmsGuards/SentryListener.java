package RealmsGuards;

import BuildingManager.Building;
import NationManager.nNation;
import PlayerManager.PlayerManager.nPlayer;
import java.util.List;
import chatapi.Utilities;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.projectiles.ProjectileSource;

import net.citizensnpcs.api.CitizensAPI;

import net.citizensnpcs.api.npc.NPC;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import Realms.RealmsAPI;
import org.bukkit.Location;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import vg.civcraft.mc.citadel.events.ReinforcementDamageEvent;
import vg.civcraft.mc.citadel.reinforcement.PlayerReinforcement;


public class SentryListener implements Listener {

    public RealmsAPI plugin;

    public SentryListener(RealmsAPI sentry) {
        plugin = sentry;
    }

    @EventHandler
    public void kill(org.bukkit.event.entity.EntityDeathEvent event) {

        if (event.getEntity() == null) {
            return;
        }

        if (event.getEntity() instanceof Player && event.getEntity().hasMetadata("NPC") == false) {
            return;
        }

        Entity killer = event.getEntity().getKiller();
        if (killer == null) {
            EntityDamageEvent ev = event.getEntity().getLastDamageCause();
            if (ev != null && ev instanceof EntityDamageByEntityEvent) {
                killer = ((EntityDamageByEntityEvent) ev).getDamager();
                if (killer instanceof Projectile && ((Projectile) killer).getShooter() instanceof Entity) {
                    killer = (Entity) ((Projectile) killer).getShooter();
                }
            }
        }

        SentryInstance sentry = this.plugin.getGuardManager().getSentry(killer);
        

        if (sentry != null && sentry.KillsDropInventory == false) {
            event.getDrops().clear();
            event.setDroppedExp(0);
        }
    }
    
    @EventHandler
    public void spawned(net.citizensnpcs.api.event.NPCSpawnEvent event) {
        NPC npc = event.getNPC();
        SentryInstance sentry = this.plugin.getGuardManager().getSentry(event.getNPC());
        if(sentry != null)
        {
            for(nNation nation : this.plugin.getNationManager().nationList){
                if(nation.getName().equalsIgnoreCase(sentry.getGroup()))
                {
                    if(nation.getBarracks() >= 1)
                    {
                        this.plugin.getGuardManager().loadGuardWaypoints(npc);
                    } else {
                        npc.destroy();
                    }
                }
            }
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void despawn(net.citizensnpcs.api.event.NPCDespawnEvent event) {
        SentryInstance sentry = this.plugin.getGuardManager().getSentry(event.getNPC());
        
        if (sentry != null && event.getReason() == net.citizensnpcs.api.event.DespawnReason.CHUNK_UNLOAD && sentry.guardEntity != null) {
            event.setCancelled(true);
        } else {
            if(sentry != null)
            {
                sentry.waypoints.clear();
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void EnvDamage(EntityDamageEvent event) {

        if (event instanceof EntityDamageByEntityEvent) {
            return;
        }

        SentryInstance inst = this.plugin.getGuardManager().getSentry(event.getEntity());
        if (inst == null) {
            return;
        }

        event.setCancelled(true);

        DamageCause cause = event.getCause();

        switch (cause) {
            case CONTACT:
            case DROWNING:
            case LAVA:
            case SUFFOCATION:
            case CUSTOM:
            case BLOCK_EXPLOSION:
            case VOID:
            case SUICIDE:
            case MAGIC:
                inst.onEnvironmentDamae(event);
                break;
            case LIGHTNING:
                inst.onEnvironmentDamae(event);
                break;
            case FIRE:
            case FIRE_TICK:
                inst.onEnvironmentDamae(event);
                break;
            case POISON:
                inst.onEnvironmentDamae(event);
                break;
            case FALL:
                break;
            default:
                break;
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onDamage(EntityDamageByEntityEvent event) {

        Entity entfrom = event.getDamager();
        Entity entto = event.getEntity();

        if (entfrom instanceof org.bukkit.entity.Projectile && entfrom instanceof Entity) {
            ProjectileSource source = ((Projectile) entfrom).getShooter();
            if (source instanceof Entity) {
                entfrom = (Entity) ((org.bukkit.entity.Projectile) entfrom).getShooter();
            }
        }

        SentryInstance from = this.plugin.getGuardManager().getSentry(entfrom);
        SentryInstance to = this.plugin.getGuardManager().getSentry(entto);

        plugin.debug("start: from: " + entfrom + " to " + entto + " cancelled " + event.isCancelled() + " damage " + event.getDamage() + " cause " + event.getCause());

        if (from != null) {

            if (event.getDamager() instanceof org.bukkit.entity.Projectile) {
                if (entto instanceof LivingEntity && from.isIgnored((LivingEntity) entto)) {
                    event.setCancelled(true);
                    event.getDamager().remove();
                    Projectile newProjectile = (Projectile) (entfrom.getWorld().spawnEntity(event.getDamager().getLocation().add(event.getDamager().getVelocity()), event.getDamager().getType()));
                    newProjectile.setVelocity(event.getDamager().getVelocity());
                    newProjectile.setShooter((LivingEntity) entfrom);
                    newProjectile.setTicksLived(event.getDamager().getTicksLived());
                    plugin.debug("Entity is a Projectile and damaged is ignored");
                    return;
                }
            }

            event.setDamage((double) from.getStrength());

            if (from.guardTarget == null || this.plugin.getGuardManager().BodyguardsObeyProtection == false) {
                event.setCancelled(false);
            }

            if (to == null) {
                NPC n = CitizensAPI.getNPCRegistry().getNPC(entto);
                if (n != null) {

                }
            }

            if (entto == from.guardEntity) {
                plugin.debug("Attacking itself");
                event.setCancelled(true);
            }

            if (entfrom == entto) {
                plugin.debug("Attacking itself");
                event.setCancelled(true);
            }

            if (from.potionEffects != null && event.isCancelled() == false) {
                ((LivingEntity) entto).addPotionEffects(from.potionEffects);
            }

        }

        boolean ok = false;

        if (to != null) {
            if (entfrom == entto) {
                return;
            }

            if (to.guardTarget == null) {
                plugin.debug("to guard target null");
                event.setCancelled(false);
            }

            if (entfrom == to.guardEntity) {
                plugin.debug("nt from to same");
                event.setCancelled(true);
            }

            if (entfrom != null) {
                NPC npc = net.citizensnpcs.api.CitizensAPI.getNPCRegistry().getNPC(entfrom);
                if (npc != null && npc.hasTrait(SentryTrait.class) && to.guardEntity != null) {
                    plugin.debug("from has sentry and to not null");
                    if (npc.getTrait(SentryTrait.class).getInstance().guardEntity == to.guardEntity) {
                        plugin.debug("npc guardentity == to guard");
                        event.setCancelled(true);
                    }
                }
            }

            if (!event.isCancelled()) {
                ok = true;
                plugin.debug("Event not cancelled throwing on damage");
                to.onDamage(event);
            }

            event.setCancelled(true);
        }

        if ((event.isCancelled() == false || ok) && entfrom != entto && event.getDamage() > 0) {
            for (NPC npc : CitizensAPI.getNPCRegistry()) {
                SentryInstance inst = this.plugin.getGuardManager().getSentry(npc);

                if (inst == null || !npc.isSpawned() || npc.getEntity().getWorld() != entto.getWorld()) {
                    continue;
                }
                if (inst.guardEntity == entto) {
                    if (inst.Retaliate && entfrom instanceof LivingEntity) {
                        inst.setTarget((LivingEntity) entfrom, true);
                    }
                }

                if (inst.hasTargetType(16) && inst.sentryStatus == RealmsGuards.SentryInstance.Status.isLOOKING && entfrom instanceof Player && CitizensAPI.getNPCRegistry().isNPC(entfrom) == false) {

                    if (npc.getEntity().getLocation().distance(entto.getLocation()) <= inst.sentryRange || npc.getEntity().getLocation().distance(entfrom.getLocation()) <= inst.sentryRange) {

                        if (inst.NightVision >= entfrom.getLocation().getBlock().getLightLevel() || inst.NightVision >= entto.getLocation().getBlock().getLightLevel()) {

                            if (inst.hasLOS(entfrom) || inst.hasLOS(entto)) {

                                if ((!(entto instanceof Player) && inst.containsTarget("event:pve"))
                                        || (entto instanceof Player && CitizensAPI.getNPCRegistry().isNPC(entto) == false && inst.containsTarget("event:pvp"))
                                        || (CitizensAPI.getNPCRegistry().isNPC(entto) == true && inst.containsTarget("event:pvnpc"))
                                        || (to != null && inst.containsTarget("event:pvsentry"))) {

                                    if (!inst.isIgnored((LivingEntity) entfrom)) {
                                        inst.setTarget((LivingEntity) entfrom, true);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return;
    }
    
    @EventHandler(priority = EventPriority.HIGH)
    public void playerDamageGuard(EntityDamageByEntityEvent e){
     
        Entity entfrom = e.getDamager();
        Entity entto = e.getEntity();
        Player pfrom = null;
        Player pto = null;
        
        if (entfrom instanceof org.bukkit.entity.Projectile && entfrom instanceof Entity) {
            ProjectileSource source = ((Projectile) entfrom).getShooter();
            if (source instanceof Entity) {
                entfrom = (Entity) ((org.bukkit.entity.Projectile) entfrom).getShooter();
            }
        }
        
        if(entfrom instanceof Player){
            pfrom = (Player) entfrom;
        }
        if(entto instanceof Player){
            pto = (Player) entto;
        }
        
        if(pfrom != null && pto != null){
            nPlayer cfrom = this.plugin.getPlayerManager().playerList.get(pfrom.getUniqueId().toString());
            nPlayer cto = this.plugin.getPlayerManager().playerList.get(pto.getUniqueId().toString());
            
            if(cfrom != null && cto != null){
                if(!cfrom.getNation().equalsIgnoreCase(cto.getNation())){
                    List<Entity> inRange = pto.getNearbyEntities(20, 20, 20);

                    for (Entity entity : inRange) {
                        if (entity.hasMetadata("NPC")) {
                            SentryInstance inst = this.plugin.getGuardManager().getSentry(entity);
                            if (inst != null) {
                                String entgroup = inst.getGroup();

                                if (entgroup.equalsIgnoreCase(cto.getNation())) {
                                    inst.setTarget(pfrom, true);
                                }                                
                            }
                        }
                    }                 
                }                
            }
        }
    }
    
    @EventHandler
    public void onNPCRightClick(net.citizensnpcs.api.event.NPCRightClickEvent event) {
        SentryInstance inst = this.plugin.getGuardManager().getSentry(event.getNPC());
        
        if (inst == null) {
            return;
        }
        
        NPC ThisNPC = event.getNPC();
        String group = inst.getGroup();
        Player player = event.getClicker();

        int role = 0;
        nPlayer check = this.plugin.getPlayerManager().playerList.get(player.getUniqueId().toString());
        if(check != null){
            if (check.getNation().equalsIgnoreCase(group)) {
                if (check.getRole() >= 2) {
                    role = check.getRole();
                }
            }          
        }
        

        if (role >= 2) {
            
            if(player.getInventory().getItemInMainHand().getType().equals(Material.PAPER)){
                ItemStack tag = player.getInventory().getItemInMainHand();
                if(tag.hasItemMeta()){
                    ItemMeta meta = tag.getItemMeta();
                    if(meta.hasLore())
                    {
                        if(meta.getLore().get(0).contains("Guard Role"))
                        {
                            if(meta.getLore().get(1).contains("Bowman"))
                            {
                                this.plugin.getGuardManager().unequip(ThisNPC);
                                
                                ItemStack helm = new ItemStack(Material.LEATHER_HELMET, 1);
                                ItemStack chest = new ItemStack(Material.LEATHER_CHESTPLATE, 1);
                                ItemStack leggings = new ItemStack(Material.LEATHER_LEGGINGS, 1);
                                ItemStack boots = new ItemStack(Material.LEATHER_BOOTS, 1);
                                ItemStack bow = new ItemStack(Material.BOW, 1);

                                
                                this.plugin.getGuardManager().equip(ThisNPC, helm);
                                this.plugin.getGuardManager().equip(ThisNPC, chest);
                                this.plugin.getGuardManager().equip(ThisNPC, leggings);
                                this.plugin.getGuardManager().equip(ThisNPC, boots);
                                this.plugin.getGuardManager().equip(ThisNPC, bow);
                                
                                inst.UpdateWeapon();
                                tag.setAmount(1);
                                player.getInventory().removeItem(tag);
                                player.updateInventory();
                                event.setCancelled(true);
                            } else if(meta.getLore().get(1).contains("Swordsman"))
                            {
                                this.plugin.getGuardManager().unequip(ThisNPC);
                                
                                ItemStack helm = new ItemStack(Material.DIAMOND_HELMET, 1);
                                ItemStack chest = new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
                                ItemStack leggings = new ItemStack(Material.DIAMOND_LEGGINGS, 1);
                                ItemStack boots = new ItemStack(Material.DIAMOND_BOOTS, 1);
                                ItemStack sword = new ItemStack(Material.DIAMOND_SWORD, 1);
                                
                                this.plugin.getGuardManager().equip(ThisNPC, helm);
                                this.plugin.getGuardManager().equip(ThisNPC, chest);
                                this.plugin.getGuardManager().equip(ThisNPC, leggings);
                                this.plugin.getGuardManager().equip(ThisNPC, boots);
                                this.plugin.getGuardManager().equip(ThisNPC, sword);
                                
                                inst.UpdateWeapon();
                                tag.setAmount(1);
                                player.getInventory().removeItem(tag);
                                player.updateInventory();
                                event.setCancelled(true);
                            } else if(meta.getLore().get(1).contains("Name Tag")){
                                ThisNPC.setName(meta.getDisplayName());
                                tag.setAmount(1);
                                player.getInventory().removeItem(tag);
                                player.updateInventory();
                                event.setCancelled(true);                            
                            }
                        }
                    }
                }
            }
            
            if (player.getInventory().getItemInMainHand().getType().equals(Material.STICK)) {
                
                player.sendMessage(ChatColor.GOLD + "------- Guard Info for (" + ThisNPC.getId() + ") " + ThisNPC.getName() + "------");
                player.sendMessage(ChatColor.GREEN + inst.getStats());
                player.sendMessage(ChatColor.GREEN + "Nation: " + inst.getGroup());
                player.sendMessage(ChatColor.GOLD + "----- Target List -----");
                player.sendMessage(ChatColor.GREEN + "Targets: " + inst.validTargets.toString());
                player.sendMessage(ChatColor.GOLD + "----- Ignore List -----");
                player.sendMessage(ChatColor.GREEN + "Ignores: " + inst.ignoreTargets.toString());

            } else if (player.getInventory().getItemInMainHand().getType().equals(Material.TORCH)) {
                player.sendMessage(inst.isCrew + "");
                if (inst.isCrew) {
                    if (inst.guardTarget != null) {
                        inst.setGuardTarget(null, false);
                        inst.ignoreLeash = false;
                        inst.Spawn = player.getLocation();
                        Utilities.sendGood(player, "No longer Guarding anyone");
                    } else {
                        inst.setGuardTarget(player.getName(), true);
                        inst.ignoreLeash = true;
                        Utilities.sendGood(player, "Now Guarding you");
                    }
                } else {
                    Utilities.sendGood(player, "This Guard type is not set to Crew");
                }

            } else if(player.getInventory().getItemInMainHand().getType().equals(Material.GOLD_NUGGET)){
                
                Boolean enterWaypointMode = false;
                
                NPC waypointNPC = this.plugin.getGuardManager().WaypointMode.get(player.getUniqueId());
                if(waypointNPC != null){
                    if(waypointNPC.equals(ThisNPC))
                    {
                        Utilities.sendBad(player, "You have already entered Waypoint Mode for ths NPC");
                    } else {
                        this.plugin.getGuardManager().WaypointMode.remove(player.getUniqueId());
                        Utilities.sendBad(player, "Exited Waypoint mode for previous NPC");
                        enterWaypointMode = true;
                    }
                } else {
                    
                    enterWaypointMode = true;
                }
                
                if(enterWaypointMode == true){
                    Utilities.sendGood(player, "Entering Waypoint Mode");
                    Utilities.sendGood(player, ChatColor.BLUE + "Right Click the Ground to set a Waypoint. Use " + ChatColor.GOLD + "/exit " + ChatColor.BLUE + "to exit Waypoint Mode");
                    this.plugin.getGuardManager().WaypointMode.put(player.getUniqueId(), ThisNPC);
                }
                
            } else if (checkFood(player.getInventory().getItemInMainHand(), ThisNPC)) {
                if (inst.getHealth() == inst.sentryHealth) {
                    Utilities.sendGood(player, "This Guard is fully healed!");
                } else {
                    Utilities.sendGood(player, "This Guard has been healed a little");
                }

                if (player.getInventory().getItemInMainHand().getAmount() > 1) {
                    player.getInventory().getItemInMainHand().setAmount(player.getInventory().getItemInMainHand().getAmount() - 1);
                } else {
                    player.getInventory().setItemInMainHand(null);
                }
            } else if(player.getInventory().getItemInMainHand().getType().equals(Material.NAME_TAG)){
            
                if(player.getInventory().getItemInMainHand().getItemMeta() != null){
                    ItemStack item = player.getInventory().getItemInMainHand();
                    ItemMeta meta = item.getItemMeta();
                    if(meta.getDisplayName() != null)
                    {
                        inst.myNPC.setName(meta.getDisplayName());
                    }
                }
            
            } else {
                Utilities.sendGood(player, "Right Click with: Stick for Guard Stats, Food to heal, Gold Nugget to set Waypoints");
            }
        } else {
            if(player.getInventory().getItemInMainHand().getType().equals(Material.STICK))
            {
                player.sendMessage(ChatColor.GOLD + "------- Guard Info for (" + ThisNPC.getId() + ") " + ThisNPC.getName() + "------");
                player.sendMessage(ChatColor.GREEN + "Nation: " + inst.getGroup());
            }
            else
            {
                Utilities.sendGood(player, "You do not have the right Nation permissions to interact with this guard");
            }
        }
    }
    
    @EventHandler
    public void guardBreakBlock(BlockBreakEvent e){
        
        Player p = e.getPlayer();
        Location loc = e.getBlock().getLocation();
        Building building = this.plugin.getBuildingManager().getBuildingFromStorage(loc);
        if (building != null || e.getBlock().getType().equals(Material.CHEST)) {
            List<Entity> inRange = p.getNearbyEntities(20, 20, 20);

            for (Entity entity : inRange) {
                if (entity.hasMetadata("NPC")) {
                    SentryInstance inst = this.plugin.getGuardManager().getSentry(entity);
                    if (inst != null) {
                        String entgroup = inst.getGroup();
                        nPlayer check = this.plugin.getPlayerManager().playerList.get(p.getUniqueId().toString());
                        if (check != null) {
                            if (!entgroup.equalsIgnoreCase(check.getNation())) {
                                inst.setTarget(p, true);
                            }
                        }
                    }
                }
            }
        }       
    }
    
    @EventHandler
    public void guardReinforcedBlock(ReinforcementDamageEvent e){
        Player p = e.getPlayer();
        PlayerReinforcement rein = (PlayerReinforcement)e.getReinforcement();
        if(rein != null){
            if (!rein.canBypass(p)) {
                List<Entity> inRange = p.getNearbyEntities(20, 20, 20);
                
                for (Entity entity : inRange) {
                    if (entity.hasMetadata("NPC")) {
                        SentryInstance inst = this.plugin.getGuardManager().getSentry(entity);
                        if (inst != null) {
                            String entgroup = inst.getGroup();
                            nPlayer check = this.plugin.getPlayerManager().playerList.get(p.getUniqueId().toString());
                            if (check != null) {
                                if (!entgroup.equalsIgnoreCase(check.getNation())) {
                                    inst.setTarget(p, true);
                                }                                                          
                            }
                        }
                    }
                }
            }
        } else {
            Location loc = e.getBlock().getLocation();
            Building building = this.plugin.getBuildingManager().getBuildingFromStorage(loc);
            if(building != null || e.getBlock().getType().equals(Material.CHEST))
            {
                List<Entity> inRange = p.getNearbyEntities(20, 20, 20);
                
                for (Entity entity : inRange) {
                    if (entity.hasMetadata("NPC")) {
                        SentryInstance inst = this.plugin.getGuardManager().getSentry(entity);
                        if (inst != null) {
                            String entgroup = inst.getGroup();
                            nPlayer check = this.plugin.getPlayerManager().playerList.get(p.getUniqueId().toString());
                            if (check != null) {
                                if (!entgroup.equalsIgnoreCase(check.getNation())) {
                                    inst.setTarget(p, true);
                                }                                                          
                            }
                        }
                    }
                }              
            }            
        }
    }
    
    
    
    @EventHandler(priority = EventPriority.LOW)
    public void guardCommandPreprocess(PlayerCommandPreprocessEvent event) {
        String[] split = event.getMessage().substring(1).split(" ");
        if (split.length == 0) {
            return;
        }
        String command = split[0];

        if (command.equalsIgnoreCase("attack")) {
            event.setCancelled(true);
            //PLEASE STATE WHO YOU WOULD LIKE TO ATTACK
            //NATION
            //PLAYER
            //ENTITY


        } else if (command.equalsIgnoreCase("ignore")) {
            event.setCancelled(true);
            //PLEASE STATE WHO YOU WOULD LIKE TO ATTACK
            //NATION
            //PLAYER
            //ENTITY
            
            
        } else if (command.equalsIgnoreCase("noattack")) {
            event.setCancelled(true);
            //PLEASE STATE WHO YOU WOULD LIKE TO ATTACK
            //NATION
            //PLAYER
            //ENTITY            

        } else if (command.equalsIgnoreCase("noignore")) {
            event.setCancelled(true);
            //PLEASE STATE WHO YOU WOULD LIKE TO ATTACK
            //NATION
            //PLAYER
            //ENTITY            
        }
    }

    public void handleIgnore(Player player, Sign sign) {
        String type = sign.getLine(1);
        String object = sign.getLine(2);
        String guardGroup = sign.getLine(3);

        if (guardGroup.length() < 1) {
            Utilities.sendGood(player, "Guard Sign is missing Guard NameLayer Group");
            return;
        } else if (type.length() < 1) {
            Utilities.sendGood(player, "Guard Sign is missing Type of ignore: NameLayer, Entity, Player");
            return;
        } else if (object.length() < 1) {
            Utilities.sendGood(player, "Guard Sign is missing the Object of ignore: NameLayer group name, Entity Type, Player Name");
            return;
        } else {
            
            int role = 0;
            
            nPlayer check = this.plugin.getPlayerManager().playerList.get(player.getUniqueId().toString());
            if (check != null) {
                if (check.getNation().equalsIgnoreCase(guardGroup)) {
                    role = check.getRole();
                }
            }



            if (role >= 2) {
                String ignore;
                if (type.equalsIgnoreCase("player")) {
                    ignore = "PLAYER:";
                } else if (type.equalsIgnoreCase("nation")) {
                    ignore = "NATION:";
                } else if (type.equalsIgnoreCase("entity")) {
                    ignore = "ENTITY:";
                } else {
                    Utilities.sendGood(player, "Guard Sign ignore type is not valid, must be: Nation, Player, or Entity");
                    return;
                }

                ignore = ignore + object.toUpperCase();

                List<Entity> inRange = player.getNearbyEntities(20, 20, 20);
                int i = 0;
                for (Entity entity : inRange) {
                    if (entity.hasMetadata("NPC")) {
                        SentryInstance inst = this.plugin.getGuardManager().getSentry(entity);
                        if (inst != null) {
                            String entgroup = inst.getGroup();

                            if (entgroup.equalsIgnoreCase(guardGroup)) {

                                if (!inst.containsIgnore(ignore.toUpperCase())) {
                                    inst.ignoreTargets.add(ignore.toUpperCase());
                                    inst.processTargets();
                                    i++;
                                }
                            }
                        }
                    }
                }
                if (i > 0) {
                    Utilities.sendGood(player, i + " Guards have been set to ignore: " + ignore);
                } else {
                    Utilities.sendGood(player, "No Guards were found within range");
                }
            } else {
                Utilities.sendGood(player, "You do not have enough permissions for nation: " + guardGroup);
            }
        }
    }

    public void handleNoIgnore(Player player, Sign sign) {
        String type = sign.getLine(1);
        String object = sign.getLine(2);
        String guardGroup = sign.getLine(3);

        if (guardGroup.length() < 1) {
            Utilities.sendGood(player, "Guard Sign is missing Guard NameLayer Group");
            return;
        } else if (type.length() < 1) {
            Utilities.sendGood(player, "Guard Sign is missing Type of ignore: NameLayer, Entity, Player");
            return;
        } else if (object.length() < 1) {
            Utilities.sendGood(player, "Guard Sign is missing the Object of ignore: NameLayer group name, Entity Type, Player Name");
            return;
        } else {
            int role = 0;
            
            nPlayer check = this.plugin.getPlayerManager().playerList.get(player.getUniqueId().toString());
            if (check != null) {
                if (check.getNation().equalsIgnoreCase(guardGroup)) {
                    role = check.getRole();
                }
            }

            if (role >= 2) {
                String ignore;
                if (type.equalsIgnoreCase("player")) {
                    ignore = "PLAYER:";
                } else if (type.equalsIgnoreCase("nation")) {
                    ignore = "NATION:";
                } else if (type.equalsIgnoreCase("entity")) {
                    ignore = "ENTITY:";
                } else {
                    Utilities.sendGood(player, "Guard Sign ignore type is not valid, must be: Nation, Player, or Entity");
                    return;
                }

                ignore = ignore + object.toUpperCase();

                List<Entity> inRange = player.getNearbyEntities(20, 20, 20);
                int i = 0;
                for (Entity entity : inRange) {
                    if (entity.hasMetadata("NPC")) {
                        SentryInstance inst = this.plugin.getGuardManager().getSentry(entity);
                        if (inst != null) {
                            String entgroup = inst.getGroup();

                            if (entgroup.equalsIgnoreCase(guardGroup)) {

                                if (inst.containsIgnore(ignore.toUpperCase())) {
                                    inst.ignoreTargets.remove(ignore.toUpperCase());
                                    inst.processTargets();
                                    i++;
                                }
                            }
                        }
                    }
                }
                if (i > 0) {
                    Utilities.sendGood(player, i + " Guards have removed from ignore list: " + ignore);
                } else {
                    Utilities.sendGood(player, "No Guards were found within range");
                }
            } else {
                Utilities.sendGood(player, "You do not have enough permissions for nation: " + guardGroup);
            }
        }

    }

    public void handleTarget(Player player, Sign sign) {
        String type = sign.getLine(1);
        String object = sign.getLine(2);
        String guardGroup = sign.getLine(3);

        if (guardGroup.length() < 1) {
            Utilities.sendGood(player, "Guard Sign is missing Guard NameLayer Group");
            return;
        } else if (type.length() < 1) {
            Utilities.sendGood(player, "Guard Sign is missing Type of target: NameLayer, Entity, Player");
            return;
        } else if (object.length() < 1) {
            Utilities.sendGood(player, "Guard Sign is missing the Object of target: NameLayer group name, Entity Type, Player Name");
            return;
        } else {
            int role = 0;
            
            nPlayer check = this.plugin.getPlayerManager().playerList.get(player.getUniqueId().toString());
            if (check != null) {
                if (check.getNation().equalsIgnoreCase(guardGroup)) {
                    role = check.getRole();
                }
            }

            if (role >= 2) {
                String target;
                if (type.equalsIgnoreCase("player")) {
                    target = "PLAYER:";
                } else if (type.equalsIgnoreCase("nation")) {
                    target = "NATION:";
                } else if (type.equalsIgnoreCase("entity")) {
                    target = "ENTITY:";
                } else {
                    Utilities.sendGood(player, "Guard Sign ignore type is not valid, must be: Nation, Player, or Entity");
                    return;
                }

                target = target + object.toUpperCase();

                List<Entity> inRange = player.getNearbyEntities(20, 20, 20);
                int i = 0;
                for (Entity entity : inRange) {
                    if (entity.hasMetadata("NPC")) {
                        SentryInstance inst = this.plugin.getGuardManager().getSentry(entity);
                        if (inst != null) {
                            String entgroup = inst.getGroup();

                            if (entgroup.equalsIgnoreCase(guardGroup)) {

                                if (!inst.containsTarget(target.toUpperCase())) {
                                    inst.validTargets.add(target.toUpperCase());
                                    inst.processTargets();
                                    i++;
                                }
                            }
                        }
                    }
                }
                if (i > 0) {
                    Utilities.sendGood(player, i + " Guards have been set to target: " + target);
                } else {
                    Utilities.sendGood(player, "No Guards were found within range");
                }
            } else {
                Utilities.sendGood(player, "You do not have enough permissions for nation: " + guardGroup);
            }
        }

    }

    public void handleNoTarget(Player player, Sign sign) {
        String type = sign.getLine(1);
        String object = sign.getLine(2);
        String guardGroup = sign.getLine(3);

        if (guardGroup.length() < 1) {
            Utilities.sendGood(player, "Guard Sign is missing Guard NameLayer Group");
            return;
        } else if (type.length() < 1) {
            Utilities.sendGood(player, "Guard Sign is missing Type of target: NameLayer, Entity, Player");
            return;
        } else if (object.length() < 1) {
            Utilities.sendGood(player, "Guard Sign is missing the Object of target: NameLayer group name, Entity Type, Player Name");
            return;
        } else {
            int role = 0;
            
            nPlayer check = this.plugin.getPlayerManager().playerList.get(player.getUniqueId().toString());
            if (check != null) {
                if (check.getNation().equalsIgnoreCase(guardGroup)) {
                    role = check.getRole();
                }
            }

            if (role >= 2) {
                String target;
                if (type.equalsIgnoreCase("player")) {
                    target = "PLAYER:";
                } else if (type.equalsIgnoreCase("nation")) {
                    target = "NATION:";
                } else if (type.equalsIgnoreCase("entity")) {
                    target = "ENTITY:";
                } else {
                    Utilities.sendGood(player, "Guard Sign ignore type is not valid, must be: Nation, Player, or Entity");
                    return;
                }

                target = target + object.toUpperCase();

                List<Entity> inRange = player.getNearbyEntities(20, 20, 20);
                int i = 0;
                for (Entity entity : inRange) {
                    if (entity.hasMetadata("NPC")) {
                        SentryInstance inst = this.plugin.getGuardManager().getSentry(entity);
                        if (inst != null) {
                            String entgroup = inst.getGroup();

                            if (entgroup.equalsIgnoreCase(guardGroup)) {

                                if (inst.containsTarget(target.toUpperCase())) {
                                    inst.validTargets.remove(target.toUpperCase());
                                    inst.processTargets();
                                    i++;
                                }
                            }
                        }
                    }
                }
                if (i > 0) {
                    Utilities.sendGood(player, i + " Guards have removed from target list: " + target);
                } else {
                    Utilities.sendGood(player, "No Guards were found within range");
                }
            } else {
                Utilities.sendGood(player, "You do not have enough permissions for nation: " + guardGroup);
            }
        }

    }

    private boolean checkFood(ItemStack food, NPC myNPC) {

        SentryInstance inst = this.plugin.getGuardManager().getSentry(myNPC);
        if (inst == null) {
            return false;
        } else {
            Double health = inst.getHealth();
            if (food.getType().equals(Material.COOKED_BEEF)) {
                Double add = this.plugin.getConfig().getDouble("DefaultHealing.CookedBeef");
                inst.setHealth(health + add);
                return true;
            } else if (food.getType().equals(Material.GRILLED_PORK)) {
                Double add = this.plugin.getConfig().getDouble("DefaultHealing.CookedPork");
                inst.setHealth(health + add);
                return true;
            } else if (food.getType().equals(Material.COOKED_MUTTON)) {
                Double add = this.plugin.getConfig().getDouble("DefaultHealing.CookedMutton");
                inst.setHealth(health + add);
                return true;
            } else if (food.getType().equals(Material.COOKED_CHICKEN)) {
                Double add = this.plugin.getConfig().getDouble("DefaultHealing.CookedChicken");
                inst.setHealth(health + add);
                return true;
            } else if (food.getType().equals(Material.BREAD)) {
                Double add = this.plugin.getConfig().getDouble("DefaultHealing.Bread");
                inst.setHealth(health + add);
                return true;
            } else if (food.getType().equals(Material.COOKED_FISH)) {
                Double add = this.plugin.getConfig().getDouble("DefaultHealing.CookedFish");
                inst.setHealth(health + add);
                return true;
            } else if (food.getType().equals(Material.MUSHROOM_SOUP)) {
                Double add = this.plugin.getConfig().getDouble("DefaultHealing.MushroomStew");
                inst.setHealth(health + add);
                return true;
            } else if (food.getType().equals(Material.APPLE)) {
                Double add = this.plugin.getConfig().getDouble("DefaultHealing.Apple");
                inst.setHealth(health + add);
                return true;
            } else if (food.getType().equals(Material.MELON)) {
                Double add = this.plugin.getConfig().getDouble("DefaultHealing.MelonSlice");
                inst.setHealth(health + add);
                return true;
            } else if (food.getType().equals(Material.PUMPKIN_PIE)) {
                Double add = this.plugin.getConfig().getDouble("DefaultHealing.PumpkinPie");
                inst.setHealth(health + add);
                return true;
            } else {
                return false;
            }
        }
    }
}
