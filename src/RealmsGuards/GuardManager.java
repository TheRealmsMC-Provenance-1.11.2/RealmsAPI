package RealmsGuards;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.trait.trait.Equipment;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Projectile;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import Realms.RealmsAPI;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 *
 * @author lanca
 */
public class GuardManager {
    
    private RealmsAPI plugin;
    
    public int archer = -1;

    public Map<Integer, Double> ArmorBuffs = new HashMap<Integer, Double>();
    public Map<Integer, Double> SpeedBuffs = new HashMap<Integer, Double>();
    public Map<Integer, Double> StrengthBuffs = new HashMap<Integer, Double>();
    public Map<Integer, List<PotionEffect>> WeaponEffects = new HashMap<Integer, List<PotionEffect>>();
    
    public HashMap<UUID, NPC> WaypointMode = new HashMap<UUID, NPC>();
    
    public ArrayList<String> commanding = new ArrayList<String>();
    public HashMap<String, String> command = new HashMap<String, String>();

    public Queue<Projectile> arrows = new LinkedList<Projectile>();

    public List<Integer> Boots = new LinkedList<Integer>(java.util.Arrays.asList(301, 305, 309, 313, 317));
    public List<Integer> Chestplates = new LinkedList<Integer>(java.util.Arrays.asList(299, 303, 307, 311, 315));
    public List<Integer> Leggings = new LinkedList<Integer>(java.util.Arrays.asList(300, 304, 308, 312, 316));
    public List<Integer> Helmets = new LinkedList<Integer>(java.util.Arrays.asList(298, 302, 306, 310, 314, 91, 86));

    public boolean debug = true;

    public boolean DieLikePlayers = false;
    public boolean BodyguardsObeyProtection = true;
    public boolean IgnoreListInvincibility = true;
    public boolean GroupsChecked = false;

    public int SentryEXP = 5;
    
    
    private static File waypointYml = null;            
    private static FileConfiguration waypointConfig = null;
    
    
    
    public GuardManager(RealmsAPI plugin){
        this.plugin = plugin;
        
        reloadWaypoints();
        
        
    }
    
    public void reloadWaypoints()
    {
        if(waypointYml == null)
        {
            waypointYml = new File(plugin.getDataFolder() + "/guards", "waypoints.yml");
            
        }
        waypointConfig = YamlConfiguration.loadConfiguration(waypointYml);        
    }
    
    public boolean unequip(NPC npc){
        Equipment trait = npc.getTrait(Equipment.class);
        if(trait == null)
        {
            return false;
        }
        
        for (int i = 0; i <= 5; i++) {
            if (trait.get(i) != null && trait.get(i).getType() != Material.AIR) {
                try {
                    this.plugin.debug("This trait slot " + i);
                    trait.set(i, null);
                } catch (Exception e) {
                }
            }
        }
        
        return true;
    }

    public boolean equip(NPC npc, ItemStack hand) {
        Equipment trait = npc.getTrait(Equipment.class);
        if (trait == null) {
            return false;
        }
        int slot = 0;
        Material type = hand == null ? Material.AIR : hand.getType();

        if (Helmets.contains(type.getId())) {
            slot = 1;
        } else if (Chestplates.contains(type.getId())) {
            slot = 2;
        } else if (Leggings.contains(type.getId())) {
            slot = 3;
        } else if (Boots.contains(type.getId())) {
            slot = 4;
        }

        if (type == Material.AIR) {
            for (int i = 0; i < 5; i++) {
                if (trait.get(i) != null && trait.get(i).getType() != Material.AIR) {
                    try {
                        trait.set(i, null);
                    } catch (Exception e) {
                    }
                }
            }
            return true;
        } else {
            ItemStack clone = hand.clone();
            clone.setAmount(1);

            try {
                trait.set(slot, clone);
            } catch (Exception e) {
                return false;
            }
            return true;
        }

    }
    
    private int GetMat(String S) {
        int item = -1;

        if (S == null) {
            return item;
        }

        String[] args = S.toUpperCase().split(":");

        org.bukkit.Material M = org.bukkit.Material.getMaterial(args[0]);

        if (item == -1) {
            try {
                item = Integer.parseInt(S.split(":")[0]);
            } catch (Exception e) {
            }
        }

        if (M != null) {
            item = M.getId();
        }

        return item;
    }
    
    private PotionEffect getpot(String S) {
        if (S == null) {
            return null;
        }
        String[] args = S.trim().split(":");

        PotionEffectType type = null;

        int dur = 10;
        int amp = 1;

        type = PotionEffectType.getByName((args[0].toUpperCase()));

        if (type == null) {
            try {
                type = PotionEffectType.getById(Integer.parseInt(args[0]));
            } catch (Exception e) {
            }
        }

        if (type == null) {
            return null;
        }

        if (args.length > 1) {
            try {
                dur = Integer.parseInt(args[1]);
            } catch (Exception e) {
            }
        }

        if (args.length > 2) {
            try {
                amp = Integer.parseInt(args[2]);
            } catch (Exception e) {
            }
        }

        return new PotionEffect(type, dur, amp);
    }
    
    public SentryInstance getSentry(Entity ent) {
        if (ent == null) {
            return null;
        }
        if (!(ent instanceof org.bukkit.entity.LivingEntity)) {
            return null;
        }
        NPC npc = net.citizensnpcs.api.CitizensAPI.getNPCRegistry().getNPC(ent);
        if (npc != null && npc.hasTrait(SentryTrait.class)) {
            return npc.getTrait(SentryTrait.class).getInstance();
        }

        return null;
    }

    public SentryInstance getSentry(NPC npc) {
        if (npc != null && npc.hasTrait(SentryTrait.class)) {
            return npc.getTrait(SentryTrait.class).getInstance();
        }
        return null;
    }

    public void loaditemlist(String key, List<Integer> list) {
        List<String> strs = this.plugin.getConfig().getStringList(key);

        if (strs.size() > 0) {
            list.clear();
        }

        for (String s : this.plugin.getConfig().getStringList(key)) {
            int item = GetMat(s.trim());
            list.add(item);
        }

    }

    private void loadmap(String node, Map<Integer, Double> map) {
        map.clear();
        for (String s : this.plugin.getConfig().getStringList(node)) {
            String[] args = s.trim().split(" ");
            if (args.length != 2) {
                continue;
            }

            double val = 0;

            try {
                val = Double.parseDouble(args[1]);
            } catch (Exception e) {
            }

            int item = GetMat(args[0]);

            if (item > 0 && val != 0 && !map.containsKey(item)) {
                map.put(item, val);
            }
        }
    }

    private void loadpots(String node, Map<Integer, List<PotionEffect>> map) {
        map.clear();
        for (String s : this.plugin.getConfig().getStringList(node)) {
            String[] args = s.trim().split(" ");

            if (args.length < 2) {
                continue;
            }

            int item = GetMat(args[0]);

            List<PotionEffect> list = new ArrayList<PotionEffect>();

            for (int i = 1; i < args.length; i++) {
                PotionEffect val = getpot(args[i]);
                if (val != null) {
                    list.add(val);
                }

            }

            if (item > 0 && list.isEmpty() == false) {
                map.put(item, list);
            }

        }
    }
    
    public void reloadMyConfig() {
        loadmap("ArmorBuffs", ArmorBuffs);
        loadmap("StrengthBuffs", StrengthBuffs);
        loadmap("SpeedBuffs", SpeedBuffs);
        loadpots("WeaponEffects", WeaponEffects);
        loaditemlist("Helmets", Helmets);
        loaditemlist("Chestplates", Chestplates);
        loaditemlist("Leggings", Leggings);
        loaditemlist("Boots", Boots);
        archer = GetMat(this.plugin.getConfig().getString("AttackTypes.Archer", null));
        DieLikePlayers = this.plugin.getConfig().getBoolean("Server.DieLikePlayers", false);
        BodyguardsObeyProtection = this.plugin.getConfig().getBoolean("Server.BodyguardsObeyProtection", true);
        IgnoreListInvincibility = this.plugin.getConfig().getBoolean("Server.IgnoreListInvincibility", true);
        SentryEXP = this.plugin.getConfig().getInt("Server.ExpValue", 5);
    }    

    public void saveWaypoints()
    {
        if (waypointConfig == null || waypointYml == null) {
            return;
        }
        try {
            getCustomConfig().save(waypointYml);
        } catch (IOException ex) {
            plugin.getLogger().log(Level.SEVERE, "Could not save config to " + waypointYml, ex);
        }   
    }
    
    public void addGuardWaypoint(NPC npc){
        int id = npc.getId();
        SentryInstance inst = this.plugin.getGuardManager().getSentry(npc);
        if(inst != null)
        {
            int i = 0;
            for(Location loc : inst.waypoints)
            {
                String world = loc.getWorld().getName();
                int x = loc.getBlockX();
                int y = loc.getBlockY();
                int z = loc.getBlockZ();
                        
                waypointConfig.set(id + ".locations." + i + ".world", world);
                waypointConfig.set(id + ".locations." + i + ".x", x);
                waypointConfig.set(id + ".locations." + i + ".y", y);
                waypointConfig.set(id + ".locations." + i + ".z", z);
                i++;
            }       
        } 
    }
    
    public void loadGuardWaypoints(NPC npc){
        int id = npc.getId();
        SentryInstance inst = this.plugin.getGuardManager().getSentry(npc);
        if(inst != null){
            
            for(int i = 0; i < 10; i++)
            {
                String world = waypointConfig.getString(id + ".locations." + i + ".world");
                if(world != null)
                {
                    int x = waypointConfig.getInt(id + ".locations." + i + ".x");
                    int y = waypointConfig.getInt(id + ".locations." + i + ".y");
                    int z = waypointConfig.getInt(id + ".locations." + i + ".z");

                    Location loc = new Location(Bukkit.getWorld(world), x, y, z);
                    inst.addWaypoint(loc);                 
                }
            } 
        }    
    }

    public FileConfiguration getCustomConfig() {
        if (waypointConfig == null) {
            reloadWaypoints(); //LINE 37
        }
        return waypointConfig;
    }
}
