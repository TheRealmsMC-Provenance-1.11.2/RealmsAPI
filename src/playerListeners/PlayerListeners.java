package playerListeners;

import BuildingManager.Building;
import BuildingManager.BuildingDesign;
import CenterUpg.CenterUpgStuff;
import InviteManager.InviteManager.Request;
import NationManager.nNation;
import NationManager.nResources;
import PlayerManager.PlayerManager.nPlayer;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.meta.ItemMeta;
import Realms.RealmsAPI;
import RealmsGuards.SentryInstance;
import Center.CenterStuff;
import chatapi.Utilities;
import chatapi.titles.TitleAPI;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.ChatColor;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;


// --------EXAMPLE TO CHECK IF A BUILDING EXISTS ------
// --------- and create one if it doesn't already -----
// Location loc = e.getClickedBlock().getLocation();                    
//String test = "test";
//Building building = plugin.getBuildingManager().checkBuilding(loc, test);
//
//--------EXAMPLE TO GET A BUILDING ----------
// Location loc = e.getClickedBlock().getLocation();
// Building building = plugin.getBuildingManager().getBuildingFromStorage(loc);
// if(building != null)
//        Then it's a building

public class PlayerListeners implements Listener {
    
    public RealmsAPI plugin;
    
    public static ConcurrentHashMap<UUID, Integer> playerTargets = new ConcurrentHashMap<UUID, Integer>();
    
    public PlayerListeners(RealmsAPI plugin){
    
        this.plugin = plugin;   
    }
    
    @EventHandler(ignoreCancelled = true)
    public void onPlayerNationJoinEvent(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        
        this.plugin.getPlayerManager().addPlayer(player);
        this.plugin.getPlayerManager().loadPlayer(player.getUniqueId().toString());
        nPlayer check = this.plugin.getPlayerManager().playerList.get(player.getUniqueId().toString());
        if(check != null)
        {
            if(this.plugin.getBuildingManager().messages.containsKey(check.getNation().toLowerCase()))
            {
                ArrayList<String> notifications = this.plugin.getBuildingManager().messages.get(check.getNation().toLowerCase());
                for(String message : notifications)
                {
                    Utilities.sendBad(player, message);
                }
            }
        }
    }
    
    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGH)
    public void onPlayerNationLeaveEvent(PlayerQuitEvent e){
        Player player = e.getPlayer();
        this.plugin.getPlayerManager().savePlayer(player.getUniqueId().toString());
        if (this.plugin.getGuardManager().WaypointMode.containsKey(player.getUniqueId())) {
            this.plugin.getGuardManager().WaypointMode.remove(player.getUniqueId());
        }
    }
    
    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
        String[] split = event.getMessage().substring(1).split(" ");
        if (split.length == 0) {
            return;
        }
        String command = split[0];

        if (command.equalsIgnoreCase("join")) {
            event.setCancelled(true);
            String playername = event.getPlayer().getName().toLowerCase();
            Request req = this.plugin.invites().get(playername);

            if (req != null) {
                this.plugin.acceptInvite(req);
            }

        } else if (command.equalsIgnoreCase("ignore")) {
            event.setCancelled(true);
            String playername = event.getPlayer().getName().toLowerCase();
            Request req = this.plugin.invites().get(playername);

            if (req != null) {
                this.plugin.denyInvite(req);
            }
        } else if (command.equalsIgnoreCase("exit")) {
            event.setCancelled(true);
            
            if(this.plugin.getGuardManager().WaypointMode.containsKey(event.getPlayer().getUniqueId())){
                Utilities.sendGood(event.getPlayer(), "Exited Waypoint Mode");
                this.plugin.getGuardManager().WaypointMode.remove(event.getPlayer().getUniqueId());
            }
        } else if (command.equalsIgnoreCase("clear")) {
            event.setCancelled(true);
            
            if (this.plugin.getGuardManager().WaypointMode.containsKey(event.getPlayer().getUniqueId())) {
                NPC ThisNPC = this.plugin.getGuardManager().WaypointMode.get(event.getPlayer().getUniqueId());
                SentryInstance inst = this.plugin.getGuardManager().getSentry(ThisNPC);
                if (ThisNPC.isSpawned()) {
                    inst.clearWaypoints();
                    Utilities.sendGood(event.getPlayer(), "Cleared the waypoints for your current Guard");
                }
            }
        }
    }
    
    @EventHandler
    public void townMobSpawn(CreatureSpawnEvent e){
        if(!e.getSpawnReason().equals(SpawnReason.BREEDING) || !e.getSpawnReason().equals(SpawnReason.CUSTOM))
        {
            Location loc = CenterUpgStuff.moveSignals(e.getEntity(), e.getLocation(), 50);

            if (loc != null) {
                boolean insideTown = false;

                double firstx = loc.getX() - 100;
                double firstz = loc.getZ() - 100;

                double secondx = loc.getX() + 100;
                double secondz = loc.getZ() + 100;

                double maxX = Math.max(firstx, secondx);
                double minX = Math.min(firstx, secondx);
                double maxZ = Math.max(firstz, secondz);
                double minZ = Math.min(firstz, secondz);

                Location To = e.getLocation();

                if ((To.getX() >= minX) && (To.getZ() >= minZ)
                        && (To.getX() <= maxX) && (To.getZ() <= maxZ)) {
                    insideTown = true;
                }

                if (insideTown) {
                    e.setCancelled(true);
                }
            }

        }
    }
        
    @EventHandler
    public void buildingCreatEvent(PlayerInteractEvent e) {
        Player player = e.getPlayer();

        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (e.getItem() != null && e.getItem().getType() == Material.PAPER) {
                ItemMeta paper = e.getItem().getItemMeta();
                if (paper == null) {
                    return;
                }
                if (paper.hasLore()) {

                    if (paper.getLore().get(0).contains("Nation Building Permit")) {
                        String buildType = paper.getLore().get(1);
                        if (buildType == null) {
                            player.sendMessage("BuildType == null");
                            return;
                        }

                        Location loc = e.getClickedBlock().getLocation();
                        String nation = "none";
                        int role = 0;
                        nPlayer check = this.plugin.getPlayerManager().playerList.get(player.getUniqueId().toString());
                        if(check != null){
                            nation = check.getNation();
                            role = check.getRole();                   
                        }

                        if ((!nation.equalsIgnoreCase("none")) && role >= 2) {

                            nNation buildNation = null;
                            for(nNation checkNation : this.plugin.getNationManager().nationList)
                            {
                                if(checkNation.getName().equalsIgnoreCase(nation))
                                {
                                    buildNation = checkNation;
                                }
                                
                            }
                            
                            if(buildNation != null)
                            {
                                Building building = this.plugin.getBuildingManager().checkBuilding(loc, nation);
                                if (building != null) {
                                    
                                    BuildingDesign design = this.plugin.getBuildingDesign(building.getDesignID());
                                    nResources checkNation = null;

                                    for (nResources resource : this.plugin.getNationManager().nationResources) {
                                        if (resource.getName().equalsIgnoreCase(nation)) {
                                            checkNation = resource;
                                        }
                                    }
                                    
                                    String designID = building.getDesignID();
                                    if(designID != null)
                                    {
                                        
                                        if(this.plugin.getNationManager().checkBuildable(player, designID, buildNation))
                                        {
                                            if (checkNation != null) {
                                                boolean cost = this.plugin.getNationManager().checkCost(player, design, checkNation);
                                                if (cost) {
                                                    checkNation.setIron(checkNation.getIron() - design.getIron());
                                                    checkNation.setGold(checkNation.getGold() - design.getGold());
                                                    checkNation.setEmerald(checkNation.getEmerald() - design.getEmerald());
                                                    checkNation.setDiamond(checkNation.getDiamond() - design.getDiamond());
                                                    checkNation.setRedstone(checkNation.getRedstone() - design.getRedstone());
                                                    checkNation.setCoal(checkNation.getCoal() - design.getCoal());
                                                    checkNation.setStone(checkNation.getStone() - design.getStone());
                                                    checkNation.setLogs(checkNation.getLogs() - design.getLogs());
                                                    checkNation.setInk(checkNation.getInk() - design.getInk());
                                                    checkNation.setBooks(checkNation.getBooks() - design.getBooks());
                                                    checkNation.setFood(checkNation.getFood() - design.getFood());

                                                    switch(designID.toLowerCase()){
                                                        case "barracks": 
                                                            this.plugin.getBuildingCreate().createBarracks(player, building);
                                                            break;
                                                        case "kiln":
                                                            this.plugin.getBuildingCreate().createKiln(player, building);
                                                            break;
                                                        case "center":
                                                            this.plugin.getBuildingCreate().createCenter(player, building);
                                                            break;
                                                        case "smeltery":
                                                            this.plugin.getBuildingCreate().createSmeltery(player, building);
                                                            break;
                                                        case "centerupg":
                                                            this.plugin.getBuildingCreate().createCenterUpg(player, building);
                                                            break;
                                                        case "crusher":
                                                            this.plugin.getBuildingCreate().createCrusher(player, building);
                                                            break;
                                                        case "pier":
                                                            this.plugin.getBuildingCreate().createPier(player, building);
                                                            break;
                                                        case "cityhall":
                                                            this.plugin.getBuildingCreate().createCityHall(player, building);
                                                            break;
                                                        case "library":
                                                            this.plugin.getBuildingCreate().createLibrary(player, building);
                                                            break;
                                                        case "printingpress":
                                                            this.plugin.getBuildingCreate().createPrintingPress(player, building);
                                                            break;
                                                        case "granary":
                                                            this.plugin.getBuildingCreate().createGranary(player, building);
                                                            break;
                                                        default: 
                                                            break;
                                                    }
                                                } else{
                                                    Utilities.sendBad(player, "You do not have the required Resources in your Nation Bank");
                                                }
                                            }
                                        } else {
                                            Utilities.sendBad(player, "Your nation has reached the Max for this Building");
                                        }                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void breakingBlockBuild(BlockBreakEvent e) {
        if(!e.isCancelled())
        {
            Location loc = e.getBlock().getLocation();

            Building check = this.plugin.getBuildingManager().getBuildingFromStorage(loc);
            if (check != null) {
                this.plugin.getBuildingManager().breakBuilding(check.getUUID());

            }          
        }
    }
    
    @EventHandler
    public void crusherEvent(PlayerInteractEvent e){
        Player player = e.getPlayer();
        if(e.getAction() == Action.RIGHT_CLICK_BLOCK){
            
            Location loc = e.getClickedBlock().getLocation();
            Building building = plugin.getBuildingManager().getBuildingFromStorage(loc);
            if(building != null)
            {
                nPlayer playerCheck = this.plugin.getPlayerManager().playerList.get(player.getUniqueId().toString());
                if (playerCheck != null) {
                    if(playerCheck.getNation().equalsIgnoreCase(building.getNation())){

                        if (building.getDesignID().equalsIgnoreCase("crusher")) {

                            String nation = "none";

                            nPlayer check = this.plugin.getPlayerManager().playerList.get(player.getUniqueId().toString());
                            if (check != null) {
                                nation = check.getNation();
                            }

                            if (building.getNation().equalsIgnoreCase(nation)) {
                                if (player.getInventory().getItemInMainHand().getType().equals(Material.COBBLESTONE) || player.getInventory().getItemInMainHand().getType().equals(Material.GRAVEL)
                                        || player.getInventory().getItemInMainHand().getType().equals(Material.SAND)) {

                                    ItemStack item = player.getInventory().getItemInMainHand();
                                    

                                    if (item.getType().equals(Material.COBBLESTONE)) {
                                        int amount = 0;

                                        if (player.isSneaking()) {
                                            amount = item.getAmount();
                                            player.getInventory().setItemInMainHand(null);
                                            player.updateInventory();
                                        } else {
                                            amount = 1;
                                            player.getInventory().setItemInMainHand(null);
                                            player.updateInventory();
                                            item.setAmount(item.getAmount() - 1);
                                            player.getLocation().getWorld().dropItem(player.getLocation(), item);
                                        }

                                        amount = amount * 2;

                                        if (amount > 64) {
                                            ItemStack overflow = item.clone();
                                            overflow.setAmount(amount - 64);
                                            item.setType(Material.GRAVEL);
                                            overflow.setType(Material.GRAVEL);
                                            player.getLocation().getWorld().dropItem(player.getLocation(), overflow);
                                            player.getLocation().getWorld().dropItem(player.getLocation(), item);
                                        } else {
                                            item.setType(Material.GRAVEL);
                                            item.setAmount(amount);
                                            player.getLocation().getWorld().dropItem(player.getLocation(), item);
                                        }

                                    } else if (item.getType().equals(Material.GRAVEL)) {
                                        int amount = 0;

                                        if (player.isSneaking()) {
                                            amount = item.getAmount();
                                            player.getInventory().setItemInMainHand(null);
                                            player.updateInventory();
                                        } else {
                                            amount = 1;
                                            player.getInventory().setItemInMainHand(null);
                                            player.updateInventory();
                                            item.setAmount(item.getAmount() - 1);
                                            player.getLocation().getWorld().dropItem(player.getLocation(), item);
                                        }
                                        
                                        amount = amount * 2;

                                        if (amount > 64) {
                                            ItemStack overflow = item.clone();
                                            overflow.setAmount(amount - 64);
                                            item.setType(Material.SAND);
                                            overflow.setType(Material.SAND);
                                            player.getLocation().getWorld().dropItemNaturally(player.getLocation(), overflow);
                                            player.getLocation().getWorld().dropItemNaturally(player.getLocation(), item);
                                        } else {
                                            item.setType(Material.SAND);
                                            item.setAmount(amount);
                                            player.getLocation().getWorld().dropItemNaturally(player.getLocation(), item);
                                        }
                                    } else if (item.getType().equals(Material.SAND) && item.getAmount() == 64) {
                                        int amount = 0;

                                        if (player.isSneaking()) {
                                            amount = item.getAmount();
                                            player.getInventory().setItemInMainHand(null);
                                            player.updateInventory();
                                        } else {
                                            Utilities.sendBad(player, "You must use 64 Sand in the Crusher");
                                            e.setCancelled(true);
                                            return;
                                        }

                                        Random rand = new Random();
                                        int n = rand.nextInt(300) + 1;

                                        if (n >= 0 && n <= 5) {
                                            //DIAMONDS
                                            int diamondAmount = rand.nextInt(3) + 1;
                                            ItemStack diamonds = new ItemStack(Material.DIAMOND);
                                            diamonds.setAmount(diamondAmount);
                                            player.getLocation().getWorld().dropItemNaturally(player.getLocation(), diamonds);
                                        } else if (n > 5 && n <= 15) {
                                            //IRON
                                            int ironAmount = rand.nextInt(5) + 1;
                                            ItemStack iron = new ItemStack(Material.IRON_INGOT);
                                            iron.setAmount(ironAmount);
                                            player.getLocation().getWorld().dropItemNaturally(player.getLocation(), iron);

                                        } else if (n > 15 && n <= 25) {
                                            //GOLD
                                            int goldAmount = rand.nextInt(5) + 1;
                                            ItemStack gold = new ItemStack(Material.GOLD_INGOT);
                                            gold.setAmount(goldAmount);
                                            player.getLocation().getWorld().dropItemNaturally(player.getLocation(), gold);

                                        } else if (n > 25 && n <= 30) {
                                            //EMERALD
                                            int emeraldAmount = rand.nextInt(3) + 1;
                                            ItemStack emeralds = new ItemStack(Material.EMERALD);
                                            emeralds.setAmount(emeraldAmount);
                                            player.getLocation().getWorld().dropItemNaturally(player.getLocation(), emeralds);

                                        } else if (n > 30 && n <= 70) {
                                            //COAL
                                            int coalAmount = rand.nextInt(3) + 1;
                                            ItemStack coal = new ItemStack(Material.COAL);
                                            coal.setAmount(coalAmount);
                                            player.getLocation().getWorld().dropItemNaturally(player.getLocation(), coal);
                                        } else if (n > 70 && n <= 160) {
                                            Utilities.sendBad(player, "You found nothing");
                                        } else if (n > 160) {
                                            n = rand.nextInt(14) + 1;

                                            switch (n) {
                                                case 1:
                                                    ItemStack book = new ItemStack(Material.BOOK);
                                                    book.setAmount(1);
                                                    player.getLocation().getWorld().dropItemNaturally(player.getLocation(), book);
                                                    break;
                                                case 2:
                                                    ItemStack bones = new ItemStack(Material.BONE);
                                                    bones.setAmount(5);
                                                    player.getLocation().getWorld().dropItemNaturally(player.getLocation(), bones);
                                                    break;
                                                case 3:
                                                    ItemStack lapis = new ItemStack(Material.LAPIS_ORE);
                                                    lapis.setAmount(3);
                                                    player.getLocation().getWorld().dropItemNaturally(player.getLocation(), lapis);
                                                    break;
                                                case 4:
                                                    ItemStack boots = new ItemStack(Material.LEATHER_BOOTS);
                                                    boots.setAmount(1);
                                                    player.getLocation().getWorld().dropItemNaturally(player.getLocation(), boots);
                                                    break;
                                                case 5:
                                                    ItemStack nametag = new ItemStack(Material.NAME_TAG);
                                                    nametag.setAmount(1);
                                                    player.getLocation().getWorld().dropItemNaturally(player.getLocation(), nametag);
                                                    break;
                                                case 6:
                                                    ItemStack bowl = new ItemStack(Material.BOWL);
                                                    bowl.setAmount(2);
                                                    player.getLocation().getWorld().dropItemNaturally(player.getLocation(), bowl);
                                                    break;
                                                case 7:
                                                    ItemStack blaze = new ItemStack(Material.BLAZE_POWDER);
                                                    blaze.setAmount(3);
                                                    player.getLocation().getWorld().dropItemNaturally(player.getLocation(), blaze);
                                                    break;
                                                case 8:
                                                    ItemStack flint = new ItemStack(Material.FLINT);
                                                    flint.setAmount(10);
                                                    player.getLocation().getWorld().dropItemNaturally(player.getLocation(), flint);
                                                    break;
                                                case 9:
                                                    ItemStack paper = new ItemStack(Material.PAPER);
                                                    paper.setAmount(3);
                                                    player.getLocation().getWorld().dropItemNaturally(player.getLocation(), paper);
                                                    break;
                                                case 10:
                                                    ItemStack redstone = new ItemStack(Material.REDSTONE);
                                                    redstone.setAmount(2);
                                                    player.getLocation().getWorld().dropItemNaturally(player.getLocation(), redstone);
                                                    break;
                                                case 11:
                                                    ItemStack dirt = new ItemStack(Material.DIRT);
                                                    dirt.setAmount(2);
                                                    player.getLocation().getWorld().dropItemNaturally(player.getLocation(), dirt);
                                                    break;
                                                case 12:
                                                    ItemStack cobble = new ItemStack(Material.COBBLESTONE);
                                                    cobble.setAmount(2);
                                                    player.getLocation().getWorld().dropItemNaturally(player.getLocation(), cobble);
                                                    break;
                                                case 13:
                                                    ItemStack arrow = new ItemStack(Material.ARROW);
                                                    arrow.setAmount(2);
                                                    player.getLocation().getWorld().dropItemNaturally(player.getLocation(), arrow);
                                                    break;
                                                case 14:
                                                    ItemStack boneblock = new ItemStack(Material.BONE_BLOCK);
                                                    boneblock.setAmount(1);
                                                    player.getLocation().getWorld().dropItemNaturally(player.getLocation(), boneblock);
                                                    break;                                                    
                                            }
                                        }
                                    }
                                }
                            }
                        }                        
                        
                    }
                }            
            }
        }
    }

    @EventHandler
    public void waypointEventAdd(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        if(e.getAction() == Action.RIGHT_CLICK_BLOCK)
        {
            if (e.getItem() != null && e.getItem().getType().equals(Material.GOLD_NUGGET)) {
                if (this.plugin.getGuardManager().WaypointMode.containsKey(player.getUniqueId())) {
                    NPC ThisNPC = this.plugin.getGuardManager().WaypointMode.get(player.getUniqueId());
                    SentryInstance inst = this.plugin.getGuardManager().getSentry(ThisNPC);
                    if (ThisNPC.isSpawned()) {
                        if(inst.waypoints.size() >= 10)
                        {
                            Utilities.sendBad(player, "You may only have 10 Waypoints per guard");
                            
                        } else {
                            Location loc = e.getClickedBlock().getLocation();
                            inst.addWaypoint(loc);
                            this.plugin.getGuardManager().addGuardWaypoint(ThisNPC);
                            Utilities.sendGood(player, "Location added as a Waypoint");
                        }
                    }
                }
            }            
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onMove(PlayerMoveEvent e) {
        Location loc = CenterStuff.moveSignals(e.getPlayer(), e.getFrom(), 100);
        Location loc2 = CenterUpgStuff.moveSignals(e.getPlayer(), e.getFrom(), 100);

        if (loc != null) {
            boolean fromTown = false;
            boolean toTown = false;

            double firstx = loc.getX() - 100;
            double firstz = loc.getZ() - 100;

            double secondx = loc.getX() + 100;
            double secondz = loc.getZ() + 100;

            double maxX = Math.max(firstx, secondx);
            double minX = Math.min(firstx, secondx);
            double maxZ = Math.max(firstz, secondz);
            double minZ = Math.min(firstz, secondz);

            Location To = e.getTo();
            Location From = e.getFrom();

            if ((To.getX() >= minX) && (To.getZ() >= minZ)
                    && (To.getX() <= maxX) && (To.getZ() <= maxZ)) {
                toTown = true;
            }
            if ((From.getX() >= minX) && (From.getZ() >= minZ)
                    && (From.getX() <= maxX) && (From.getZ() <= maxZ)) {
                fromTown = true;
            }
            if (!fromTown && toTown) {
                CenterStuff antLoc = CenterStuff.getAntenna(loc);
                String message = Utilities.capitalize(antLoc.getNation());
                TitleAPI.sendTitle(e.getPlayer(), 20, 60, 20, message, "Now Entering");
            }
        } else if(loc2 != null){
            boolean fromTown = false;
            boolean toTown = false;

            double firstx = loc2.getX() - 100;
            double firstz = loc2.getZ() - 100;

            double secondx = loc2.getX() + 100;
            double secondz = loc2.getZ() + 100;

            double maxX = Math.max(firstx, secondx);
            double minX = Math.min(firstx, secondx);
            double maxZ = Math.max(firstz, secondz);
            double minZ = Math.min(firstz, secondz);

            Location To = e.getTo();
            Location From = e.getFrom();

            if ((To.getX() >= minX) && (To.getZ() >= minZ)
                    && (To.getX() <= maxX) && (To.getZ() <= maxZ)) {
                toTown = true;
            }
            if ((From.getX() >= minX) && (From.getZ() >= minZ)
                    && (From.getX() <= maxX) && (From.getZ() <= maxZ)) {
                fromTown = true;
            }
            if (!fromTown && toTown) {
                CenterUpgStuff antLoc = CenterUpgStuff.getAntenna(loc2);
                String message = Utilities.capitalize(antLoc.getNation());
                TitleAPI.sendTitle(e.getPlayer(), 20, 60, 20, message, "Now Entering");
            }            
        }
    }
    
    @EventHandler
    public void blockPlaceNation(BlockPlaceEvent e) {
        Player player = e.getPlayer();
        if (player == null) {
            return;
        }
        if (player.isOp()){
            return;
        }

        Location loc = CenterStuff.moveSignals(e.getPlayer(), e.getBlock().getLocation(), 50);
        Location loc2 = CenterUpgStuff.moveSignals(e.getPlayer(), e.getBlock().getLocation(), 50);
        
        if (loc != null) {
            boolean insideTown = false;

            double firstx = loc.getX() - 100;
            double firstz = loc.getZ() - 100;

            double secondx = loc.getX() + 100;
            double secondz = loc.getZ() + 100;

            double maxX = Math.max(firstx, secondx);
            double minX = Math.min(firstx, secondx);
            double maxZ = Math.max(firstz, secondz);
            double minZ = Math.min(firstz, secondz);

            Location To = e.getBlock().getLocation();

            if ((To.getX() >= minX) && (To.getZ() >= minZ)
                    && (To.getX() <= maxX) && (To.getZ() <= maxZ)) {
                insideTown = true;
            }

            if (insideTown) {
                CenterStuff antLoc = CenterStuff.getAntenna(loc);
                nPlayer p = this.plugin.getPlayerManager().playerList.get(player.getUniqueId().toString());
                if(p != null){
                    if (!(antLoc.getNation().equalsIgnoreCase(p.getNation()))) {
                        e.setCancelled(true);
                        Utilities.sendBad(player, "Unable to build here");
                        Utilities.sendBad(player, "You do not belong to Nation " + ChatColor.GOLD + Utilities.capitalize(antLoc.getNation()));
                    }                 
                }
            }
        } else if(loc2 != null) {
            boolean insideTown = false;

            double firstx = loc2.getX() - 100;
            double firstz = loc2.getZ() - 100;

            double secondx = loc2.getX() + 100;
            double secondz = loc2.getZ() + 100;

            double maxX = Math.max(firstx, secondx);
            double minX = Math.min(firstx, secondx);
            double maxZ = Math.max(firstz, secondz);
            double minZ = Math.min(firstz, secondz);

            Location To = e.getBlock().getLocation();

            if ((To.getX() >= minX) && (To.getZ() >= minZ)
                    && (To.getX() <= maxX) && (To.getZ() <= maxZ)) {
                insideTown = true;
            }

            if (insideTown) {
                CenterUpgStuff antLoc = CenterUpgStuff.getAntenna(loc2);
                nPlayer p = this.plugin.getPlayerManager().playerList.get(player.getUniqueId().toString());
                if(p != null){
                    if (!(antLoc.getNation().equalsIgnoreCase(p.getNation()))) {
                        e.setCancelled(true);
                        Utilities.sendBad(player, "Unable to build here");
                        Utilities.sendBad(player, "You do not belong to Nation " + ChatColor.GOLD + Utilities.capitalize(antLoc.getNation()));
                    }                 
                }
            }            
        }
    }
}


