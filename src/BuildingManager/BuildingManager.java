package BuildingManager;

import CenterUpg.CenterUpgStuff;
import Center.CenterStuff;
import Granary.GranaryStuff;
import NationManager.nNation;
import NationManager.nResources;
import Pier.PierStuff;
import PlayerManager.PlayerManager;
import PlayerManager.PlayerManager.nPlayer;
import java.util.List;
import java.util.UUID;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.BlockFace;
import org.bukkit.util.Vector;
import Realms.RealmsAPI;
import chatapi.Utilities;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import org.bukkit.entity.Player;

/**
 *
 * @author lanca
 */
public class BuildingManager {
    
    public HashMap<UUID, Building> buildingList;
    private HashMap<String, UUID> buildingNameMap;
    public HashMap<String, ArrayList<String>> messages;
    
    private final RealmsAPI plugin;

    
    public BuildingManager(RealmsAPI plugin){
        this.plugin = plugin;
        this.buildingList = new HashMap<UUID, Building>();
        this.buildingNameMap = new HashMap<String, UUID>();
        this.messages = new HashMap<String, ArrayList<String>>();
        
    }
    
    
    public void createBuilding(Building building){
        
        this.buildingList.put(building.getUUID(), building);
        this.buildingNameMap.put(building.getDesignID(), building.getUUID());
        
        String design = building.getDesignID();
        
        switch(design.toLowerCase()){
            case "barracks":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setBarracks(nation.getBarracks()+1);
                    }
                }
                break;
                
            case "cityhall":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setCityHall(nation.getCityHall()+1);
                    }
                }
                break;
                
            case "crusher":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setCrusher(nation.getCrusher()+1);
                    }
                }
                break;
                
            case "kiln":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setKiln(nation.getKiln()+1);
                    }
                }
                break;
                
            case "center":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setCenter(nation.getCenter()+1);
                    }
                }
                break;
                
            case "centerupg":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setCenterUpg(nation.getCenterUpg()+1);
                    }
                }
                break;
                
            case "smeltery":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setSmithy(nation.getSmithy()+1);
                    }
                }
                break;
                
            case "forge":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setForge(nation.getForge()+1);
                    }
                }
                break;
                
            case "pier":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setPier(nation.getPier()+1);
                    }
                }
                break;
                
            case "granary":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setGranary(nation.getGranary()+1);
                    }
                }
                break;
                
            case "printingpress":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setPrinting(nation.getPrinting()+1);
                    }
                }
                break;
                
            case "library":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setLibrary(nation.getLibrary()+1);
                    }
                }
                break;   
        }
    }
    
    public void breakBuilding(UUID uuid){
        Building building = this.buildingList.get(uuid);
        //REMOVE FROM HASHMAP
        this.buildingList.remove(uuid);
        
        //REMOVE THE BUILDING FROM DATABASE HERE
        this.plugin.getBuildingSQL().deleteBuildings(uuid);
        
        //REMOVE BUILDING FROM NATION
        String design = building.getDesignID();
        
        //SEND MESSAGE TO ONLINE NATION MEMBERS AND THOSE WHO LOG IN
        if(this.messages.containsKey(building.getNation().toLowerCase()))
        {
            ArrayList<String> notifications = this.messages.get(building.getNation().toLowerCase());
            notifications.add("The " + Utilities.capitalize(design.toLowerCase()) + " has been destroyed!" 
                            + " X: " + building.getOffset().getBlockX() 
                            + " Y: " + building.getOffset().getBlockY() 
                            + " Z: " + building.getOffset().getBlockZ());
            this.messages.put(building.getNation().toLowerCase(), notifications);
        } else{
            ArrayList<String> notifications = new ArrayList<String>();
            notifications.add("The " + Utilities.capitalize(design.toLowerCase()) + " has been destroyed!" 
                            + " X: " + building.getOffset().getBlockX() 
                            + " Y: " + building.getOffset().getBlockY() 
                            + " Z: " + building.getOffset().getBlockZ());
            this.messages.put(building.getNation().toLowerCase(), notifications);
  
        }
        
        for(Player player : this.plugin.getServer().getOnlinePlayers())
        {
            nPlayer playerCheck = this.plugin.getPlayerManager().playerList.get(player.getUniqueId().toString());
            if(playerCheck != null)
            {
                if(playerCheck.getNation().equalsIgnoreCase(building.getNation()))
                {
                    Utilities.sendBad(player, "The " + Utilities.capitalize(design.toLowerCase()) + " has been destroyed!" 
                            + " X: " + building.getOffset().getBlockX() 
                            + " Y: " + building.getOffset().getBlockY() 
                            + " Z: " + building.getOffset().getBlockZ());
                }
            }
        }
        
        switch(design.toLowerCase()){
            case "barracks":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setBarracks(nation.getBarracks()-1);
                        this.plugin.getBuildingBreak().breakBarracks(building);
                    }
                }
                break;
                
            case "cityhall":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setCityHall(nation.getCityHall()-1);
                    }
                }
                break;
                
            case "crusher":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setCrusher(nation.getCrusher()-1);
                    }
                }
                break;
                
            case "kiln":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setKiln(nation.getKiln()-1);
                    }
                }
                break;
                
            case "center":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setCenter(nation.getCenter()-1);
                    }
                }
                break;
                
            case "centerupg":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setCenterUpg(nation.getCenterUpg()-1);
                    }
                }
                break;
                
            case "smeltery":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setSmithy(nation.getSmithy()-1);
                    }
                }
                break;
                
            case "forge":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setForge(nation.getForge()-1);
                    }
                }
                break;
                
            case "pier":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setPier(nation.getPier()-1);
                    }
                }
                break;
                
            case "granary":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setGranary(nation.getGranary()-1);
                    }
                }
                break;
                
            case "printingpress":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setPrinting(nation.getPrinting()-1);
                    }
                }
                break;
                
            case "library":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setLibrary(nation.getLibrary()-1);
                    }
                }
                break;   
        }
    }
    
    public void loadBuilding(Building building){
        
        this.buildingList.put(building.getUUID(), building);
        this.buildingNameMap.put(building.getDesignID(), building.getUUID());
        
        switch(building.getDesignID().toLowerCase()){
            case "center":
                Location loc = new Location(building.getWorldBukkit(),building.getOffset().getBlockX(),building.getOffset().getBlockY(),building.getOffset().getBlockZ());
                CenterStuff antLoc = new CenterStuff(loc, building.getNation());
                break;
            case "centerupg":
                Location loc1 = new Location(building.getWorldBukkit(),building.getOffset().getBlockX(),building.getOffset().getBlockY(),building.getOffset().getBlockZ());
                CenterUpgStuff antLoc1 = new CenterUpgStuff(loc1, building.getNation());
                break;
            case "pier":
                Location loc2 = new Location(building.getWorldBukkit(),building.getOffset().getBlockX(),building.getOffset().getBlockY(),building.getOffset().getBlockZ());
                PierStuff antLoc2 = new PierStuff(loc2, building.getNation());
                break;
            case "granary":
                Location loc3 = new Location(building.getWorldBukkit(),building.getOffset().getBlockX(),building.getOffset().getBlockY(),building.getOffset().getBlockZ());
                GranaryStuff antLoc3 = new GranaryStuff(loc3, building.getNation());
                break;
        }
    }
    
    public void removeBuilding(Location loc){
        Building building = getBuildingFromStorage(loc);
        
        //REMOVE FROM HASHMAP
        this.buildingList.remove(building.getUUID());
        
        //REMOVE THE BUILDING FROM DATABASE HERE
        this.plugin.getBuildingSQL().deleteBuildings(building.getUUID());
        
        //REMOVE BUILDING FROM NATION
        String design = building.getDesignID();
        
        switch(design.toLowerCase()){
            case "barracks":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setBarracks(nation.getBarracks()-1);
                        this.plugin.getBuildingBreak().breakBarracks(building);
                    }
                }
                break;
                
            case "cityhall":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setCityHall(nation.getCityHall()-1);
                    }
                }
                break;
                
            case "crusher":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setCrusher(nation.getCrusher()-1);
                    }
                }
                break;
                
            case "kiln":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setKiln(nation.getKiln()-1);
                    }
                }
                break;
                
            case "center":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setCenter(nation.getCenter()-1);
                    }
                }
                break;
                
            case "centerupg":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setCenterUpg(nation.getCenterUpg()-1);
                    }
                }
                break;
                
            case "smeltery":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setSmithy(nation.getSmithy()-1);
                    }
                }
                break;
                
            case "forge":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setForge(nation.getForge()-1);
                    }
                }
                break;
                
            case "pier":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setPier(nation.getPier()-1);
                    }
                }
                break;
                
            case "granary":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setGranary(nation.getGranary()-1);
                    }
                }
                break;
                
            case "printingpress":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setPrinting(nation.getPrinting()-1);
                    }
                }
                break;
                
            case "library":
                for(nNation nation : this.plugin.getNationManager().nationList){
                    if(nation.getName().equalsIgnoreCase(building.getNation())){
                        nation.setLibrary(nation.getLibrary()-1);
                    }
                }
                break;   
        }     
    }
    
    public void saveBuilds(){
        this.plugin.getBuildingSQL().saveBuildings();
    }
    
    public void loadBuilds(){
        this.plugin.getBuildingSQL().loadBuildings();
        
        for(Building building : this.buildingList.values())
        {
            if(building.getDesignID().equalsIgnoreCase("center"))
            {
                Location loc = new Location(building.getWorldBukkit(),building.getOffset().getBlockX(),building.getOffset().getBlockY(),building.getOffset().getBlockZ());
                CenterStuff town = new CenterStuff(loc, building.getNation());
            }
        }
    }
    
    public Building getBuildingFromStorage(Location loc) {
        RealmsAPI.getInstance().debug("Starting Building Search");
        for (Building building : this.buildingList.values()) {
            RealmsAPI.getInstance().debug("For Building");
            RealmsAPI.getInstance().debug("" + loc.toVector().distance(building.getOffset()));
            if (loc.toVector().distance(building.getOffset()) <= 15 && building.isBuildingBlock(loc.getBlock())) {
                //&& building.isBuildingBlock(loc.getBlock())
                
                    return building;
            }
        }
        
        return null;
    }
    
    public Building checkBuilding(Location buildingBlock, String nation) {
        World world = buildingBlock.getWorld();


        for (BuildingDesign buildingDesign : plugin.getDesignStorage().getBuildingDesignList()) {
            // check of all directions
            BlockFace buildingDirection = BlockFace.NORTH;
            for (int i = 0; i < 4; i++) {
                // for all blocks for the design
                List<SimpleBlock> designBlockList = buildingDesign.getAllBuildingBlocks(buildingDirection);
                //check for empty entries
                if (designBlockList.size() == 0) {
                    plugin.logInfo("There are empty building Schematics in folder. Please check it.");
                    return null;
                }
                for (SimpleBlock designBlock : designBlockList) {
                    // compare blocks
                    if (designBlock.compareBlockFuzzy(buildingBlock.getBlock())) {
                        // this block is same as in the design, get the offset
                        Vector offset = designBlock.subtractInverted(buildingBlock).toVector();

                        boolean isBuilding = true;

                        for (SimpleBlock checkBlocks : designBlockList) {
                            if (!checkBlocks.compareBlockFuzzy(world, offset)) {
                                // if the block does not match this is not the
                                // right one
                                isBuilding = false;
                                break;
                            }
                        }


                        if (isBuilding) {    
                            return new Building(buildingDesign, world.getName(), offset, buildingDirection, nation, Utilities.getDate());
                        }
                    }
                }

                buildingDirection = BuildingsUtil.roatateFace(buildingDirection);
            }

        }
        return null;
    }
        
    public void upkeepBuildings() throws ParseException{
        List<UUID> removeBuilds = new ArrayList<>();
        Date day1 = new Date();
        DateFormat format = new SimpleDateFormat("YYYY-MM-dd");
        Date current = format.parse(Utilities.getDate());
        for(Building building : this.buildingList.values()){
            day1 = format.parse(building.getDate());
            long difference = current.getTime() - day1.getTime();
            long differenceDays = difference / (1000 * 60 * 60 * 24);
            if(differenceDays > 7){
                BuildingDesign design = this.plugin.getBuildingDesign(building.getDesignID());
                nResources checkNation = null;

                for (nResources resource : this.plugin.getNationManager().nationResources) {
                    if (resource.getName().equalsIgnoreCase(building.getNation())) {
                        checkNation = resource;
                    }
                }
                if(checkNation != null){
                    if(design.getDesignID() != null){
                        boolean cost = this.plugin.getNationManager().checkUpkeepCost(design, checkNation);
                        if (cost) {
                            checkNation.setIron(checkNation.getIron() - design.getRecurringIron());
                            checkNation.setGold(checkNation.getGold() - design.getRecurringGold());
                            checkNation.setEmerald(checkNation.getEmerald() - design.getRecurringEmerald());
                            checkNation.setDiamond(checkNation.getDiamond() - design.getRecurringDiamond());
                            checkNation.setRedstone(checkNation.getRedstone() - design.getRecurringRedstone());
                            checkNation.setCoal(checkNation.getCoal() - design.getRecurringCoal());
                            checkNation.setStone(checkNation.getStone() - design.getRecurringStone());
                            checkNation.setLogs(checkNation.getLogs() - design.getRecurringLogs());
                            checkNation.setInk(checkNation.getInk() - design.getRecurringInk());
                            checkNation.setBooks(checkNation.getBooks() - design.getRecurringBooks());
                            checkNation.setFood(checkNation.getFood() - design.getRecurringFood());
                        } else {
                            removeBuilds.add(building.getUUID());
                        }
                    } else {
                        removeBuilds.add(building.getUUID());
                    }
                } else {
                    removeBuilds.add(building.getUUID());
                }
            }
        }
        for(UUID id : removeBuilds)
        {
            breakBuilding(id);
        }
        
    }
    
}
