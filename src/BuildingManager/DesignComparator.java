package BuildingManager;

import java.util.Comparator;

import org.bukkit.block.BlockFace;



public class DesignComparator implements Comparator<BuildingDesign>
{

	@Override
	public int compare(BuildingDesign design1, BuildingDesign design2)
	{
		int amount1 = getCannonBlockAmount(design1);
		int amount2 = getCannonBlockAmount(design2);
		
		return amount2 - amount1;
	}
	
	private Integer getCannonBlockAmount(BuildingDesign design)
	{
		if (design == null) return 0;
		//if the design is invalid something goes wrong, message the user
		if (design.getAllBuildingBlocks(BlockFace.NORTH) == null) 
		{
			System.out.println("[Cannons] invalid cannon design for " + design.getDesignName());
			return 0;
		}
		
		return design.getAllBuildingBlocks(BlockFace.NORTH).size();
	}

}
