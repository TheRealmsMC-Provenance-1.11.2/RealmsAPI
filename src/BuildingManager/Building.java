package BuildingManager;

import Realms.RealmsAPI;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.util.Vector;



/**
 *
 * @author lanca
 */
public class Building {
    
    private String nation;
    private Vector offset;
    private String designID;
    private UUID buildingID;
    private BlockFace buildingDirection;
    private String world;
    private String date;
    
    private BuildingDesign design;
    
    public Building(BuildingDesign design, String world, Vector offset, BlockFace buildingDirection, String nation, String date) {
        this.nation = nation;
        this.offset = offset;
        this.world = world;
        this.buildingDirection = buildingDirection;
        this.designID = design.getDesignID();
        this.date = date;
        this.buildingID = UUID.randomUUID();
        this.design = design;
        
    }
    
    
    public BlockFace getBuildingDirection()
    {
        return buildingDirection;
    }
    
    public void setBuildingDirection(BlockFace buildingDirection) {
        this.buildingDirection = buildingDirection;
    }
    
    public Vector getOffset() {
        return offset;
    }

    public void setOffset(Vector offset) {
        this.offset = offset;
    }

    public String getDate(){
        return this.date;
    }
    
    public void setDate(String date){
       this.date = date;
    }
    
    public String getWorld()
    {
        
        return world;
    }

    public void setWorld(String world)
    {
        this.world = world;
    }
    
    public World getWorldBukkit()
    {
        if (this.world != null)
        {
            World bukkitWorld = Bukkit.getWorld(this.world);
            if (bukkitWorld == null)
                System.out.println("[Nations] Can't find world: " + world);
            return Bukkit.getWorld(this.world);
            // return new Location(bukkitWorld, )
        }
        return null;
    }
    
    public String getDesignID()
    {
        return designID;
    }

    public void setDesignID(String designID)
    {
        this.designID = designID;
    }
    
    public String getNation(){
        return nation;
    }
    
    public void setNation(String nation){
        this.nation = nation;
    }
    
    public UUID getUUID(){
        return this.buildingID;
    }
    
    public void setUUID(UUID id){
        this.buildingID = id;
    }
    
    public boolean isBuildingBlock(Block block)
    {
        if (getWorld().equals(block.getWorld().getName())){
            RealmsAPI.getInstance().debug("Checking design block");
            for (SimpleBlock designBlock : this.design.getAllBuildingBlocks(buildingDirection))
            {
              
                if (designBlock.compareBlockAndLocFuzzy(block, offset))
                {
                    return true;
                }
            }
        }
       
        return false;
    }
}
