package BuildingManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

public class BuildingsUtil {

    /**
     * changes the extension of the a string (e.g. classic.yml to
     * classic.schematic)
     *
     * @param originalName
     * @param newExtension
     * @return
     */
    public static String changeExtension(String originalName, String newExtension) {
        int lastDot = originalName.lastIndexOf(".");
        if (lastDot != -1) {
            return originalName.substring(0, lastDot) + newExtension;
        } else {
            return originalName + newExtension;
        }
    }


    public static String removeExtension(String str) {
        return str.substring(0, str.lastIndexOf('.'));
    }

    /**
     * return true if the folder is empty
     *
     * @param folderPath
     * @return
     */
    public static boolean isFolderEmpty(String folderPath) {
        File file = new File(folderPath);
        if (file.isDirectory()) {
            if (file.list().length > 0) {
                //folder is not empty
                return false;
            }
        }
        return true;
    }

    /**
     * copies a file form the .jar to the disk
     *
     * @param in
     * @param file
     */
    public static void copyFile(InputStream in, File file) {
        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * rotates the direction by 90°
     *
     * @param face
     * @return
     */
    public static BlockFace roatateFace(BlockFace face) {
        if (face.equals(BlockFace.NORTH)) {
            return BlockFace.EAST;
        }
        if (face.equals(BlockFace.EAST)) {
            return BlockFace.SOUTH;
        }
        if (face.equals(BlockFace.SOUTH)) {
            return BlockFace.WEST;
        }
        if (face.equals(BlockFace.WEST)) {
            return BlockFace.NORTH;
        }
        return BlockFace.UP;
    }

    /**
     * rotates the direction by -90°
     *
     * @param face
     * @return
     */
    public static BlockFace roatateFaceOpposite(BlockFace face) {
        if (face.equals(BlockFace.NORTH)) {
            return BlockFace.WEST;
        }
        if (face.equals(BlockFace.EAST)) {
            return BlockFace.NORTH;
        }
        if (face.equals(BlockFace.SOUTH)) {
            return BlockFace.EAST;
        }
        if (face.equals(BlockFace.WEST)) {
            return BlockFace.SOUTH;
        }
        return BlockFace.UP;
    }

    /**
     * returns a list of MaterialHolder. Formatting id:data
     *
     * @param stringList list of Materials as strings
     * @return list of MaterialHolders
     */
    public static List<MaterialHolder> toMaterialHolderList(List<String> stringList) {
        List<MaterialHolder> materialList = new ArrayList<MaterialHolder>();

        for (String str : stringList) {
            MaterialHolder material = new MaterialHolder(str);
            //if id == -1 the str was invalid
            materialList.add(material);
        }

        return materialList;
    }

    /**
     * returns a list of MaterialHolder. Formatting id:data min:max
     *
     * @param stringList list of strings to convert
     * @return list of converted SpawnMaterialHolder
     */
    /**
     * returns a list of MaterialHolder. Formatting id:data min:max
     *
     * @param stringList list of strings to convert
     * @return list of converted SpawnMaterialHolder
     */
    /**
     * get all block next to this block (UP, DOWN, SOUT, WEST, NORTH, EAST)
     *
     * @param block
     * @return
     */
    public static ArrayList<Block> SurroundingBlocks(Block block) {
        ArrayList<Block> Blocks = new ArrayList<Block>();

        Blocks.add(block.getRelative(BlockFace.UP));
        Blocks.add(block.getRelative(BlockFace.DOWN));
        Blocks.add(block.getRelative(BlockFace.SOUTH));
        Blocks.add(block.getRelative(BlockFace.WEST));
        Blocks.add(block.getRelative(BlockFace.NORTH));
        Blocks.add(block.getRelative(BlockFace.EAST));
        return Blocks;
    }

    /**
     * get all block in the horizontal plane next to this block (SOUTH, WEST,
     * NORTH, EAST)
     *
     * @param block
     * @return
     */
    public static ArrayList<Block> HorizontalSurroundingBlocks(Block block) {
        ArrayList<Block> Blocks = new ArrayList<Block>();

        Blocks.add(block.getRelative(BlockFace.SOUTH));
        Blocks.add(block.getRelative(BlockFace.WEST));
        Blocks.add(block.getRelative(BlockFace.NORTH));
        Blocks.add(block.getRelative(BlockFace.EAST));
        return Blocks;
    }

    /**
     * returns a random block face
     *
     * @return - random BlockFace
     */
    public static BlockFace randomBlockFaceNoDown() {
        Random r = new Random();
        switch (r.nextInt(5)) {
            case 0:
                return BlockFace.UP;
            case 1:
                return BlockFace.EAST;
            case 2:
                return BlockFace.SOUTH;
            case 3:
                return BlockFace.WEST;
            case 4:
                return BlockFace.NORTH;
            default:
                return BlockFace.SELF;
        }
    }

    /**
     * adds a little bit random to the location so the effects don't spawn at
     * the same point.
     *
     * @return - randomized location
     */
    public static Location randomLocationOrthogonal(Location loc, BlockFace face) {
        Random r = new Random();

        //this is the direction we want to avoid
        Vector vect = new Vector(face.getModX(), face.getModY(), face.getModZ());
        //orthogonal vector - somehow
        vect = vect.multiply(vect).subtract(new Vector(1, 1, 1));

        loc.setX(loc.getX() + vect.getX() * (r.nextDouble() - 0.5));
        loc.setY(loc.getY() + vect.getY() * (r.nextDouble() - 0.5));
        loc.setZ(loc.getZ() + vect.getZ() * (r.nextDouble() - 0.5));

        return loc;
    }

    /**
     * find the surface in the given direction
     *
     * @param start starting point
     * @param direction direction
     * @return returns the the location of one block in front of the surface or
     * (if the surface is not found) the start location
     */
    /**
     * find the first block on the surface in the given direction
     *
     * @param start starting point
     * @param direction direction
     * @return returns the the location of one block in front of the surface or
     * (if the surface is not found) the start location
     */
    public static Location findFirstBlock(Location start, Vector direction) {
        World world = start.getWorld();
        Location surface = start.clone();

        //see if there is a block already - then go back if necessary
        if (!start.getBlock().isEmpty()) {
            surface.subtract(direction);
        }

        //are we now in air - if not, something is wrong
        if (!start.getBlock().isEmpty()) {
            return start;
        }

        //int length = (int) (direction.length()*3);
        BlockIterator iter = new BlockIterator(world, start.toVector(), direction.clone().normalize(), 0, 10);

        //try to find a surface of the
        while (iter.hasNext()) {
            Block next = iter.next();
            //if there is no block, go further until we hit the surface
            if (!next.isEmpty()) {
                return next.getLocation();
            }
        }
        // no surface found
        return null;
    }

    /**
     * returns a random number in the given range
     *
     * @param min smallest value
     * @param max largest value
     * @return a integer in the given range
     */
    public static int getRandomInt(int min, int max) {
        Random r = new Random();
        return r.nextInt(max + 1 - min) + min;
    }

    /**
     * converts a string to float
     *
     * @param str string to convert
     * @return returns parsed number or default
     */
    public static float parseFloat(String str, float default_value) {
        if (str != null) {
            try {
                return Float.parseFloat(str);
            } catch (Exception e) {
                throw new NumberFormatException();
            }
        }
        return default_value;
    }

    /**
     * converts a string to int
     *
     * @param str string to convert
     * @return returns parsed number or default
     */
    public static int parseInt(String str, int default_value) {
        if (str != null) {
            try {
                return Integer.parseInt(str);
            } catch (Exception e) {
                throw new NumberFormatException();
            }
        }
        return default_value;
    }

    /**
     * converts a string to color
     *
     * @param str string to convert
     * @return returns parsed color or default
     */
    public static Color parseColor(String str, Color default_value) {
        if (str != null) {
            try {
                return Color.fromRGB(Integer.parseInt(str));

            } catch (Exception e) {
                throw new NumberFormatException();
            }
        }
        return default_value;
    }

}
