package BuildingManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.block.BlockFace;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;
import Realms.RealmsAPI;

/**
 *
 * @author lanca
 */
public class BuildingSQL {
    
    private RealmsAPI plugin;
    
    public BuildingSQL(RealmsAPI _plugin) {
        plugin = _plugin;
    }
    
    public void loadBuildings(){
        
        Connection connection = null;
        String loadBuildings = "SELECT * FROM buildings";
        PreparedStatement load = null;
        
        try{
            connection = plugin.getHikari().getConnection();
            load = connection.prepareStatement(loadBuildings);
            ResultSet r = load.executeQuery();
            
            if(!r.next())
            {
                r.close();
                          
            } else {
                
                BuildingDesign design = plugin.getBuildingDesign(r.getString("designID"));

                World w = Bukkit.getWorld(r.getString("world"));
                

                if (w != null) {
                    String uuid = r.getString("uuid");
                    String nation = r.getString("nation");
                    Vector offset = new Vector(r.getInt("LocX"), r.getInt("LocY"), r.getInt("LocZ"));
                    BlockFace buildingDirection = BlockFace.valueOf(r.getString("buildingDirection"));
                    String date = r.getString("date");

                    Building building = new Building(design, w.getName(), offset, buildingDirection, nation, date);
                    building.setUUID(UUID.fromString(uuid));

                    BukkitTask task = new CreateBuilding(plugin, building).runTask(plugin);
                }
                
                while (r.next()) {

                    design = plugin.getBuildingDesign(r.getString("designID"));

                    w = Bukkit.getWorld(r.getString("world"));

                    if (w == null) {
                        continue;
                    }

                    String uuid = r.getString("uuid");
                    String nation = r.getString("nation");
                    Vector offset = new Vector(r.getInt("LocX"), r.getInt("LocY"), r.getInt("LocZ"));
                    BlockFace buildingDirection = BlockFace.valueOf(r.getString("buildingDirection"));
                    String date = r.getString("date");
                    
                    Building building = new Building(design, w.getName(), offset, buildingDirection, nation, date);
                    building.setUUID(UUID.fromString(uuid));

                    BukkitTask task = new CreateBuilding(plugin, building).runTask(plugin);
                }
            }  
            r.close();
            
        } catch (SQLException e){
            e.printStackTrace();
        } finally {
            if(connection != null){
                try {
                    connection.close();
                } catch (SQLException e){
                    e.printStackTrace();
                }
            }
            if (load != null){
                try{
                    load.close();
                } catch(SQLException e){
                    e.printStackTrace();
                }
            }
        }
        
    }
    
    public void saveBuildings(){
        
        for(Building building : this.plugin.getBuildingManager().buildingList.values())
        {
            Connection connection = null;

            String insert = "INSERT INTO buildings(uuid, nation, world, LocX, LocY, LocZ, designID, buildingDirection, date) VALUES(?,?,?,?,?,?,?,?,?) "
                    + "ON DUPLICATE KEY UPDATE nation=?, world=?, LocX=?, LocY=?, LocZ=?, designID=?, buildingDirection=?, date=?";
            PreparedStatement saveBuildings = null;

            String uuid;
            String nation;
            String world;
            int LocX;
            int LocY;
            int LocZ;
            String designID;
            String buildingDirection;
            String date;

            try {
                
                    connection = this.plugin.getHikari().getConnection();
                    saveBuildings = connection.prepareStatement(insert);

                    uuid = building.getUUID().toString();
                    nation = building.getNation();
                    world = building.getWorld();
                    LocX = building.getOffset().getBlockX();
                    LocY = building.getOffset().getBlockY();
                    LocZ = building.getOffset().getBlockZ();
                    date = building.getDate();
                    designID = building.getDesignID();
                    buildingDirection = building.getBuildingDirection().name();

                    saveBuildings.setString(1, uuid);
                    saveBuildings.setString(2, nation);
                    saveBuildings.setString(3, world);
                    saveBuildings.setInt(4, LocX);
                    saveBuildings.setInt(5, LocY);
                    saveBuildings.setInt(6, LocZ);
                    saveBuildings.setString(7, designID);
                    saveBuildings.setString(8, buildingDirection);
                    saveBuildings.setString(9, date);

                    saveBuildings.setString(10, nation);
                    saveBuildings.setString(11, world);
                    saveBuildings.setInt(12, LocX);
                    saveBuildings.setInt(13, LocY);
                    saveBuildings.setInt(14, LocZ);
                    saveBuildings.setString(15, designID);
                    saveBuildings.setString(16, buildingDirection);
                    saveBuildings.setString(17, date);
                    
                    saveBuildings.execute();

            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
                if (saveBuildings != null) {
                    try {
                        saveBuildings.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
        }  
    }
    
    public void deleteBuildings(UUID id){
        
        Connection connection = null;
        
        String delete = "DELETE FROM buildings WHERE uuid=?";
        
        PreparedStatement deleteBuilding = null;
        
        String uuid = id.toString();
        
        try{
            connection = plugin.getHikari().getConnection();
            deleteBuilding = connection.prepareStatement(delete);
            deleteBuilding.setString(1, uuid);
            deleteBuilding.execute();
            
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if(connection != null) {
                try{
                    connection.close();
                } catch (SQLException e){
                    e.printStackTrace();
                }
            }
            
            if(deleteBuilding != null){
                try{
                    deleteBuilding.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }   
}
