package BuildingManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.util.Vector;

/**
 *
 * @author lanca
 */
public class BuildingDesign {
    
    private String designID;
    private String designName;
    private String description;
    private String permissionBuild;
    private String permissionUse;
    
    private int iron;
    private int gold;
    private int emerald;
    private int diamond;
    private int redstone;
    private int coal;
    private int stone;
    private int logs;
    private int ink;
    private int books;
    private int food;
    
    private int recurringiron;
    private int recurringgold;
    private int recurringemerald;
    private int recurringdiamond;
    private int recurringredstone;
    private int recurringcoal;
    private int recurringstone;
    private int recurringlogs;
    private int recurringink;
    private int recurringbooks;
    private int recurringfood;
    
    private BlockFace defaultHorizontalFacing; 
    
    private MaterialHolder schematicBlockTypeIgnore;
    private MaterialHolder schematicBlockTypeRotationCenter;
    
    
    
    private HashMap<BlockFace, BuildingBlocks> buildingBlockMap = new HashMap<BlockFace, BuildingBlocks>();
    
    public Location getRotationCenter(Building building) {
        BuildingBlocks buildingBlocks = buildingBlockMap.get(building.getBuildingDirection());
        if(buildingBlocks != null){
            return buildingBlocks.getRotationCenter().clone().add(building.getOffset()).toLocation(building.getWorldBukkit());
        }
        
        return building.getOffset().toLocation(building.getWorldBukkit());
    }
    
    public List<SimpleBlock> getAllBuildingBlocks(BlockFace buildingDirection) {
        BuildingBlocks buildingBlocks = buildingBlockMap.get(buildingDirection);
        if (buildingBlocks != null){
            return buildingBlocks.getAllBuildingBlocks();
        }
        
        return new ArrayList<SimpleBlock>();
    }
    
    public List<Location> getAllBuildingBlocks(Building building) {
        BuildingBlocks buildingBlocks = buildingBlockMap.get(building.getBuildingDirection());
        List<Location> locList = new ArrayList<Location>();
        
        if (buildingBlocks != null) {
            for (SimpleBlock block : buildingBlocks.getAllBuildingBlocks()) {
                Vector vect = block.toVector();
                locList.add(vect.clone().add(building.getOffset()).toLocation(building.getWorldBukkit()));   
            }
        }
        return locList;
    }
    
    public String getDesignID() {
        return designID;
    }

    public void setDesignID(String designID) {
        this.designID = designID;
    }

    public String getDesignName() {
        return designName;
    }

    public void setDesignName(String designName) {
        this.designName = designName;
    }

    public String getPermissionBuild(){
        return permissionBuild;
    }
    
    public String getPermissionUse(){
        return permissionUse;
    }
    
    public void setPermissionBuild(String permissionBuild){
        this.permissionBuild = permissionBuild;
    }
    
    public void setPermissionUse(String permissionUse){
        this.permissionUse = permissionUse;
    }
    
    public MaterialHolder getSchematicBlockTypeIgnore() {
        return schematicBlockTypeIgnore;
    }

    public void setSchematicBlockTypeIgnore(MaterialHolder schematicBlockTypeIgnore) {
        this.schematicBlockTypeIgnore = schematicBlockTypeIgnore;
    }    

    public MaterialHolder getSchematicBlockTypeRotationCenter() {
        return schematicBlockTypeRotationCenter;
    }

    public void setSchematicBlockTypeRotationCenter(MaterialHolder schematicBlockTypeRotationCenter) {
        this.schematicBlockTypeRotationCenter = schematicBlockTypeRotationCenter;
    }
    
    public HashMap<BlockFace, BuildingBlocks> getBuildingBlockMap() {
        return buildingBlockMap;
    }

    public void setBuildingBlockMap(HashMap<BlockFace, BuildingBlocks> buildingBlockMap) {
        this.buildingBlockMap = buildingBlockMap;
    }
    
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDefaultHorizontalFacing(BlockFace defaultHorizontalFacing) {
        this.defaultHorizontalFacing = defaultHorizontalFacing;
    }

    public BlockFace getDefaultHorizontalFacing() {
        return defaultHorizontalFacing;
    }

    public int getIron()
    {
        return this.iron;
    }
    
    public int getGold()
    {
        return this.gold;
    }
    
    public int getEmerald()
    {
        return this.emerald;
    }
    
    public int getDiamond()
    {
        return this.diamond;
    }
    
    public int getRedstone()
    {
        return this.redstone;
    }
    
    public int getCoal()
    {
        return this.coal;
    }
    
    public int getStone()
    {
        return this.stone;
    }
    
    public int getLogs()
    {
        return this.logs;
    }
    
    public int getInk()
    {
        return this.ink;
    }
    
    public int getBooks()
    {
        return this.books;
    }
    
    public int getFood()
    {
        return this.food;
    }
    
    
    public void setIron(int i)
    {
        this.iron = i;
    }
    
    public void setGold(int i)
    {
        this.gold = i;
    }
    
    public void setEmerald(int i)
    {
        this.emerald = i;
    }
    
    public void setDiamond(int i)
    {
        this.diamond = i;
    }
    
    public void setRedstone(int i)
    {
        this.redstone = i;
    }
    
    public void setCoal(int i)
    {
        this.coal = i;
    }
    
    public void setStone(int i)
    {
        this.stone = i;
    }
    
    public void setLogs(int i)
    {
        this.logs = i;
    }
    
    public void setInk(int i)
    {
        this.ink = i;
    }
    
    public void setBooks(int i)
    {
        this.books = i;
    }
    
    public void setFood(int i)
    {
        this.food = i;
    }
    
    
    
    public int getRecurringIron()
    {
        return this.recurringiron;
    }
    
    public int getRecurringGold()
    {
        return this.recurringgold;
    }
    
    public int getRecurringEmerald()
    {
        return this.recurringemerald;
    }
    
    public int getRecurringDiamond()
    {
        return this.recurringdiamond;
    }
    
    public int getRecurringRedstone()
    {
        return this.recurringredstone;
    }
    
    public int getRecurringCoal()
    {
        return this.recurringcoal;
    }
    
    public int getRecurringStone()
    {
        return this.recurringstone;
    }
    
    public int getRecurringLogs()
    {
        return this.recurringlogs;
    }
    
    public int getRecurringInk()
    {
        return this.recurringink;
    }
    
    public int getRecurringBooks()
    {
        return this.recurringbooks;
    }
    
    public int getRecurringFood()
    {
        return this.recurringfood;
    }
    
    public void setRecurringIron(int i)
    {
        this.recurringiron = i;
    }
    
    public void setRecurringGold(int i)
    {
        this.recurringgold = i;
    }
    
    public void setRecurringEmerald(int i)
    {
        this.recurringemerald = i;
    }
    
    public void setRecurringDiamond(int i)
    {
        this.recurringdiamond = i;
    }
    
    public void setRecurringRedstone(int i)
    {
        this.recurringredstone = i;
    }
    
    public void setRecurringCoal(int i)
    {
        this.recurringcoal = i;
    }
    
    public void setRecurringStone(int i)
    {
        this.recurringstone = i;
    }
    
    public void setRecurringLogs(int i)
    {
        this.recurringlogs = i;
    }
    
    public void setRecurringInk(int i)
    {
        this.recurringink = i;
    }
    
    public void setRecurringBooks(int i)
    {
        this.recurringbooks = i;
    }
    
    public void setRecurringFood(int i)
    {
        this.recurringfood = i;
    }
    
    
}
