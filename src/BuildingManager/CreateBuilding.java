package BuildingManager;

import org.bukkit.scheduler.BukkitRunnable;
import Realms.RealmsAPI;

public class CreateBuilding extends BukkitRunnable {

    private final RealmsAPI plugin;
    private Building building;

    public CreateBuilding(RealmsAPI plugin, Building building){
        this.plugin = plugin;
        this.building = building;
    }

    @Override
    public void run() {
        plugin.getBuildingManager().loadBuilding(building);
    }
}
