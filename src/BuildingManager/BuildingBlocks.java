package BuildingManager;

import java.util.ArrayList;

import org.bukkit.util.Vector;


class BuildingBlocks {

    private Vector rotationCenter;														//center off all rotation blocks																//center off all muzzle blocks - spawing Vector for snowball
    private ArrayList<SimpleBlock> allBuildingBlocks = new ArrayList<SimpleBlock>();
    private ArrayList<Vector> destructibleBlocks = new ArrayList<Vector>();


    public Vector getRotationCenter() {
        return rotationCenter;
    }

    public void setRotationCenter(Vector rotationCenter) {
        this.rotationCenter = rotationCenter;
    }



    public ArrayList<SimpleBlock> getAllBuildingBlocks() {
        return allBuildingBlocks;
    }

    public void setAllBuildingBlocks(ArrayList<SimpleBlock> allBuildingBlocks) {
        this.allBuildingBlocks = allBuildingBlocks;
    }



    public ArrayList<Vector> getDestructibleBlocks() {
        return destructibleBlocks;
    }

    public void setDestructibleBlocks(ArrayList<Vector> destructibleBlocks) {
        this.destructibleBlocks = destructibleBlocks;
    }

}
