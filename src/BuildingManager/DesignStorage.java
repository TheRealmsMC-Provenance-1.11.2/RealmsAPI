package BuildingManager;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.blocks.BaseBlock;
import com.sk89q.worldedit.schematic.SchematicFormat;
import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.util.Vector;
import Realms.RealmsAPI;

/**
 *
 * @author lanca
 */
public class DesignStorage {
    
    private final List<BuildingDesign> buildingDesignList;
    
    private final RealmsAPI plugin;
    
    public DesignStorage(RealmsAPI realmsapi){
        this.plugin = realmsapi;
        buildingDesignList = new ArrayList<BuildingDesign>();
        copyDefaultDesigns();
    }
    
    public ArrayList<String> getDesignIds() {
        ArrayList<String> list = new ArrayList<String>();
        for (BuildingDesign design : buildingDesignList) {
            list.add(design.getDesignID());
        }
        return list;
    }
    
    
    public void loadBuildingDesigns() {
        plugin.logInfo("Loading building designs");

        //clear designList before loading
        buildingDesignList.clear();

        // check if design folder is empty or does not exist
        if (BuildingsUtil.isFolderEmpty(getPath())) {
            // the folder is empty, copy defaults
            plugin.logInfo("No building designs loaded - loading default designs");
            copyDefaultDesigns();
        }

        ArrayList<DesignFileName> designFileList = getDesignFiles();

        // stop if there are no files found
        if (designFileList == null || designFileList.size() == 0) {
            return;
        }

        for (DesignFileName designFile : designFileList) {
            plugin.logInfo("loading building " + designFile.getYmlString());
            BuildingDesign buildingDesign = new BuildingDesign();
            //load .yml
            loadDesignYml(buildingDesign, designFile.getYmlString());
            //load .shematic and add to list if valid
            if (loadDesignSchematic(buildingDesign, designFile.getSchematicString())) {
                buildingDesignList.add(buildingDesign);
            }
        }

        //sort the list so the designs with more building blocks comes first
        //important if there is a design with one block less but else identically 
        Comparator<BuildingDesign> comparator = new DesignComparator();
        Collections.sort(buildingDesignList, comparator);

        for (BuildingDesign design : buildingDesignList) {
            plugin.logInfo("design " + design.toString());
        }

    }

    /**
     * returns a list with valid building designs (.yml + .schematic)
     *
     * @return
     */
    private ArrayList<DesignFileName> getDesignFiles() {
        ArrayList<DesignFileName> designList = new ArrayList<DesignFileName>();

        try {
            // check plugin/buildings/designs for .yml and .schematic files
            String ymlFile;
            File folder = new File(getPath());

            File[] listOfFiles = folder.listFiles();
            if (listOfFiles == null) {
                plugin.logInfo("Design folder empty");
                return designList;
            }

            for (int i = 0; i < listOfFiles.length; i++) {

                if (listOfFiles[i].isFile()) {
                    ymlFile = listOfFiles[i].getName();
                    if (ymlFile.endsWith(".yml") || ymlFile.endsWith(".yaml")) {
                        String schematicFile = BuildingsUtil.changeExtension(ymlFile, ".schematic");
                        if (new File(getPath() + schematicFile).isFile()) {
                            // there is a shematic file and a .yml file
                            designList.add(new DesignFileName(ymlFile, schematicFile));
                        } else {
                            plugin.logInfo(schematicFile + " is missing");
                        }
                    }
                }
            }
        } catch (Exception e) {
            plugin.logInfo("Error while checking yml and schematic " + e);
        }
        return designList;
    }

    /**
     * loads the config for one building from the .yml file
     *
     * @param buildingDesign design of the building
     * @param ymlFile of the building config file
     */
    private void loadDesignYml(BuildingDesign buildingDesign, String ymlFile) {
        // load .yml file
        File buildingDesignFile = new File(getPath() + ymlFile);
        FileConfiguration buildingDesignConfig = YamlConfiguration.loadConfiguration(buildingDesignFile);

        // load all entries of the config file
        // general
        buildingDesign.setDesignID(BuildingsUtil.removeExtension(ymlFile));
        buildingDesign.setDesignName(buildingDesignConfig.getString("general.designName", "no buildingName"));
        buildingDesign.setDescription(buildingDesignConfig.getString("general.description", "no description for this building"));
        
        //ResourceCost
        buildingDesign.setIron(buildingDesignConfig.getInt("cost.iron"));
        buildingDesign.setGold(buildingDesignConfig.getInt("cost.gold"));
        buildingDesign.setEmerald(buildingDesignConfig.getInt("cost.emerald"));
        buildingDesign.setDiamond(buildingDesignConfig.getInt("cost.diamond"));
        buildingDesign.setRedstone(buildingDesignConfig.getInt("cost.redstone"));
        buildingDesign.setCoal(buildingDesignConfig.getInt("cost.coal"));
        buildingDesign.setStone(buildingDesignConfig.getInt("cost.stone"));
        buildingDesign.setLogs(buildingDesignConfig.getInt("cost.logs"));
        buildingDesign.setInk(buildingDesignConfig.getInt("cost.ink"));
        buildingDesign.setBooks(buildingDesignConfig.getInt("cost.books"));
        buildingDesign.setFood(buildingDesignConfig.getInt("cost.food"));
        
        buildingDesign.setRecurringIron(buildingDesignConfig.getInt("upkeep.iron"));
        buildingDesign.setRecurringGold(buildingDesignConfig.getInt("upkeep.gold"));
        buildingDesign.setRecurringEmerald(buildingDesignConfig.getInt("upkeep.emerald"));
        buildingDesign.setRecurringDiamond(buildingDesignConfig.getInt("upkeep.diamond"));
        buildingDesign.setRecurringRedstone(buildingDesignConfig.getInt("upkeep.redstone"));
        buildingDesign.setRecurringCoal(buildingDesignConfig.getInt("upkeep.coal"));
        buildingDesign.setRecurringStone(buildingDesignConfig.getInt("upkeep.stone"));
        buildingDesign.setRecurringLogs(buildingDesignConfig.getInt("upkeep.logs"));
        buildingDesign.setRecurringInk(buildingDesignConfig.getInt("upkeep.ink"));
        buildingDesign.setRecurringBooks(buildingDesignConfig.getInt("upkeep.books"));
        buildingDesign.setRecurringFood(buildingDesignConfig.getInt("upkeep.food"));


        //permissions
        buildingDesign.setPermissionBuild(buildingDesignConfig.getString("permissions.build", "buildings.player.build"));
        
        //Building Default Facing
        buildingDesign.setDefaultHorizontalFacing(BlockFace.EAST);


        // constructionBlocks
        buildingDesign.setSchematicBlockTypeIgnore(new MaterialHolder(buildingDesignConfig.getString("constructionBlocks.ignore", "SAND:0")));
        buildingDesign.setSchematicBlockTypeRotationCenter(new MaterialHolder(buildingDesignConfig.getString("constructionBlocks.rotationCenter", "REDSTONE_ORE:0")));


    }

    /**
     * loads the schematic of the config file
     *
     * @param buildingDesign design of the building
     * @param schematicFile path of the schematic file
     */
    private boolean loadDesignSchematic(BuildingDesign buildingDesign, String schematicFile) {
        long startTime = System.nanoTime();

        // load schematic with worldedit
        CuboidClipboard cc;
        File file = new File(getPath() + schematicFile);
        try {
            SchematicFormat schematic = SchematicFormat.getFormat(file);
            if (schematic == null) {
                plugin.logInfo("Schematic not loadable ");
            }
            cc = schematic.load(file);
        } catch (Exception e) {
            plugin.logInfo("Error while loading schematic " + getPath() + schematicFile + " :" + e + "; does file exist: " + file.exists());
            return false;
        }
        //failed to load schematic
        if (cc == null) {
            plugin.logInfo("Failed to loading schematic");
            return false;
        }

        // convert all schematic blocks from the config to BaseBlocks so they
        // can be rotated
        BaseBlock blockIgnore = buildingDesign.getSchematicBlockTypeIgnore().toBaseBlock();
        BaseBlock blockRotationCenter = buildingDesign.getSchematicBlockTypeRotationCenter().toBaseBlock();


        BlockFace buildingDirection = buildingDesign.getDefaultHorizontalFacing();

        // for all directions
        for (int i = 0; i < 4; i++) {
            // read out blocks
            int width = cc.getWidth();
            int height = cc.getHeight();
            int length = cc.getLength();

            // to set the muzzle location the maximum and mininum x, y, z values
            // of all muzzle blocks have to be found
            Vector minMuzzle = new Vector(0, 0, 0);
            Vector maxMuzzle = new Vector(0, 0, 0);
            boolean firstEntryMuzzle = true;

            // to set the rotation Center maximum and mininum x, y, z values
            // of all rotation blocks have to be found
            // setting max to the size of the marked area is a good approximation
            // if no rotationblock is given
            Vector minRotation = new Vector(0, 0, 0);
            Vector maxRotation = new Vector(width, height, length);
            boolean firstEntryRotation = true;

            // create BuildingBlocks entry
            BuildingBlocks buildingBlocks = new BuildingBlocks();

            for (int x = 0; x < width; ++x) {
                for (int y = 0; y < height; ++y) {
                    for (int z = 0; z < length; ++z) {
                        BlockVector pt = new BlockVector(x, y, z);
                        BaseBlock block = cc.getBlock(pt);

                        // ignore if block is AIR or the IgnoreBlock type
                        if (block.getId() != 0 && !block.equalsFuzzy(blockIgnore)) {

                            //plugin.logInfo("x:" + x + " y:" + y + " z:" + z + " blockType " + block.getId() + " blockData " + block.getData());
                            // #############  find the min and max for muzzle blocks so the
                            // buildingball is fired from the middle

                            if (block.equalsFuzzy(blockRotationCenter)) {
                                // reset for the first entry
                                if (firstEntryRotation) {
                                    firstEntryRotation = false;
                                    minRotation = new Vector(x, y, z);
                                    maxRotation = new Vector(x, y, z);
                                } else {
                                    minRotation = findMinimum(x, y, z, minRotation);
                                    maxRotation = findMaximum(x, y, z, maxRotation);
                                }
                            } // #############  redstoneTorch
                            
                            
                             // #############  rightClickTrigger
                            
                            else {
                                // all remaining blocks are loading interface or buildingBlocks
                                buildingBlocks.getAllBuildingBlocks().add(new SimpleBlock(x, y, z, block));
                                // this can be a destructible block

                                buildingBlocks.getDestructibleBlocks().add(new Vector(x, y, z));
                                
                            }


                        }
                    }
                }
            }
            // calculate the rotation Center
            maxRotation.add(new Vector(1, 1, 1));
            buildingBlocks.setRotationCenter(maxRotation.add(maxRotation).multiply(0.5));



            //plugin.logInfo("rotation loc " + buildingBlocks.getRotationCenter());
            // add blocks to the HashMap
            buildingDesign.getBuildingBlockMap().put(buildingDirection, buildingBlocks);

            //rotate blocks for the next iteration
            blockIgnore.rotate90();
            blockRotationCenter.rotate90();


            //rotate clipboard
            cc.rotate2D(90);


            buildingDirection = BuildingsUtil.roatateFace(buildingDirection);

        }
        plugin.logInfo("Time to load designs: " + new DecimalFormat("0.00").format((System.nanoTime() - startTime) / 1000000.0) + "ms");

        return true;
    }

    private Vector findMinimum(int x, int y, int z, Vector min) {
        if (x < min.getBlockX()) {
            min.setX(x);
        }
        if (y < min.getBlockY()) {
            min.setY(y);
        }
        if (z < min.getBlockZ()) {
            min.setZ(z);
        }

        return min;
    }

    private Vector findMaximum(int x, int y, int z, Vector max) {
        if (x > max.getBlockX()) {
            max.setX(x);
        }
        if (y > max.getBlockY()) {
            max.setY(y);
        }
        if (z > max.getBlockZ()) {
            max.setZ(z);
        }

        return max;
    }

    /**
     * copy the default designs from the .jar to the disk
     */
    private void copyDefaultDesigns() {
        copyFile("kiln");
        copyFile("barracks");
        copyFile("center");
    }

    /**
     * Copys the given .yml and .schematic from the .jar to the disk
     *
     * @param fileName - name of the design file
     */
    private void copyFile(String fileName) {
        File YmlFile = new File(plugin.getDataFolder(), "designs/" + fileName + ".yml");
        File SchematicFile = new File(plugin.getDataFolder(), "designs/" + fileName + ".schematic");

        SchematicFile.getParentFile().mkdirs();
        if (!YmlFile.exists()) {
            BuildingsUtil.copyFile(plugin.getResource("designs/" + fileName + ".yml"), YmlFile);
        }
        if (!SchematicFile.exists()) {
            BuildingsUtil.copyFile(plugin.getResource("designs/" + fileName + ".schematic"), SchematicFile);
        }
    }

    private boolean isInList(List<BaseBlock> list, BaseBlock block) {
        if (block == null) {
            return true;
        }

        for (BaseBlock listBlock : list) {
            if (listBlock != null && listBlock.equalsFuzzy(block)) {
                return true;
            }
        }
        return false;
    }

    private String getPath() {
        // Directory path here
        return "plugins/RealmsAPI/designs/";
    }

    public List<BuildingDesign> getBuildingDesignList() {
        return buildingDesignList;
    }

    /**
     * returns the building design of the building
     *
     * @param building the building
     * @return design of building
     */
    public BuildingDesign getDesign(Building building) {
        return getDesign(building.getDesignID());
    }

    /**
     * returns the building design by its id
     *
     * @param designId Name of the design
     * @return building design
     */
    public BuildingDesign getDesign(String designId) {
        for (BuildingDesign buildingDesign : buildingDesignList) {
            if (buildingDesign.getDesignID().equalsIgnoreCase(designId)) {
                return buildingDesign;
            }
        }
        return null;
    }

    /**
     * is there a building design with the give name
     *
     * @param name name of the design
     * @return true if there is a building design with this name
     */
    public boolean hasDesign(String name) {
        for (BuildingDesign design : buildingDesignList) {
            if (design.getDesignID().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }   
    
    
}
